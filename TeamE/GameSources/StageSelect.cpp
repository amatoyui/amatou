#include "stdafx.h"
#include "Project.h"

namespace basecross
{
    //----------------------------------------------------------------
	//	ステージセレクトクラス実体
	//----------------------------------------------------------------
	void StageSelect::CreateCiewLight()
	{
		auto PtrView = CreateView<SingleView>();
		//ビューとカメラ設定
		auto PtrCamera = ObjectFactory::Create<MyCamera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 20.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		PtrCamera->SetSelect(true);

		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, 1.0f);
	}

	//プレートの作成
	void StageSelect::CreatePlate()
	{
		auto scene = App::GetApp()->GetScene<Scene>();

		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quat Qt;
		Qt.rotationRollPitchYawFromVector(Vec3(XM_PIDIV2, 0, 0));
		//PtrTrans->SetScale(static_cast<float>(scene->GetVer()), static_cast<float>(scene->GetHori()) * 0.263f, 1.0f);
		PtrTrans->SetScale(100, 100 * 0.263f, 1.0f);
		PtrTrans->SetQuaternion(Qt);
		//PtrTrans->SetPosition(static_cast<float>(scene->GetVer()) / 2, 0.0f, static_cast<float>(scene->GetHori()) / 2);
		PtrTrans->SetPosition(50, -0.1f, 50);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"MAN_TX");
	}


	void StageSelect::CreatePlayer()
	{
		auto player = AddGameObject<Player>(Vec3(50.0f, 0.0f,45.0f), Vec3(1.5f, 1.5f, 1.5f), Vec3(0, 22.0f, 0), float(18.0f), L"NIGAKI_TX", SelectStageState::Instance(),2);
		player->SetVer(105);
		player->SetHori(75);
		auto scene = App::GetApp()->GetScene<Scene>();
		scene->SetMinHori(25);
		scene->SetMinVer(-7);
	}

	void StageSelect::FadeOutManager()
	{
		auto fadeout = AddGameObject<FadeOut>();
		SetSharedGameObject(L"Fadeout", fadeout);
	}

	void StageSelect::CreateDoor()
	{
		//フロア１----------------------------------------------------------------------------
		auto door1 = AddGameObject<SelectDoor>(Vec3(12.51f, 0.0f, 39.1f), L"DOOR1-1_TX");
		door1->SetStageNum(1);
		auto door2 = AddGameObject<SelectDoor>(Vec3(21.84f, 0.0f, 39.1f), L"DOOR1-2_TX");
		door2->SetStageNum(2);
		auto door3 = AddGameObject<SelectDoor>(Vec3(31.17f, 0.0f, 39.1f), L"DOOR1-3_TX");
		door3->SetStageNum(3);
		auto door4 = AddGameObject<SelectDoor>(Vec3(40.50f, 0.0f, 39.1f), L"DOOR1-4_TX");
		door4->SetStageNum(4);
		auto door5 = AddGameObject<SelectDoor>(Vec3(49.82f, 0.0f, 39.1f), L"DOOR1-5_TX");
		door5->SetStageNum(5);
		auto door6 = AddGameObject<SelectDoor>(Vec3(59.14f, 0.0f, 39.1f), L"DOOR1-6_TX");
		door6->SetStageNum(6);
		auto door7 = AddGameObject<SelectDoor>(Vec3(68.47f, 0.0f, 39.1f), L"DOOR1-7_TX");
		door7->SetStageNum(7);
		auto door8 = AddGameObject<SelectDoor>(Vec3(77.80f, 0.0f, 39.1f), L"DOOR1-8_TX");
		door8->SetStageNum(8);
		auto door9 = AddGameObject<SelectDoor>(Vec3(87.12f, 0.0f, 39.1f), L"DOOR1-9_TX");
		door9->SetStageNum(9);
		auto door10 = AddGameObject<SelectDoor>(Vec3(96.45f, 0.0f, 39.1f), L"DOOR1-10_TX");
		door10->SetStageNum(10);
		//フロア２----------------------------------------------------------------------------
		auto door11 = AddGameObject<SelectDoor>(Vec3(12.51f, 0.0f, 44.36f), L"DOOR2-1_TX");
		door11->SetStageNum(11);
		auto door12 = AddGameObject<SelectDoor>(Vec3(21.84f, 0.0f, 44.36f), L"DOOR2-2_TX");
		door12->SetStageNum(12);
		auto door13 = AddGameObject<SelectDoor>(Vec3(31.17f, 0.0f, 44.36f), L"DOOR2-3_TX");
		door13->SetStageNum(13);
		auto door14 = AddGameObject<SelectDoor>(Vec3(40.50f, 0.0f, 44.36f), L"DOOR2-4_TX");
		door14->SetStageNum(14);
		auto door15 = AddGameObject<SelectDoor>(Vec3(49.82f, 0.0f, 44.36f), L"DOOR2-5_TX");
		door15->SetStageNum(15);
		auto door16 = AddGameObject<SelectDoor>(Vec3(59.14f, 0.0f, 44.36f), L"DOOR2-6_TX");
		door16->SetStageNum(16);
		auto door17 = AddGameObject<SelectDoor>(Vec3(68.47f, 0.0f, 44.36f), L"DOOR2-7_TX");
		door17->SetStageNum(17);
		auto door18 = AddGameObject<SelectDoor>(Vec3(77.80f, 0.0f, 44.36f), L"DOOR2-8_TX");
		door18->SetStageNum(18);
		auto door19 = AddGameObject<SelectDoor>(Vec3(87.12f, 0.0f, 44.36f), L"DOOR2-9_TX");
		door19->SetStageNum(19);
		auto door20 = AddGameObject<SelectDoor>(Vec3(96.45f, 0.0f, 44.36f), L"DOOR2-10_TX");
		door20->SetStageNum(20);
		//フロア3----------------------------------------------------------------------------
		auto door21 = AddGameObject<SelectDoor>(Vec3(12.51f, 0.0f, 49.63f), L"DOORX_TX");
		door21->SetStageNum(21);
		auto door22 = AddGameObject<SelectDoor>(Vec3(21.84f, 0.0f, 49.63f), L"DOORX_TX");
		door22->SetStageNum(22);
		auto door23 = AddGameObject<SelectDoor>(Vec3(31.17f, 0.0f, 49.63f), L"DOORX_TX");
		door23->SetStageNum(23);
		auto door24 = AddGameObject<SelectDoor>(Vec3(40.50f, 0.0f, 49.63f), L"DOORX_TX");
		door24->SetStageNum(24);
		auto door25 = AddGameObject<SelectDoor>(Vec3(49.82f, 0.0f, 49.63f), L"DOORX_TX");
		door25->SetStageNum(25);
		auto door26 = AddGameObject<SelectDoor>(Vec3(59.14f, 0.0f, 49.63f), L"DOORX_TX");
		door26->SetStageNum(26);
		auto door27 = AddGameObject<SelectDoor>(Vec3(68.47f, 0.0f, 49.63f), L"DOORX_TX");
		door27->SetStageNum(27);
		auto door28 = AddGameObject<SelectDoor>(Vec3(77.80f, 0.0f, 49.63f), L"DOORX_TX");
		door28->SetStageNum(28);
		auto door29 = AddGameObject<SelectDoor>(Vec3(87.12f, 0.0f, 49.63f), L"DOORX_TX");
		door29->SetStageNum(29);
		auto door30 = AddGameObject<SelectDoor>(Vec3(96.45f, 0.0f, 49.63f), L"DOORX_TX");
		door30->SetStageNum(30);
		//フロア4----------------------------------------------------------------------------
		auto door31 = AddGameObject<SelectDoor>(Vec3(12.51f, 0.0f, 54.90f), L"DOOR_TX");
		door31->SetStageNum(31);
		auto door32 = AddGameObject<SelectDoor>(Vec3(21.84f, 0.0f, 54.90f), L"DOOR_TX");
		door32->SetStageNum(32);
		auto door33 = AddGameObject<SelectDoor>(Vec3(31.17f, 0.0f, 54.90f), L"DOOR_TX");
		door33->SetStageNum(33);
		auto door34 = AddGameObject<SelectDoor>(Vec3(40.50f, 0.0f, 54.90f), L"DOOR_TX");
		door34->SetStageNum(34);
		auto door35 = AddGameObject<SelectDoor>(Vec3(49.82f, 0.0f, 54.90f), L"DOOR_TX");
		door35->SetStageNum(35);
		auto door36 = AddGameObject<SelectDoor>(Vec3(59.14f, 0.0f, 54.90f), L"DOOR_TX");
		door36->SetStageNum(36);
		auto door37 = AddGameObject<SelectDoor>(Vec3(68.47f, 0.0f, 54.90f), L"DOOR_TX");
		door37->SetStageNum(37);
		auto door38 = AddGameObject<SelectDoor>(Vec3(77.80f, 0.0f, 54.90f), L"DOOR_TX");
		door38->SetStageNum(38);
		auto door39 = AddGameObject<SelectDoor>(Vec3(87.12f, 0.0f, 54.90f), L"DOOR_TX");
		door39->SetStageNum(39);
		auto door40 = AddGameObject<SelectDoor>(Vec3(96.45f, 0.0f, 54.90f), L"DOOR_TX");
		door40->SetStageNum(40);
		//フロア5----------------------------------------------------------------------------
		auto door41 = AddGameObject<CraeteDoor>(Vec3(12.51f, 0.0f, 60.16f), L"DOORX_TX");
		door41->SetStageNum(41);
		auto door42 = AddGameObject<SelectDoor>(Vec3(21.84f, 0.0f, 60.16f), L"DOOR_TX");
		door42->SetStageNum(42);
		auto door43 = AddGameObject<SelectDoor>(Vec3(31.17f, 0.0f, 60.16f), L"DOOR_TX");
		door43->SetStageNum(43);
		auto door44 = AddGameObject<SelectDoor>(Vec3(40.50f, 0.0f, 60.16f), L"DOOR_TX");
		door44->SetStageNum(44);
		auto door45 = AddGameObject<SelectDoor>(Vec3(49.82f, 0.0f, 60.16f), L"DOOR_TX");
		door45->SetStageNum(45);
		auto door46 = AddGameObject<SelectDoor>(Vec3(59.14f, 0.0f, 60.16f), L"DOOR_TX");
		door46->SetStageNum(46);
		auto door47 = AddGameObject<SelectDoor>(Vec3(68.47f, 0.0f, 60.16f), L"DOOR_TX");
		door47->SetStageNum(47);
		auto door48 = AddGameObject<SelectDoor>(Vec3(77.80f, 0.0f, 60.16f), L"DOOR_TX");
		door48->SetStageNum(48);
		auto door49 = AddGameObject<SelectDoor>(Vec3(87.12f, 0.0f, 60.16f), L"DOOR_TX");
		door49->SetStageNum(49);
		auto door50 = AddGameObject<SelectDoor>(Vec3(96.45f, 0.0f, 60.16f), L"DOOR_TX");
		door50->SetStageNum(50);
	}

	void StageSelect::CreateSprite()
	{
		auto sprite = AddGameObject<Sprite>(Vec2(-340.0f, 300.0f), Vec2(900.0f * 0.75f * 0.75f, 300.0f * 0.75f * 0.75f), L"SELECTLOGO_TX", true);
	}

	void StageSelect::OnCreate()
	{
		try{
			CreateSharedObjectGroup(L"EditGroup");

			//ビューとライトの作成
			CreateCiewLight();
			//プレイヤーの作成
			CreatePlayer();
			//プレートの作成
			CreatePlate();
			//AddGameObject<SelectPanel>(Vec3(50, -0.1f, 50));
			//スプライトの作成
			CreateSprite();
			//テスト
			FadeOutManager();
			//セレクト用のドア
			CreateDoor();

			AddGameObject<FadeIn>();
		}
		catch (...)
		{
			throw;
		}
	}	
}
