/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{
	Player::Player
	(
		const shared_ptr<Stage>& StagePtr,
		const Vec3 & StartPos,
		const Vec3& StartScale,
		const Vec3& StartRotation,
		const float& Speed,
		const wstring& TexturePtr,
		const shared_ptr<ObjState<Player>>& StartState,
		const int& LineCount
	) :
		GameObject(StagePtr),
		m_Pos(StartPos),
		m_Scale(StartScale),
		m_Rot(StartRotation),
		m_Speed(Speed),
		m_Texture(TexturePtr),
		m_LinePos(99.0f, 99.0f, 99.0f),
		m_State(StartState),
		m_buttonB(false),
		m_buttonY(false),
		m_hit(false),
		m_Pointflag(false),
		m_LineCount(LineCount),
		m_NowDis(0.0f),
		m_OneDis(0.0f),
		m_ObjCount(0),
		m_Deceleration(1.0f),
		m_ObjRot(0.0f),
		m_flag(false),
		m_MoveFlag(true)
	{}

	Player::Player(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node) :
		GameObject(StagePtr),
		m_LinePos(99.0f, 99.0f, 99.0f),
		m_buttonB(false),
		m_buttonY(false),
		m_hit(false),
		m_Pointflag(false),
		m_NowDis(0.0f),
		m_OneDis(0.0f),
		m_ObjCount(0),
		m_Deceleration(1.0f),
		m_ObjRot(0.0f),
		m_flag(false),
		m_MoveFlag(true)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			0.0f,
			static_cast<float>(_wtof(PosT[2].c_str()))
		);

		auto RotStr = XmlDocReader::GetAttribute(Node, L"Rot");
		vector<wstring> RotT;
		Util::WStrToTokenVector(RotT, RotStr, L',');
		m_Rot = Vec3
		(
			0, 
			22.0f,
			0
		);

		auto CountStr = XmlDocReader::GetAttribute(Node, L"LineCount");
		m_LineCount = static_cast<int>(_wtoi(CountStr.c_str()));

		m_Scale = Vec3(1.5f, 1.5f, 1.5f);
		m_Speed = 7.0f;
		m_Texture = L"NIGAKI_TX";
		m_State = PlayerLineState::Instance();
	}

	void Player::OnCreate()
	{
		auto Ptr = GetComponent<Transform>();
		Ptr->SetPosition(m_Pos);
		Ptr->SetRotation(m_Rot);
		Ptr->SetScale(m_Scale);
		
		//モデル用
		Mat4x4 mat;
		mat.affineTransformation
		(
			Vec3(0.5f,0.5f,0.5f),
			Vec3(0.0f,0.0f,0.0f),
			Vec3(0.0f,0.0f,0.0f),
			Vec3(0.0f,0.0f,0.0f)
		);

		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::None);

		//描画のコンポーネント
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"Hand");
		PtrDraw->SetMeshToTransformMatrix(mat);

		auto PtrCamera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		if (PtrCamera)
		{
			PtrCamera->SetTargetObject(GetThis<GameObject>());
		}

		m_StateMachine.reset(new LayeredStateMachine<Player>(GetThis<Player>()));
		m_StateMachine->Reset(m_State);

		m_PointArea = GetStage()->AddGameObject<Area>(Vec3(0.0f, 0.0f, 0.0f), Vec3(2.0f, 2.0f, 2.0f), Vec3(DegUp, 0.0f, 0.0f), L"CIRCLE_TX");
		m_PointArea.lock()->SetDrawActive(false);

		m_ThroughLine = GetStage()->AddGameObject<ThroughLine>(Vec3(8.0f, 0.0f, 7.0f), Vec3(DegUp, 0.0f, 0.0f), Vec3(10.0f, 10.0f, 10.0f), L"T_YELLOW_TX");
		m_ThroughLine.lock()->SetDrawActive(false);

		m_BeforePos = m_Pos;

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Reflection");

		GetStage()->SetSharedGameObject(L"Player", GetThis<Player>());
		AddTag(L"Player");
		SetAlphaActive(true);
	}

	void Player::OnUpdate()
	{
		m_StateMachine->Update();	
	}

	void Player::OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec)
	{
		for (auto& v : OtherVec)
		{
			//ラインゲームの処理
			if (v->FindTag(L"Point"))
			{
				m_hit = true;
				if (GetButtonB() && GetStateMachine()->GetTopState() == PlayerLineState::Instance() || GetButtonB() && GetStateMachine()->GetTopState() == EditModeState::Instance())
				{
					m_ObjStay = v;
					GetStateMachine()->Push(ChoosePointState::Instance());
				}

				if (!GetButtonB() && GetStateMachine()->GetTopState() == ChoosePointState::Instance())
				{
					m_ObjStay = v;
					GetStateMachine()->Pop();
				}
			}

			if (v->FindTag(L"Chara"))
			{
				m_hit = true;
				if (GetButtonB())
				{
					v->GetThis<AutoCharacter>()->SetTouchAcceleration(3.0f);
				}
			}

			if (GetButtonY() && GetStateMachine()->GetTopState() == EditModeState::Instance())
			{
				GetStage()->RemoveGameObject<GameObject>(v);
				GetStage()->GetSharedObjectGroup(L"EditGroup")->Remove(v);
			}

			if (v->FindTag(L"Door"))
			{
				m_hit = true;
			}
		}
	}

	void Player::OnCollisionExit(vector<shared_ptr<GameObject>>& OtherVec)
	{
		m_hit = false;
	}

	inline float Player::Distance(Vec3 one, Vec3 two)
	{
		//float distance = sqrtf(pow(m_oneVec.x - m_twoVec.x, 2) + pow(m_oneVec.z - m_twoVec.z, 2));
		float distance = sqrtf(pow(one.x - two.x, 2) + pow(one.z - two.z, 2));
		return distance;
	}

	inline Vec3 Player::dis()
	{
		Vec3 distance = m_twoVec - m_oneVec;
		return distance;
	}

	inline float Player::Rad()
	{
		float x = m_twoVec.x - m_oneVec.x;
		float z = m_twoVec.z - m_oneVec.z;
		float rad = -atan2f(z, x);

		return rad;
	}

	void Player::LineCreate()
	{
		if (m_hitcount == 2 && m_LineCount >= 1)
		{
			//線の生成
			auto line = GetStage()->AddGameObject<Line>(Vec3((m_twoVec.x + m_oneVec.x) / 2.0f, 0.0f, (m_twoVec.z + m_oneVec.z) / 2.0f), Vec3(0.0f, Rad(), 0.0f), Vec3(Distance(m_oneVec, m_twoVec), 0.1f, 1.0f), L"BLUE_TX");
			line->SetDistance(dis().normalize());
			line->SetOnePoint(m_oneVec);
			line->SetTwoPoint(m_twoVec);

			auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"Reflection");

			//点の色のリセット
			if (GetStateMachine()->GetTopState() == PlayerLineState::Instance() || GetStateMachine()->GetTopState() == EditModeState::Instance())
			{
				PointAcquisition();
			}
			m_hitcount = 0;
			m_LineCount -= 1;
		}

		if (m_hitcount == 2 && m_LineCount <= 0)
		{
			if (GetStateMachine()->GetTopState() == PlayerLineState::Instance() || GetStateMachine()->GetTopState() == EditModeState::Instance())
			{
				PointAcquisition();
			}
			m_hitcount = 0;
		}
	}

	void Player::PointChoose(shared_ptr<GameObject>& Obj)
	{
		if (!m_flag)
		{
			//ラインゲームの処理
			//1つ目の選択
			if (m_hitcount == 0)
			{
				Obj->GetThis<Point>()->GetStateMachine()->Push(PointChooseState::Instance());
				m_oneVec = Obj->GetComponent<Transform>()->GetPosition();
				m_hitcount += 1;
				m_PointArea.lock()->GetComponent<Transform>()->SetPosition(Vec3(m_oneVec.x, -0.3f, m_oneVec.z));
				m_PointArea.lock()->GetComponent<Transform>()->SetScale(Vec3(20.0f, 20.0f, 20.0f));
				m_PointArea.lock()->SetDrawActive(true);
				m_ThroughLine.lock()->SetDrawActive(true);
			}
			//2つ目の選択
			else if (m_oneVec != Obj->GetComponent<Transform>()->GetPosition() && m_Pointflag == true)
			{
				Obj->GetThis<Point>()->GetStateMachine()->Push(PointChooseState::Instance());
				m_twoVec = Obj->GetComponent<Transform>()->GetPosition();
				m_hitcount += 1;
				m_Pointflag = false;
			}
			//再選択時にステートを戻す
			else if (Obj->GetThis<Point>()->GetStateMachine()->GetTopState() == PointChooseState::Instance())
			{
				PointAcquisition();
				m_hitcount = 0;
				m_Pointflag = false;
			}

			m_Pointflag = true;
		}
		else
		{
			PointAcquisition();
			m_hitcount = 0;
			m_Pointflag = false;
			m_flag = false;
		}
	}

	void Player::PointAcquisition()
	{
		//点の取得
		auto point = GetStage()->GetSharedObjectGroup(L"EditGroup");
		for (auto& v : point->GetGroupVector())
		{
			auto shPtr = v.lock();
			if (shPtr)
			{
				auto pointPtr = dynamic_pointer_cast<Point>(shPtr);
				if (pointPtr)
				{
					//ステートのリセット
					pointPtr->GetStateMachine()->Reset(PointDefaultState::Instance());
					m_PointArea.lock()->SetDrawActive(false);
					m_ThroughLine.lock()->SetDrawActive(false);
				}
			}
		}
	}

	void Player::LineChoose(shared_ptr<GameObject>& Obj)
	{
		//あみだの処理
		//始点
		if (m_hitcount == 0 && m_LinePos != Obj->GetComponent<Transform>()->GetPosition())
		{
			m_LinePos = Obj->GetComponent<Transform>()->GetPosition();
			m_oneVec = Vec3(m_LinePos.x, 0.0f, GetComponent<Transform>()->GetPosition().z);
			m_hitcount += 1;
		}

		//終点
		if (m_oneVec != GetComponent<Transform>()->GetPosition() && m_LinePos != Obj->GetComponent<Transform>()->GetPosition())
		{
			m_twoVec = Vec3(Obj->GetComponent<Transform>()->GetPosition().x, 0.0f, GetComponent<Transform>()->GetPosition().z);
			m_hitcount += 1;
		}

		//ステートマシンをデフォルトに
		GetStateMachine()->Pop();

	}

	void Player::LineRange()
	{
		auto PlayerPos = GetComponent<Transform>()->GetPosition();

		if (m_hitcount == 1)
		{
			m_OneDis = sqrtf(pow(PlayerPos.x - m_oneVec.x, 2) + pow(PlayerPos.z - m_oneVec.z, 2));
		}
	}
	
	void Player::MoveRestriction()
	{
		auto PlayerPos = GetComponent<Transform>()->GetPosition();
		auto scene = App::GetApp()->GetScene<Scene>();
		auto minver = scene->GetMinVer();
		auto minhori = scene->GetMinHori();

		if (m_Vertical - 10 < PlayerPos.x || PlayerPos.x < (10 + minver))
		{
			PlayerPos.x = GetBeforePos().x;
		}
		if (m_Horizontal - 10 < PlayerPos.z || PlayerPos.z < (20 + minhori))
		{
			PlayerPos.z = GetBeforePos().z;
		}
		SetPosition(PlayerPos);
		if (m_OneDis > 10.0f)
		{
			SetPosition(GetBeforePos());
		}
	}

	void Player::SelectMoveRestriction()
	{
		auto PlayerPos = GetComponent<Transform>()->GetPosition();
		auto scene = App::GetApp()->GetScene<Scene>();
		auto minver = scene->GetMinVer();
		auto minhori = scene->GetMinHori();

		if (m_Vertical - 10 < PlayerPos.x || PlayerPos.x < (10 + minver))
		{
			PlayerPos.x = GetBeforePos().x;
		}
		if (m_Horizontal - 13 < PlayerPos.z || PlayerPos.z < (14 + minhori))
		{
			PlayerPos.z = GetBeforePos().z;
		}
		SetPosition(PlayerPos);
	}

	void Player::PredictionLine()
	{
		auto PlayerPos = GetComponent<Transform>()->GetPosition();
		auto distance = sqrtf(pow(PlayerPos.x - m_oneVec.x, 2) + pow(PlayerPos.z - m_oneVec.z, 2));
		auto x = m_oneVec.x - PlayerPos.x;
		auto z = m_oneVec.z - PlayerPos.z;
		auto rad = atan2f(z, x);
		if (m_hitcount == 1)
		{
			m_ThroughLine.lock()->GetThis<ThroughLine>()->SetPosition(Vec3((m_oneVec.x + PlayerPos.x) / 2.0f, 0.0f, (m_oneVec.z + PlayerPos.z) / 2.0f));
			m_ThroughLine.lock()->GetThis<ThroughLine>()->SetScale(Vec3(distance, 1.0f, 1.0f));
			m_ThroughLine.lock()->GetThis<ThroughLine>()->SetRot(Vec3(DegUp, -rad, 0.0f));
		}
	}

	void Player::ReDece()
	{
		if (GetDeceleration() < 1.0f)
		{
			auto time = App::GetApp()->GetElapsedTime();
			m_time += time;
			if (m_time > 1.0f)
			{
				SetDeceleration(1.0f);
				m_time = 0.0f;
			}
		}
	}

	void Player::CooseReset()
	{
		if (!GetButtonB() && !m_hit)
		{
			m_flag = true;
			m_OneDis = 0.0f;
			GetStateMachine()->Pop();
		}
	}

	void Player::CreateObject()
	{
		auto PlayerPos = GetComponent<Transform>()->GetPosition();
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		auto strxml = DataDir + L"StageCreate\\Data.xml";
		
		if (GetButtonA())
		{
			switch (GetObjCount())
			{
			case 0: 
			{
				//点
				auto point = GetStage()->AddGameObject<Point>
					(
						Vec3(PlayerPos),
						Vec3(1.0f, 1.0f, 1.0f),
						L"BLUE_TX",
						L"RED_TX"
					);
				break;
			}
			case 1:
			{
				float lineLength = 20.0f;
				float linewidth = 5.0f;
				//スタート用の足場
				switch (static_cast<int>(m_ObjRot))
				{
				case 0:
				{
					auto line_S = GetStage()->AddGameObject<Line>
						(
							Vec3(PlayerPos.x + lineLength * 0.5f, PlayerPos.y, PlayerPos.z),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(lineLength, 0.1f, linewidth),
							L"WHITE_TX"
						);
					line_S->AddTag(L"Line");
					//line_S->SetGroup();

					auto linePos = line_S->GetComponent<Transform>()->GetPosition();

					auto Chara = GetStage()->AddGameObject<AutoCharacter>
						(
							Vec3(linePos.x + lineLength / 2, linePos.y, linePos.z),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(1.5f, 1.5f, 1.5f),
							L"DOG_TX", -5.0f,
							CharaLineState::Instance(),
							-1.0f,
							0.0f,
							2.0f
						);
					//UpDateが止まっているとエラー
					//GetStage()->SetSharedGameObject(L"Chara", Chara);
					Chara->SetMoveFlag(false);
					//Chara->SetGroup();

					auto point_2 = GetStage()->AddGameObject<Point>
						(
							Vec3(linePos.x + lineLength / 2, linePos.y, linePos.z),
							Vec3(1.0f, 1.0f, 1.0f),
							L"BLUE_TX",
							L"RED_TX"
						);

					break;
				}
				case 90:
				{
					auto line_S = GetStage()->AddGameObject<Line>
						(
							Vec3(PlayerPos.x, PlayerPos.y, PlayerPos.z - lineLength * 0.5f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(lineLength, 0.1f, linewidth),
							L"WHITE_TX"
							);
					line_S->AddTag(L"Line");
					//line_S->SetGroup();

					auto linePos = line_S->GetComponent<Transform>()->GetPosition();

					auto Chara = GetStage()->AddGameObject<AutoCharacter>
						(
							Vec3(linePos.x, linePos.y, linePos.z - lineLength / 2),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(1.5f, 1.5f, 1.5f),
							L"DOG_TX", -5.0f,
							CharaLineState::Instance(),
							0.0f,
							1.0f,
							2.0f
							);
					//UpDateが止まっているとエラー
					//GetStage()->SetSharedGameObject(L"Chara", Chara);
					Chara->SetMoveFlag(false);
					//Chara->SetGroup();

					auto point_2 = GetStage()->AddGameObject<Point>
						(
							Vec3(linePos.x, linePos.y, linePos.z + lineLength / 2),
							Vec3(1.0f, 1.0f, 1.0f),
							L"BLUE_TX",
							L"RED_TX"
							);
					break;
				}
				case 180:
				{
					auto line_S = GetStage()->AddGameObject<Line>
						(
							Vec3(PlayerPos.x - lineLength * 0.5f, PlayerPos.y, PlayerPos.z),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(lineLength, 0.1f, linewidth),
							L"WHITE_TX"
							);
					line_S->AddTag(L"Line");
					//line_S->SetGroup();

					auto linePos = line_S->GetComponent<Transform>()->GetPosition();

					auto Chara = GetStage()->AddGameObject<AutoCharacter>
						(
							Vec3(linePos.x - lineLength / 2, linePos.y, linePos.z),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(1.5f, 1.5f, 1.5f),
							L"DOG_TX", -5.0f,
							CharaLineState::Instance(),
							1.0f,
							0.0f,
							2.0f
							);
					//UpDateが止まっているとエラー
					//GetStage()->SetSharedGameObject(L"Chara", Chara);
					Chara->SetMoveFlag(false);
					//Chara->SetGroup();

					auto point_2 = GetStage()->AddGameObject<Point>
						(
							Vec3(linePos.x + lineLength / 2, linePos.y, linePos.z),
							Vec3(1.0f, 1.0f, 1.0f),
							L"BLUE_TX",
							L"RED_TX"
							);
					break;
				}
				case 270:
				{
					auto line_S = GetStage()->AddGameObject<Line>
						(
							Vec3(PlayerPos.x, PlayerPos.y, PlayerPos.z + lineLength * 0.5f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(lineLength, 0.1f, linewidth),
							L"WHITE_TX"
							);
					line_S->AddTag(L"Line");
					//line_S->SetGroup();

					auto linePos = line_S->GetComponent<Transform>()->GetPosition();

					auto Chara = GetStage()->AddGameObject<AutoCharacter>
						(
							Vec3(linePos.x, linePos.y, linePos.z + lineLength / 2),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(1.5f, 1.5f, 1.5f),
							L"DOG_TX", -5.0f,
							CharaLineState::Instance(),
							0.0f,
							-1.0f,
							2.0f
							);
					Chara->SetMoveFlag(false);

					auto point_2 = GetStage()->AddGameObject<Point>
						(
							Vec3(linePos.x, linePos.y, linePos.z + lineLength / 2),
							Vec3(1.0f, 1.0f, 1.0f),
							L"BLUE_TX",
							L"RED_TX"
							);

					break;
				}
				default:
					break;
				}
				break;
			}
			case 2:
			{
				//ゴール用の足場
				float lineLength = 20.0f;
				float linewidth = 8.0f;
				switch (static_cast<int>(m_ObjRot))
				{
				case 0:
				{
					auto line_G = GetStage()->AddGameObject<GoalLine>
						(
							Vec3(PlayerPos.x + lineLength * 0.5f, PlayerPos.y, PlayerPos.z),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(lineLength, 0.1f, linewidth),
							L"WHITE_TX"
							);
					line_G->AddTag(L"Goalline");

					auto linePos = line_G->GetComponent<Transform>()->GetPosition();
					auto linescale = line_G->GetComponent<Transform>()->GetScale();

					auto point_1 = GetStage()->AddGameObject<Point>
						(
							Vec3(linePos.x - lineLength / 2, linePos.y, linePos.z),
							Vec3(1.0f, 1.0f, 1.0f),
							L"BLUE_TX",
							L"RED_TX"
						);

					auto goal = GetStage()->AddGameObject<Goal>
						(
							Vec3(linePos.x - lineLength / 4, linePos.y + 1.0f, linePos.z),
							Vec3(1.0f, 1.0f, 1.0f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							L"YELLOW_TX"
							);

					auto goalPanel = GetStage()->AddGameObject<GoalPanel>
						(
							Vec3(linePos.x - lineLength / 5.5f, linePos.y + 1.0f, linePos.z),
							Vec3(2.0f, linewidth, 1.0f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							L"HORIGOAL_TX"
						);
					break;
				}
				case 90:
				{
					auto line_G = GetStage()->AddGameObject<GoalLine>
						(
							Vec3(PlayerPos.x, PlayerPos.y, PlayerPos.z - lineLength * 0.5f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(lineLength, 0.1f, linewidth),
							L"WHITE_TX"
						);
					line_G->AddTag(L"Goalline");

					auto linePos = line_G->GetComponent<Transform>()->GetPosition();
					auto linescale = line_G->GetComponent<Transform>()->GetScale();

					auto point_1 = GetStage()->AddGameObject<Point>
						(
							Vec3(linePos.x, linePos.y, linePos.z - lineLength / 2),
							Vec3(1.0f, 1.0f, 1.0f),
							L"BLUE_TX",
							L"RED_TX"
							);

					auto goal = GetStage()->AddGameObject<Goal>
						(
							Vec3(linePos.x, linePos.y + 1.0f, linePos.z + lineLength / 4),
							Vec3(1.0f, 1.0f, 1.0f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							L"YELLOW_TX"
							);

					auto goalPanel = GetStage()->AddGameObject<GoalPanel>
						(
							Vec3(linePos.x, linePos.y + 1.0f, linePos.z + lineLength / 10.0f),
							Vec3(linewidth, 2.0f, 1.0f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							L"VERGOAL_TX"
							);
					break;
				}
				case 180:
				{
					auto line_G = GetStage()->AddGameObject<GoalLine>
						(
							Vec3(PlayerPos.x - lineLength * 0.5f, PlayerPos.y, PlayerPos.z),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(lineLength, 0.1f, linewidth),
							L"WHITE_TX"
							);
					line_G->AddTag(L"Goalline");

					auto linePos = line_G->GetComponent<Transform>()->GetPosition();
					auto linescale = line_G->GetComponent<Transform>()->GetScale();

					auto point_1 = GetStage()->AddGameObject<Point>
						(
							Vec3(linePos.x + lineLength / 2, linePos.y, linePos.z),
							Vec3(1.0f, 1.0f, 1.0f),
							L"BLUE_TX",
							L"RED_TX"
							);

					auto goal = GetStage()->AddGameObject<Goal>
						(
							Vec3(linePos.x + lineLength / 4, linePos.y + 1.0f, linePos.z),
							Vec3(1.0f, 1.0f, 1.0f), 
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							L"YELLOW_TX"
							);

					auto goalPanel = GetStage()->AddGameObject<GoalPanel>
						(
							Vec3(linePos.x + lineLength / 5.5f, linePos.y + 1.0f, linePos.z),
							Vec3(2.0f, linewidth, 1.0f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							L"HORIGOAL_TX"
							);
					break;
				}
				case 270:
				{

					auto line_G = GetStage()->AddGameObject<GoalLine>
						(
							Vec3(PlayerPos.x, PlayerPos.y, PlayerPos.z + lineLength * 0.5f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							Vec3(lineLength, 0.1f, linewidth),
							L"WHITE_TX"
							);
					line_G->AddTag(L"Goalline");

					auto linePos = line_G->GetComponent<Transform>()->GetPosition();
					auto linescale = line_G->GetComponent<Transform>()->GetScale();

					auto point_1 = GetStage()->AddGameObject<Point>
						(
							Vec3(linePos.x, linePos.y, linePos.z - lineLength / 2),
							Vec3(1.0f, 1.0f, 1.0f),
							L"BLUE_TX",
							L"RED_TX"
							);

					auto goal = GetStage()->AddGameObject<Goal>
						(
							Vec3(linePos.x, linePos.y + 1.0f, linePos.z - lineLength / 4),
							Vec3(1.0f, 1.0f, 1.0f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							L"YELLOW_TX"
							);

					auto goalPanel = GetStage()->AddGameObject<GoalPanel>
						(
							Vec3(linePos.x, linePos.y + 1.0f, linePos.z - lineLength / 7.0f),
							Vec3(linewidth, 2.0f, 1.0f),
							Vec3(0.0f, m_ObjRot * XM_PI / 180.0f, 0.0f),
							L"VERGOAL_TX"
							);
					break;
				}
				default:
					break;
				}

				break;
			}
			case 3:
			{
				//近づく敵
				auto close = GetStage()->AddGameObject<CloseGousun>(Vec3(PlayerPos), Vec3(DegUp, 0.0f, 0.0f), Vec3(2.0f, 2.0f, 2.0f), L"GOUSUN_TX");
				close->SetMove(false);
				break;
			}
			case 4:
			{
				//往復する敵
				auto turn = GetStage()->AddGameObject<TurnGousun>(Vec3(PlayerPos), Vec3(DegUp, 0.0f, 0.0f), Vec3(2.0f, 2.0f, 2.0f), L"GOUSUN_TX");
				turn->SetMove(false);
				break;
			}
			case 5:
			{
				//逃げていく敵
				auto away = GetStage()->AddGameObject<AwayGousun>(Vec3(PlayerPos), Vec3(DegUp, 0.0f, 0.0f), Vec3(2.0f, 2.0f, 2.0f), L"GOUSUN_TX");
				away->SetMove(false);
				break;
			}
			case 6:
			{
				//本数が増えるアイテム
				auto countItem = GetStage()->AddGameObject<PlusCount>(Vec3(PlayerPos.x, 0.5f, PlayerPos.z));
				break;
			}

			}
		}

		if (GetStartButton())
		{
			auto edit = GetStage()->GetSharedObjectGroup(L"EditGroup");

			auto stageNum = App::GetApp()->GetScene<Scene>()->GetStageNum();
			auto xml = new XmlAccessor(strxml);

			for (auto& v : edit->GetGroupVector())
			{
				auto shPtr = v.lock();
				if (shPtr)
				{
					auto pointPtr = dynamic_pointer_cast<Point>(shPtr);
					if (pointPtr)
					{
						xml->SetPoint(pointPtr, stageNum);
					}

					auto linePtr = dynamic_pointer_cast<Line>(shPtr);
					if(linePtr)
					{
						xml->SetLine(linePtr, stageNum);
					}
					auto goallinePtr = dynamic_pointer_cast<GoalLine>(shPtr);
					if (goallinePtr)
					{
						xml->SetGoalLine(goallinePtr, stageNum);
					}

					auto goalPtr = dynamic_pointer_cast<Goal>(shPtr);
					if (goalPtr)
					{
						xml->SetGoal(goalPtr, stageNum);
					}

					auto charaPtr = dynamic_pointer_cast<AutoCharacter>(shPtr);
					if (charaPtr)
					{
						xml->SetChara(charaPtr, stageNum);
					}

					auto turnPtr = dynamic_pointer_cast<TurnGousun>(shPtr);
					if (turnPtr)
					{
						xml->SetTurn(turnPtr, stageNum);
					}

					auto awayPtr = dynamic_pointer_cast<AwayGousun>(shPtr);
					if (awayPtr)
					{
						xml->SetAway(awayPtr, stageNum);
					}

					auto closePtr = dynamic_pointer_cast<CloseGousun>(shPtr);
					if (closePtr)
					{
						xml->SetClose(closePtr, stageNum);
					}

					auto plusItem = dynamic_pointer_cast<PlusCount>(shPtr);
					if (plusItem)
					{
						xml->SetItem(plusItem, stageNum);
					}

					auto goalPanel = dynamic_pointer_cast<GoalPanel>(shPtr);
					if (goalPanel)
					{
						xml->SetGoalPanel(goalPanel, stageNum);
					}
				}
			}			
			xml->SetPlayer(GetThis<Player>(), stageNum);
			xml->SetStageElement(stageNum);

			delete xml;
		}
	}

	void Player::RotationObj(int value)
	{ 
		switch (value)
		{
		case 0:
			m_ObjRot = 90.0f;
			break;
		case 1:
			m_ObjRot = 180.0f;
			break;
		case 2:
			m_ObjRot = 270.0f;
			break;
		case 3:
			m_ObjRot = 0.0f;
			break;
		}
	}

	void Player::SceneChange(int value)
	{
		auto scene = App::GetApp()->GetScene<Scene>();
		if (value == 0)
		{
			//App::GetApp()->GetScene<Scene>()->SetCsv(L"GameStage.csv");
			PostEvent(0.0f, GetThis<ObjectInterface>(), scene, L"ToGameStage");
		}
		else
		{
			App::GetApp()->GetScene<Scene>()->SetCsv(L"GameStage2.csv");
			PostEvent(0.0f, GetThis<ObjectInterface>(), scene, L"ToGameStage");
		}
	}

	void Player::CharaStop()
	{
		auto edit = GetStage()->GetSharedObjectGroup(L"EditGroup");
		for (auto& v : edit->GetGroupVector())
		{
			auto shPtr = v.lock();
			if (shPtr)
			{
				auto charaPtr = dynamic_pointer_cast<AutoCharacter>(shPtr);
				if (charaPtr)
				{
					charaPtr->SetMoveFlag(false);
				}
				auto turn = dynamic_pointer_cast<TurnGousun>(shPtr);
				if (turn)
				{
					turn->SetMove(false);
				}

				auto close = dynamic_pointer_cast<CloseGousun>(shPtr);
				if (close)
				{
					close->SetMove(false);
				}

				auto away = dynamic_pointer_cast<AwayGousun>(shPtr);
				if (away)
				{
					away->SetMove(false);
				}
			}
		}
	}

	//ラインゲーム用のステート
	IMPLEMENT_SINGLETON_INSTANCE(PlayerLineState)
	void PlayerLineState::Enter(const shared_ptr<Player>& Obj)
	{
	}
	void PlayerLineState::Execute(const shared_ptr<Player>& Obj)
	{
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(Obj->OnGetDrawCamera());
		if (PtrCamera->GetStart())
		{
			Obj->m_inputhandler.Move(Obj);
		}
		Obj->m_inputhandler.HandlerChoose(0, Obj);
		Obj->LineCreate();
		Obj->MoveRestriction();
		Obj->ReDece();
	}
	void PlayerLineState::Exit(const shared_ptr<Player>& Obj)
	{
	}

	//点を選択したとき
	IMPLEMENT_SINGLETON_INSTANCE(ChoosePointState)
	void ChoosePointState::Enter(const shared_ptr<Player>& Obj)
	{
		Obj->PointChoose(Obj->GetObj().lock());
	}
	void ChoosePointState::Execute(const shared_ptr<Player>& Obj)
	{
		Obj->m_inputhandler.Move(Obj);
		Obj->m_inputhandler.HandlerChoose(0, Obj);

		Obj->PredictionLine();
		Obj->LineRange();
		Obj->CooseReset();
		Obj->MoveRestriction();
		Obj->ReDece();
	}
	void ChoosePointState::Exit(const shared_ptr<Player>& Obj)
	{
		Obj->PointChoose(Obj->GetObj().lock());
	}

	//セレクトステージ用のステート
	IMPLEMENT_SINGLETON_INSTANCE(SelectStageState)
	void SelectStageState::Enter(const shared_ptr<Player>& Obj)
	{
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(Obj->OnGetDrawCamera());
		if (PtrCamera)
		{
			PtrCamera->SetStart(true);
		}
	}
	void SelectStageState::Execute(const shared_ptr<Player>& Obj)
	{
		Obj->m_inputhandler.Move(Obj);
		Obj->m_inputhandler.HandlerChoose(2, Obj);

		Obj->SelectMoveRestriction();
	}
	void SelectStageState::Exit(const shared_ptr<Player>& Obj)
	{
	}

	//エディットモードのステート
	IMPLEMENT_SINGLETON_INSTANCE(EditModeState)
	void EditModeState::Enter(const shared_ptr<Player>& Obj)
	{
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(Obj->OnGetDrawCamera());
		if (PtrCamera)
		{
			PtrCamera->SetStart(true);
		}
	}
	void EditModeState::Execute(const shared_ptr<Player>& Obj)
	{
		Obj->CreateObject();

		Obj->m_inputhandler.Move(Obj);
		Obj->m_inputhandler.HandlerChoose(1, Obj);

		Obj->LineCreate();
		Obj->LineRange();
		Obj->PredictionLine();
		Obj->MoveRestriction();

		Obj->CharaStop();
	}
	void EditModeState::Exit(const shared_ptr<Player>& Obj)
	{
	}
}
//end basecross

