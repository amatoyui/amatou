#pragma once
#include"stdafx.h"
#include "Project.h"

namespace basecross
{
	//-------------------------------------------------------------------------
	//	class Sprite : public GameObject
	//	用途: とりあえず表示だけさせてるやつ
	//-------------------------------------------------------------------------

	class Sprite : public GameObject
	{
	protected:
		bool m_Trace;
		Vec2 m_Pos;
		Vec2 m_Scale;
		Vec3 m_Rot;
		wstring m_Texture;
		float m_HalfSize;
		float m_AlphaNum;

		vector<VertexPositionColorTexture> m_BackupVertices;
	public:
		Sprite(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const wstring TexturePtr, const bool Trace);
		Sprite(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const Vec3& StartRot, const wstring TexturePtr, const bool Trace);
		virtual ~Sprite() {};

		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		
		void SetPosition(Vec2 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetPosition(value.x, value.y, 0.0f);
		}
		void SetScale(Vec2 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetScale(value.x, value.y, 0.0f);
		}
	}; 

	//-------------------------------------------------------------------------
	//	class TimeSprite : public GameObject
	//	用途 : タイム表示のスプライト
	//-------------------------------------------------------------------------

	class TimeSprite : public GameObject {
		bool m_Trace;
		Vec2 m_StartScale;
		Vec3 m_StartPos;
		wstring m_TextureKey;
		float m_Time;
		bool m_Start;
		//桁数
		UINT m_NumberOfDigits;
		//バックアップ頂点データ
		vector<VertexPositionTexture> m_BackupVertices;
	public:

		TimeSprite
		(
			const shared_ptr<Stage>& StagePtr, 
			UINT NumberOfDigits,
			float TimeNum,
			const wstring& TextureKey,
			bool Trace,
			const Vec2& StartScale, 
			const Vec3& StartPos
		);
		virtual ~TimeSprite() {}

		virtual void OnCreate() override;
		virtual void OnUpdate()override;

		//時間の管理
		void TimeSystem();

		float GetTime() { return m_Time; }

		void SetStart(bool flag) { m_Start = flag; }
		bool GetStart() { return m_Start; }
	};

	//-------------------------------------------------------------------------
	//	class TimeText : public GameObject
	//	用途 : 残り時間のText表示
	//-------------------------------------------------------------------------
	class TimeText : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;
		wstring m_Texture;

		vector<VertexPositionColorTexture> m_BackupVertices;
	public :
		TimeText(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const wstring TexturePtr);
		~TimeText() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

	//-------------------------------------------------------------------------
	//	class FadeOut : public GameObject
	//	用途 : フェードアウト 明るいところから暗くなる
	//-------------------------------------------------------------------------
	class FadeOut : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;
		wstring m_Texture;

		float Width;		//画像の横幅
		float Height;		//画像の縦幅

		float WidthSp;		//横幅の縮小速度
		float HeightSp;		//縦幅の縮小速度
		float Count;
		float m_AlphaNum;	



		bool m_On;		//完了したかどうか
		bool m_Start;	//起動するかどうか
		vector<VertexPositionColorTexture> m_BackupVertices;

	public:

		FadeOut(const shared_ptr<Stage>& StagePtr);
		virtual ~FadeOut() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
		void SetStart(bool start)
		{
			m_Start = start;
		};
		bool getOn() { return m_On; }


	};


	//-------------------------------------------------------------------------
	//	class FadeIn : public GameObject
	//	用途 : フェードイン 暗いところから明るくなる
	//-------------------------------------------------------------------------
	class FadeIn : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;
		wstring m_Texture;

		float Width;		//画像の横幅
		float Height;		//画像の縦幅

		float WidthSp;		//横幅の縮小速度
		float HeightSp;		//縦幅の縮小速度
		float Count;	

		

		bool m_On;
		vector<VertexPositionColorTexture> m_BackupVertices;

	public :

		FadeIn(const shared_ptr<Stage>& StagePtr);
		virtual ~FadeIn() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};


	//-------------------------------------------------------------------------
	//	class NumberSprite : public GameObject
	//	用途 : 数字変換用
	//-------------------------------------------------------------------------
	
	class NumberSprite : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Scale;
		//頂点の配列を管理する配列
		vector<vector<VertexPositionColorTexture>> m_NumVertexVec;
		//何番目かを取得
		size_t m_Num;

	public:
		NumberSprite(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartScale);
		virtual ~NumberSprite() {};

		virtual void OnCreate();
		virtual void OnUpdate();

		//アクセサ
		void SetNum(size_t num) { m_Num = num; }
	};

	//-------------------------------------------------------------------------
	//	class CountSsprite : public GameObject
	//	用途 : 本数表示用
	//-------------------------------------------------------------------------

	class CountSprite : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Scale;

		vector<shared_ptr<NumberSprite>> NumberSpritePtr;

	public:
		CountSprite(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartScale);
		virtual ~CountSprite() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

	//-------------------------------------------------------------------------
	//	class CountText : public GameObject
	//	用途 : 残り本数のText表示
	//-------------------------------------------------------------------------
	class CountText : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;
		wstring m_Texture;

		vector<VertexPositionColorTexture> m_BackupVertices;
	public:
		CountText(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const wstring TexturePtr);
		~CountText() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};



	//-------------------------------------------------------------------------
	//	class NextStage : public GameObject
	//	用途 : 次のステージに行くSprite
	//-------------------------------------------------------------------------
	class NextStage : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Scale;
		wstring m_Texture;

		vector<VertexPositionColorTexture> m_BackupVertices;
	public:
		NextStage(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const wstring TexturePtr);
		virtual ~NextStage() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

	//-------------------------------------------------------------------------
	//	class SelectStage : public GameObject
	//	用途 : セレクトステージへ行くSprite
	//-------------------------------------------------------------------------
	class SelectStage : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Scale;
		wstring m_Texture;

		vector<VertexPositionColorTexture> m_BackupVertices;
	public:
		SelectStage(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const wstring TexturePtr);
		virtual ~SelectStage() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

	//-------------------------------------------------------------------------
	//	class ResultUI : public GameObject
	//	用途 : リザルトで表示するUIをまとめたもの
	//-------------------------------------------------------------------------
	class ResultUI : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;
		int m_num;

		shared_ptr<GameObject> m_NextSprite;
		shared_ptr<GameObject> m_SelectSprite;
		shared_ptr<GameObject> m_HandSprite;

		vector<shared_ptr<GameObject>> m_RankUI;
	public:
		ResultUI(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale);
		virtual ~ResultUI() {};

		InputHandler<ResultUI> m_InputHandler;

		virtual void OnCreate();
		virtual void OnUpdate();

		void Choose();
		void SceneChange();
		void RankDisplay(bool flag);
	};

	//-------------------------------------------------------------------------
	//	class AnyButton : public GameObject
	//	用途 : TitleのAnyButton
	//-------------------------------------------------------------------------
	class AnyButton : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;
		wstring m_Texture;
		float m_TotalTime;
		bool m_DrawActive;

		vector<VertexPositionColorTexture> m_BackupVertices;
	public :
		AnyButton(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const wstring TexturePtr);
		virtual ~AnyButton() {};

		virtual void OnCreate();
		virtual void OnUpdate();

		//シーン変更
		void SceneChenger();
	};

	//-------------------------------------------------------------------------
	//	class TypeUI : public GameObject
	//	用途 : Typeの表示
	//-------------------------------------------------------------------------
	class TypeUI : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;

		vector<VertexPositionColorTexture> m_BackupVertices;
	public:
		TypeUI(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale);
		virtual ~TypeUI() {};

		virtual void OnCreate();
		virtual void OnUpdate();
	};

	//-------------------------------------------------------------------------
	//	class CountUI : public GameObject
	//	用途 : Countを保存するよう
	//-------------------------------------------------------------------------
	class CountUI : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;
		
		int m_count;

		vector<VertexPositionColorTexture> m_BackupVertices;

		vector<shared_ptr<NumberSprite>> NumberSpritePtr;
	public:
		CountUI(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale);
		virtual ~CountUI() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

	//-------------------------------------------------------------------------
	//	class RotUI : public GameObject
	//	用途 : Rotの表示
	//-------------------------------------------------------------------------
	class RotUI : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;
		Vec3 m_Rot;

		vector<VertexPositionColorTexture> m_BackupVertices;
	public:
		RotUI(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale);
		virtual ~RotUI() {};

		virtual void OnCreate();
		virtual void OnUpdate();
	};

	//-------------------------------------------------------------------------
	//	class RankUI : public GameObject
	//	用途 : Rankの表示
	//-------------------------------------------------------------------------
	class RankUI : public GameObject
	{
		Vec2 m_Pos;
		Vec2 m_Scale;
		Vec3 m_Rot;

		vector<VertexPositionColorTexture> m_BackupVertices;

		vector<shared_ptr<Sprite>> m_Rank;
	public:
		RankUI(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale);
		virtual ~RankUI() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};
}
