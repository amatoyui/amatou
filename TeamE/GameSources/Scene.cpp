﻿
/*!
@file Scene.cpp
@brief シーン実体
*/

#include "stdafx.h"
#include "Project.h"


namespace basecross{

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	void Scene::CreateResourses() {
		wstring DataDir;
		wstring DoorDataDir;
		//各ゲームは以下のようにデータディレクトリを取得すべき

		//テクスチャ
		App::GetApp()->GetDataDirectory(DataDir);
		DataDir += L"Texture\\";

		App::GetApp()->GetDataDirectory(DoorDataDir);
		DoorDataDir += L"Texture\\Doors\\";

		wstring strTexture = DataDir + L"Checker.png";
		App::GetApp()->RegisterTexture(L"Checker_TX", strTexture);
		strTexture = DataDir + L"hand.png";
		App::GetApp()->RegisterTexture(L"HAND_TX", strTexture);
		strTexture = DataDir + L"Blue.png";
		App::GetApp()->RegisterTexture(L"BLUE_TX", strTexture);
		strTexture = DataDir + L"Red.png";
		App::GetApp()->RegisterTexture(L"RED_TX", strTexture);
		strTexture = DataDir + L"yellow.png";
		App::GetApp()->RegisterTexture(L"YELLOW_TX", strTexture);
		strTexture = DataDir + L"dogText.png";
		App::GetApp()->RegisterTexture(L"DOG_TX", strTexture);
		strTexture = DataDir + L"sikaku.png";
		App::GetApp()->RegisterTexture(L"WHITE_TX", strTexture);
		strTexture = DataDir + L"kokken_skelton.jpg";
		App::GetApp()->RegisterTexture(L"KOKKEN_TX", strTexture);
		strTexture = DataDir + L"TimeNumber.png";
		App::GetApp()->RegisterTexture(L"TIMER_TX",strTexture);
		strTexture = DataDir + L"Circle2.png";
		App::GetApp()->RegisterTexture(L"CIRCLE_TX", strTexture);
		strTexture = DataDir + L"background.png";
		App::GetApp()->RegisterTexture(L"BACKGROUND_TX", strTexture);
		strTexture = DataDir + L"Door.png";
		App::GetApp()->RegisterTexture(L"DOOR_TX", strTexture);
		strTexture = DataDir + L"SenNokori.png";
		App::GetApp()->RegisterTexture(L"SENNOKORI_TX", strTexture);
		strTexture = DataDir + L"Nokorijikan.png";
		App::GetApp()->RegisterTexture(L"NOKORIJIKAN_TX", strTexture);
		strTexture = DataDir + L"ThroughYellow.png";
		App::GetApp()->RegisterTexture(L"T_YELLOW_TX", strTexture);
		strTexture = DataDir + L"Fade.png";
		App::GetApp()->RegisterTexture(L"FADE_TX", strTexture);
		strTexture = DataDir + L"FadeBox.png";
		App::GetApp()->RegisterTexture(L"FADEBOX_TX", strTexture);
		strTexture = DataDir + L"NextStageLogo.png";
		App::GetApp()->RegisterTexture(L"NEXTSTAGE_TX", strTexture);
		strTexture = DataDir + L"StageSelectLogo.png";
		App::GetApp()->RegisterTexture(L"SELECTSTAGE_TX", strTexture);
		strTexture = DataDir + L"TitleLogo.png";
		App::GetApp()->RegisterTexture(L"TITLE_TX", strTexture);
		strTexture = DataDir + L"SelectLogo.png";
		App::GetApp()->RegisterTexture(L"SELECTLOGO_TX", strTexture);
		strTexture = DataDir + L"AnyKey.png";
		App::GetApp()->RegisterTexture(L"ANYBUTTON_TX", strTexture);
		strTexture = DataDir + L"TypeGoal.png";
		App::GetApp()->RegisterTexture(L"TYPEGOAL_TX", strTexture);
		strTexture = DataDir + L"TypeGoalLine.png";
		App::GetApp()->RegisterTexture(L"TYPEGOALLINE_TX", strTexture);
		strTexture = DataDir + L"TypePoint.png";
		App::GetApp()->RegisterTexture(L"TYPEPOINT_TX", strTexture);
		strTexture = DataDir + L"TypeStartLine.png";
		App::GetApp()->RegisterTexture(L"TYPESTARTLINE_TX", strTexture);
		strTexture = DataDir + L"Man.png";
		App::GetApp()->RegisterTexture(L"MAN_TX", strTexture);
		strTexture = DataDir + L"Turn.png";
		App::GetApp()->RegisterTexture(L"TURN_TX", strTexture);
		strTexture = DataDir + L"Away.png";
		App::GetApp()->RegisterTexture(L"AWAY_TX", strTexture);
		strTexture = DataDir + L"Close.png";
		App::GetApp()->RegisterTexture(L"CLOSE_TX", strTexture);
		strTexture = DataDir + L"Close.png";
		App::GetApp()->RegisterTexture(L"CLOSE_TX", strTexture);
		strTexture = DataDir + L"HoriGoal.png";
		App::GetApp()->RegisterTexture(L"HORIGOAL_TX", strTexture);
		strTexture = DataDir + L"VerGoal.png";
		App::GetApp()->RegisterTexture(L"VERGOAL_TX", strTexture);
		strTexture = DataDir + L"TypeItem.png";
		App::GetApp()->RegisterTexture(L"TYPEITEM_TX", strTexture);
		strTexture = DataDir + L"Apartment.png";
		App::GetApp()->RegisterTexture(L"APARTMENT_TX", strTexture);

		strTexture = DataDir + L"Gousun.png";
		App::GetApp()->RegisterTexture(L"GOUSUN_TX", strTexture);

		strTexture = DataDir + L"Bone.png";
		App::GetApp()->RegisterTexture(L"BONE_TX", strTexture);


		//ステージドアのテクスチャ　効率厨なら変えてどうぞ(⦿ω⦿)👈ｵﾚ?
		//フロア１
		strTexture = DoorDataDir + L"Door1-1.png";
		App::GetApp()->RegisterTexture(L"DOOR1-1_TX", strTexture);
		strTexture = DoorDataDir + L"Door1-2.png";
		App::GetApp()->RegisterTexture(L"DOOR1-2_TX", strTexture);
		strTexture = DoorDataDir + L"Door1-3.png";
		App::GetApp()->RegisterTexture(L"DOOR1-3_TX", strTexture);
		strTexture = DoorDataDir + L"Door1-4.png";
		App::GetApp()->RegisterTexture(L"DOOR1-4_TX", strTexture);
		strTexture = DoorDataDir + L"Door1-5.png";
		App::GetApp()->RegisterTexture(L"DOOR1-5_TX", strTexture);
		strTexture = DoorDataDir + L"Door1-6.png";
		App::GetApp()->RegisterTexture(L"DOOR1-6_TX", strTexture);
		strTexture = DoorDataDir + L"Door1-7.png";
		App::GetApp()->RegisterTexture(L"DOOR1-7_TX", strTexture);
		strTexture = DoorDataDir + L"Door1-8.png";
		App::GetApp()->RegisterTexture(L"DOOR1-8_TX", strTexture);
		strTexture = DoorDataDir + L"Door1-9.png";
		App::GetApp()->RegisterTexture(L"DOOR1-9_TX", strTexture);
		strTexture = DoorDataDir + L"Door1-10.png";
		App::GetApp()->RegisterTexture(L"DOOR1-10_TX", strTexture);
		//フロア２
		strTexture = DoorDataDir + L"Door2-1.png";
		App::GetApp()->RegisterTexture(L"DOOR2-1_TX", strTexture);
		strTexture = DoorDataDir + L"Door2-2.png";
		App::GetApp()->RegisterTexture(L"DOOR2-2_TX", strTexture);
		strTexture = DoorDataDir + L"Door2-3.png";
		App::GetApp()->RegisterTexture(L"DOOR2-3_TX", strTexture);
		strTexture = DoorDataDir + L"Door2-4.png";
		App::GetApp()->RegisterTexture(L"DOOR2-4_TX", strTexture);
		strTexture = DoorDataDir + L"Door2-5.png";
		App::GetApp()->RegisterTexture(L"DOOR2-5_TX", strTexture);
		strTexture = DoorDataDir + L"Door2-6.png";
		App::GetApp()->RegisterTexture(L"DOOR2-6_TX", strTexture);
		strTexture = DoorDataDir + L"Door2-7.png";
		App::GetApp()->RegisterTexture(L"DOOR2-7_TX", strTexture);
		strTexture = DoorDataDir + L"Door2-8.png";
		App::GetApp()->RegisterTexture(L"DOOR2-8_TX", strTexture);
		strTexture = DoorDataDir + L"Door2-9.png";
		App::GetApp()->RegisterTexture(L"DOOR2-9_TX", strTexture);
		strTexture = DoorDataDir + L"Door2-10.png";
		App::GetApp()->RegisterTexture(L"DOOR2-10_TX", strTexture);
		//フロア３
		strTexture = DoorDataDir + L"Door3-1.png";
		App::GetApp()->RegisterTexture(L"DOOR3-1_TX", strTexture);
		strTexture = DoorDataDir + L"Door3-2.png";
		App::GetApp()->RegisterTexture(L"DOOR3-2_TX", strTexture);
		strTexture = DoorDataDir + L"Door3-3.png";
		App::GetApp()->RegisterTexture(L"DOOR3-3_TX", strTexture);
		strTexture = DoorDataDir + L"Door3-4.png";
		App::GetApp()->RegisterTexture(L"DOOR3-4_TX", strTexture);
		strTexture = DoorDataDir + L"Door3-5.png";
		App::GetApp()->RegisterTexture(L"DOOR3-5_TX", strTexture);
		strTexture = DoorDataDir + L"Door3-6.png";
		App::GetApp()->RegisterTexture(L"DOOR3-6_TX", strTexture);
		strTexture = DoorDataDir + L"Door3-7.png";
		App::GetApp()->RegisterTexture(L"DOOR3-7_TX", strTexture);
		strTexture = DoorDataDir + L"Door3-8.png";
		App::GetApp()->RegisterTexture(L"DOOR3-8_TX", strTexture);
		strTexture = DoorDataDir + L"Door3-9.png";
		App::GetApp()->RegisterTexture(L"DOOR3-9_TX", strTexture);
		strTexture = DoorDataDir + L"Door3-10.png";
		App::GetApp()->RegisterTexture(L"DOOR3-10_TX", strTexture);
		//フロア４
		strTexture = DoorDataDir + L"Door4-1.png";
		App::GetApp()->RegisterTexture(L"DOOR4-1_TX", strTexture);
		strTexture = DoorDataDir + L"Door4-2.png";
		App::GetApp()->RegisterTexture(L"DOOR4-2_TX", strTexture);
		strTexture = DoorDataDir + L"Door4-3.png";
		App::GetApp()->RegisterTexture(L"DOOR4-3_TX", strTexture);
		strTexture = DoorDataDir + L"Door4-4.png";
		App::GetApp()->RegisterTexture(L"DOOR4-4_TX", strTexture);
		strTexture = DoorDataDir + L"Door4-5.png";
		App::GetApp()->RegisterTexture(L"DOOR4-5_TX", strTexture);
		strTexture = DoorDataDir + L"Door4-6.png";
		App::GetApp()->RegisterTexture(L"DOOR4-6_TX", strTexture);
		strTexture = DoorDataDir + L"Door4-7.png";
		App::GetApp()->RegisterTexture(L"DOOR4-7_TX", strTexture);
		strTexture = DoorDataDir + L"Door4-8.png";
		App::GetApp()->RegisterTexture(L"DOOR4-8_TX", strTexture);
		strTexture = DoorDataDir + L"Door4-9.png";
		App::GetApp()->RegisterTexture(L"DOOR4-9_TX", strTexture);
		strTexture = DoorDataDir + L"Door4-10.png";
		App::GetApp()->RegisterTexture(L"DOOR4-10_TX", strTexture);
		//フロア５
		strTexture = DoorDataDir + L"Door5-1.png";
		App::GetApp()->RegisterTexture(L"DOOR5-1_TX", strTexture);
		strTexture = DoorDataDir + L"Door5-2.png";
		App::GetApp()->RegisterTexture(L"DOOR5-2_TX", strTexture);
		strTexture = DoorDataDir + L"Door5-3.png";
		App::GetApp()->RegisterTexture(L"DOOR5-3_TX", strTexture);
		strTexture = DoorDataDir + L"Door5-4.png";
		App::GetApp()->RegisterTexture(L"DOOR5-4_TX", strTexture);
		strTexture = DoorDataDir + L"Door5-5.png";
		App::GetApp()->RegisterTexture(L"DOOR5-5_TX", strTexture);
		strTexture = DoorDataDir + L"Door5-6.png";
		App::GetApp()->RegisterTexture(L"DOOR5-6_TX", strTexture);
		strTexture = DoorDataDir + L"Door5-7.png";
		App::GetApp()->RegisterTexture(L"DOOR5-7_TX", strTexture);
		strTexture = DoorDataDir + L"Door5-8.png";
		App::GetApp()->RegisterTexture(L"DOOR5-8_TX", strTexture);
		strTexture = DoorDataDir + L"Door5-9.png";
		App::GetApp()->RegisterTexture(L"DOOR5-9_TX", strTexture);
		strTexture = DoorDataDir + L"Door5-10.png";
		App::GetApp()->RegisterTexture(L"DOOR5-10_TX", strTexture);

		//その他ドア
		strTexture = DoorDataDir + L"Door.png";
		App::GetApp()->RegisterTexture(L"DOOR_TX", strTexture);
		strTexture = DoorDataDir + L"DoorX.png";
		App::GetApp()->RegisterTexture(L"DOORX_TX", strTexture);

		//モデル
		App::GetApp()->GetDataDirectory(DataDir);
		DataDir += L"Model\\";
		auto ModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Goal.bmf");
		App::GetApp()->RegisterResource(L"Goal", ModelMesh);
		ModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Hand.bmf");
		App::GetApp()->RegisterResource(L"Hand", ModelMesh);
		ModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Kokken.bmf");
		App::GetApp()->RegisterResource(L"Kokken", ModelMesh);

		ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"Kokken_03.bmf");
		App::GetApp()->RegisterResource(L"Kokken2", ModelMesh);

		//BGM
		App::GetApp()->GetDataDirectory(DataDir);
		DataDir += L"Sound\\BGM\\";
		wstring strBgm = DataDir + L"nanika.wav";
		App::GetApp()->RegisterWav(L"Main1", strBgm);
		strBgm = DataDir + L"ra ra ra ragtime.wav";
		App::GetApp()->RegisterWav(L"Select", strBgm);
		strBgm = DataDir + L"sad_day.wav";
		App::GetApp()->RegisterWav(L"Main", strBgm);
		strBgm = DataDir + L"雨の日のお散歩.wav";
		App::GetApp()->RegisterWav(L"Title", strBgm);

		//SE
		App::GetApp()->GetDataDirectory(DataDir);
		DataDir += L"Sound\\SE\\";
		wstring strDog = DataDir + L"dog1.wav";
		App::GetApp()->RegisterWav(L"Dog", strDog);
		wstring strFall = DataDir + L"fall02.wav";
		App::GetApp()->RegisterWav(L"Fall", strFall);
		wstring strSentaku = DataDir + L"Sys_Set02-sentaku.mp3";
		App::GetApp()->RegisterWav(L"Sentaku", strSentaku);
		wstring strKettei = DataDir + L"システム決定音_7_3.wav";
		App::GetApp()->RegisterWav(L"Kettei", strKettei);
		wstring strFanfare = DataDir + L"水色の光.wav";
		App::GetApp()->RegisterWav(L"Fanfare", strFanfare);
		wstring strKnock = DataDir + L"knock.wav";
		App::GetApp()->RegisterWav(L"Knock", strKnock);
		wstring strReflection = DataDir + L"reflection.wav";
		App::GetApp()->RegisterWav(L"Reflection", strReflection);

	}

	void Scene::OnCreate() {
		try {
			//リソース作成
			CreateResourses();

			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"Select");
			m_AudioObjectPtr->AddAudioResource(L"Main");
			m_AudioObjectPtr->AddAudioResource(L"Title");

			//自分自身にイベントを送る 
			//これにより各ステージやオブジェクトがCreate時にシーンにアクセスできる
			//PostEvent(0.0f, GetThis<ObjectInterface>(), GetThis<Scene>(), L"ToObjCreate");
		    //PostEvent(0.0f, GetThis<ObjectInterface>(), GetThis<Scene>(), L"ToStageSelect");
			PostEvent(0.0f, GetThis<ObjectInterface>(), GetThis<Scene>(), L"ToTitle");
		}
		catch (...) {
			throw;
		}
	}

	void Scene::OnEvent(const shared_ptr<Event>& event) {
		if (event->m_MsgStr == L"ToTitle") {
			StopBGM();
			m_AudioObjectPtr->Start(L"Title", XAUDIO2_LOOP_INFINITE, 0.5f);
			//最初のアクティブステージの設定
			ResetActiveStage<Title>();
		}
		if (event->m_MsgStr == L"ToGameStage")
		{
			StopBGM();
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.5f);

			ResetActiveStage<GameStage>();
		}
		if (event->m_MsgStr == L"ToStageSelect")
		{
			StopBGM();
			m_AudioObjectPtr->Start(L"Select", XAUDIO2_LOOP_INFINITE, 0.5f);

			ResetActiveStage<StageSelect>();
		}
		if (event->m_MsgStr == L"ToResult")
		{
			ResetActiveStage<Result>();
		}

		//あみだ用のステージ生成
		if (event->m_MsgStr == L"ToObjCreate")
		{
			ResetActiveStage<ObjCreateStage>();
		}
	}

	void Scene::StopBGM()
	{
		m_AudioObjectPtr->Stop(L"Title");
		m_AudioObjectPtr->Stop(L"Select");
		m_AudioObjectPtr->Stop(L"Main");
	}


}
//end basecross
