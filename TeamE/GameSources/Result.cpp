#include "stdafx.h"
#include "Project.h"
#include "Result.h"

namespace basecross
{
	//----------------------------------------------------------------
	//	リザルトクラス実体
    //----------------------------------------------------------------
	void Result::CreateViewLight()
	{
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラ設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vec3(0.0f, 10.0f, -1.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		PtrCamera->SetFovY(45.0f);
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	void Result::CreateSprite()
	{
		auto sprite = AddGameObject<Sprite>(Vec2(0.0f, 0.0f), Vec2(320.0f, 320.0f), L"SANRESULT_TX", false);
	}

	void Result::SceneChenger()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToStageSelect");
	}

	void Result::OnCreate()
	{
		try {
			CreateViewLight();
			CreateSprite();
		}
		catch (...)
		{
			throw;
		}
	}

	void Result::OnUpdate()
	{
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B)
		{
			SceneChenger();
		}
	}

}
