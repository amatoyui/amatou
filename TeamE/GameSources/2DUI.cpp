#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	Sprite::Sprite(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const wstring TexturePtr, const bool Trace) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Rot(0.0f, 0.0f, 0.0f), m_Texture(TexturePtr), m_Trace(Trace),m_AlphaNum(1.0f)
	{}
	Sprite::Sprite(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const Vec3& StartRot, const wstring TexturePtr, const bool Trace) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Rot(StartRot), m_Texture(TexturePtr), m_Trace(Trace)
	{}

	void Sprite::OnCreate()
	{
		m_HalfSize = 0.5f;
		//頂点の位置と色を指定して表示
		m_BackupVertices = 
		{
			{VertexPositionColorTexture(Vec3(-m_HalfSize,m_HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f))},
			{ VertexPositionColorTexture(Vec3(m_HalfSize,m_HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-m_HalfSize,-m_HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(m_HalfSize,-m_HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }

		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(m_Rot);
		
		//描画のコンポーネント
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices,indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_Texture);
		PtrDraw->OnDraw();

	}

	void Sprite::OnUpdate()
	{
	}

	//-------------------------------------------------------------------------
	//	class TimeText : public GameObject
	//	用途 : 残り時間のText表示
	//-------------------------------------------------------------------------
	TimeText::TimeText(const shared_ptr<Stage>& StagePtr, const Vec2 & StartPos, const Vec2 & StartScale, const wstring TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Texture(TexturePtr)
	{}

	void TimeText::OnCreate()
	{
		float HalfSize = 0.5f;
		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }

		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(true);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//描画のコンポーネント
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_Texture);
	}

	void TimeText::OnUpdate()
	{
		auto time = GetStage()->GetSharedGameObject<TimeSprite>(L"TimeSprite")->GetTime();
		if (time <= 0)
		{
			SetDrawActive(false);
		}
	}


	//----------------------------------------------------------------------
	//	class FadeOut : public GameObeject
	//	用途 : フェードアウト
	//----------------------------------------------------------------------
	FadeOut::FadeOut(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_Pos(0.0f, 0.0f),
		m_Scale(1600.0f, 900.0f),
		m_Texture(L"FADEBOX_TX"),
		Width(1600),
		Height(900),
		WidthSp(800.0f),
		HeightSp(450.0f),
		Count(),
		m_AlphaNum(0.0f),
		m_On(false),
		m_Start(false)
	{}
	void FadeOut::OnCreate()
	{

		float HelfSize = 0.5f;

		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HelfSize,HelfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize,HelfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HelfSize,-HelfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize,-HelfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }

		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };

		//透明度の適応
		SetAlphaActive(true);

		//トランスフォームの取得と設定
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//描画の取得と設定
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_Texture);
		PtrDraw->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, 0.0f));


	}
	void FadeOut::OnUpdate()
	{

		Count++;

		//トランスフォームの取得
		auto PtrTransform = GetComponent<Transform>();
		//elapasedtimeの取得
		auto ela = App::GetApp()->GetElapsedTime();

		
		auto PtrDraw = AddComponent<PCTSpriteDraw>();
		//透明度変更
		if (m_Start) 
		{
			PtrDraw->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, m_AlphaNum));
			m_AlphaNum += 0.025;

			if (Count >= 80)//フェード時間
			{
				m_AlphaNum = 1.0f;
				m_On = true;
			}
		}
	}

	//----------------------------------------------------------------------
	//	class FadeIn : public GameObeject
	//	用途 : フェードイン
	//----------------------------------------------------------------------
	FadeIn::FadeIn(const shared_ptr<Stage>& StagePtr):
		GameObject(StagePtr),
		m_Pos(0.0f,0.0f),
		m_Scale(1.0f,1.0f),
		m_Texture(L"FADE_TX"),
		Width(1600),
		Height(900),
		WidthSp(1000.0f),
		HeightSp(650.0f),
		Count(),
		m_On(false)
	{}
	void FadeIn::OnCreate()
	{

		float HelfSize = 0.5f;

		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HelfSize,HelfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize,HelfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HelfSize,-HelfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize,-HelfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }

		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };

		//透明度の適応
		SetAlphaActive(true);

		//トランスフォームの取得と設定
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x,m_Pos.y,0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//描画の取得と設定
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_Texture);
		

	}
	void FadeIn::OnUpdate()
	{
		//トランスフォームの取得
		auto PtrTransform = GetComponent<Transform>();
		//elapasedtimeの取得
		auto ela = App::GetApp()->GetElapsedTime();

		Count++;
		if (!m_On && Count < 100)
		{
			Width += WidthSp;
			Height += HeightSp;
			PtrTransform->SetScale(Width, Height, 1.0f);
		}
		else
		{
			auto a = GetThis<GameObject>();
			GetStage()->RemoveGameObject<GameObject>(a);
		}
	}

	//-------------------------------------------------------------------------
	///	タイム表示のスプライト
	//-------------------------------------------------------------------------
	TimeSprite::TimeSprite
	(
		const shared_ptr<Stage>& StagePtr,
		UINT NumberOfDigits,
		float TimeNum,
		const wstring& TextureKey,
		bool Trace,
		const Vec2& StartScale,
		const Vec3& StartPos
	) :
		GameObject(StagePtr),
		m_NumberOfDigits(NumberOfDigits),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_Time(TimeNum),
		m_Start(false)
	{}


	void TimeSprite::OnCreate()
	{
		float XPiecesize = 1.0f / (float)m_NumberOfDigits;
		float HelfSize = 0.5f;

		//インデックス配列
		vector<uint16_t> indices;
		for (UINT i = 0; i < m_NumberOfDigits; i++) 
		{
			float Vertex0 = -HelfSize + XPiecesize * (float)i;
			float Vertex1 = Vertex0 + XPiecesize;
			//0
			m_BackupVertices.push_back
			(
				VertexPositionTexture(Vec3(Vertex0, HelfSize, 0), Vec2(0.0f, 0.0f))
			);
			//1
			m_BackupVertices.push_back
			(
				VertexPositionTexture(Vec3(Vertex1, HelfSize, 0), Vec2(0.1f, 0.0f))
			);
			//2
			m_BackupVertices.push_back
			(
				VertexPositionTexture(Vec3(Vertex0, -HelfSize, 0), Vec2(0.0f, 1.0f))
			);
			//3
			m_BackupVertices.push_back
			(
				VertexPositionTexture(Vec3(Vertex1, -HelfSize, 0), Vec2(0.1f, 1.0f))
			);
			indices.push_back(i * 4 + 0);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 2);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 3);
			indices.push_back(i * 4 + 2);
		}

		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetTextureResource(m_TextureKey);
	}

	void TimeSprite::OnUpdate() 
	{
		vector<VertexPositionTexture> NewVertices;
		UINT Num;
		int VerNum = 0;
		for (UINT i = m_NumberOfDigits; i > 0; i--)
		{
			UINT Base = (UINT)pow(10, i);
			Num = ((UINT)m_Time) % Base;
			Num = Num / (Base / 10);

			//左上頂点
			Vec2 UV0 = m_BackupVertices[VerNum].textureCoordinate;
			UV0.x = (float)Num / 10.0f;
			auto v = VertexPositionTexture
			(
				m_BackupVertices[VerNum].position,
				UV0
			);
			NewVertices.push_back(v);

			//右上頂点
			Vec2 UV1 = m_BackupVertices[VerNum + 1].textureCoordinate;
			UV1.x = UV0.x + 0.1f;
			v = VertexPositionTexture
			(
				m_BackupVertices[VerNum + 1].position,
				UV1
			);
			NewVertices.push_back(v);

			//左下頂点
			Vec2 UV2 = m_BackupVertices[VerNum + 2].textureCoordinate;
			UV2.x = UV0.x;

			v = VertexPositionTexture
			(
				m_BackupVertices[VerNum + 2].position,
				UV2
			);
			NewVertices.push_back(v);

			//右下頂点
			Vec2 UV3 = m_BackupVertices[VerNum + 3].textureCoordinate;
			UV3.x = UV0.x + 0.1f;

			v = VertexPositionTexture
			(
				m_BackupVertices[VerNum + 3].position,
				UV3
			);
			NewVertices.push_back(v);

			VerNum += 4;
		}
		auto PtrDraw = GetComponent<PTSpriteDraw>();
		PtrDraw->UpdateVertices(NewVertices);

		TimeSystem();
	}

	void TimeSprite::TimeSystem()
	{
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		if (PtrCamera->GetStart())
		{
			auto time = App::GetApp()->GetElapsedTime();
			auto chara = GetStage()->GetSharedGameObject<AutoCharacter>(L"Chara");
			m_Time -= time;

			if (m_Time < 0.0f)
			{
				m_Time = 0.0f;
				SetDrawActive(false);
				chara->SetMoveFlag(true);
				m_Start = false;
			}
		}
	}

	//-------------------------------------------------------------------------
	//	class NumberSprite : public GameObject
	//	用途 : 数字変換用
	//-------------------------------------------------------------------------
	NumberSprite::NumberSprite(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const Vec3 & StartScale) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale)
	{}

	void NumberSprite::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(m_Scale);

		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"TIMER_TX");
		SetAlphaActive(true);
		//頂点のバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		auto Color = Col4(1.0f, 1.0f, 1.0f, 1.0f);
		//各数字ごとにUV値を含む頂点データを配列化する
		for (size_t i = 0; i < 10; i++)
		{
			float from = static_cast<float>(i) / 10.0f;
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture
				(
					SpVertexVec[0].position,
					Color,
					Vec2(from,0.0f)
				),
				//右上頂点
				VertexPositionColorTexture
				(
					SpVertexVec[1].position,
					Color,
					Vec2(to,0.0f)
				),
				//左下頂点
				VertexPositionColorTexture
				(
					SpVertexVec[2].position,
					Color,
					Vec2(from,1.0f)
				),
				//右下頂点
				VertexPositionColorTexture
				(
					SpVertexVec[3].position,
					Color,
					Vec2(to,1.0f)
				)		
			};
			m_NumVertexVec.push_back(NumVirtex);
		}
	}

	void NumberSprite::OnUpdate()
	{
		m_Num = m_Num % 10;
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値を変える
		MeshRes->UpdateVirtexBuffer(m_NumVertexVec[m_Num]);
	}

	//-------------------------------------------------------------------------
	//	class CountSsprite : public GameObject
	//	用途 : 本数表示用
	//-------------------------------------------------------------------------
	CountSprite::CountSprite(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const Vec3 & StartScale) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale)
	{}

	void CountSprite::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);

		NumberSpritePtr.push_back(GetStage()->AddGameObject<NumberSprite>(m_Pos, m_Scale));
		SetDrawActive(false);
	}

	void CountSprite::OnUpdate()
	{
		auto CountPtr = GetStage()->GetSharedGameObject<Player>(L"Player")->GetCount();
		NumberSpritePtr[0]->SetNum(CountPtr);
	}

	//-------------------------------------------------------------------------
	//	class CountText : public GameObject
	//	用途 : 残り本数のText表示
	//-------------------------------------------------------------------------
	CountText::CountText(const shared_ptr<Stage>& StagePtr, const Vec2 & StartPos, const Vec2 & StartScale, const wstring TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Texture(TexturePtr)
	{}
	void CountText::OnCreate()
	{
		float HalfSize = 0.5f;
		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }

		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(true);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//描画のコンポーネント
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_Texture);
	}
	void CountText::OnUpdate()
	{
	}

	//-------------------------------------------------------------------------
	//	class NextStage : public GameObject
	//	用途 : 次のステージに行くSprite
	//-------------------------------------------------------------------------
	NextStage::NextStage(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const wstring TexturePtr) :
		GameObject(StagePtr),m_Pos(StartPos), m_Scale(StartScale), m_Texture(TexturePtr)
	{}
	void NextStage::OnCreate()
	{
		float HalfSize = 0.5f;
		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }

		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(true);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//描画のコンポーネント
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_Texture);

	}
	void NextStage::OnUpdate()
	{

	}

	//-------------------------------------------------------------------------
	//	class SelectStage : public GameObject
	//	用途 : セレクトステージへ行くSprite
	//-------------------------------------------------------------------------
	SelectStage::SelectStage(const shared_ptr<Stage>& StagePtr, const Vec2& StartPos, const Vec2& StartScale, const wstring TexturePtr):
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Texture(TexturePtr)
	{}
	void SelectStage::OnCreate()
	{
		float HalfSize = 0.5f;
		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }

		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(true);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//描画のコンポーネント
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_Texture);
	}
	void SelectStage::OnUpdate()
	{

	}

	//-------------------------------------------------------------------------
	//	class ResultUI : public GameObject
	//	用途 : リザルトで表示するUIをまとめたもの
	//-------------------------------------------------------------------------
	ResultUI::ResultUI(const shared_ptr<Stage>& StagePtr, const Vec2 & StartPos, const Vec2 & StartScale) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_num(0)
	{}

	void ResultUI::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(1.0f, 1.0f, 1.0f);

		m_NextSprite = GetStage()->AddGameObject<NextStage>(Vec2(-300.0f, -200.0f), Vec2(256.0f + m_Scale.x, 170.0f + m_Scale.y), L"NEXTSTAGE_TX");
		m_NextSprite->SetDrawActive(false);
		auto nextPos = m_NextSprite->GetComponent<Transform>()->GetPosition();
		m_SelectSprite = GetStage()->AddGameObject<SelectStage>(Vec2(300.0f, -200.0f), Vec2(256.0f + m_Scale.x, 170.0f + m_Scale.y), L"SELECTSTAGE_TX");
		m_SelectSprite->SetDrawActive(false);
		m_HandSprite = GetStage()->AddGameObject<Sprite>(Vec2(nextPos.x + 100.0f, -300.0f), Vec2(100.0f, 144.0f), Vec3(0.0f, 0.0f, 45.0f * XM_PI / 180.0f), L"HAND_TX", true);
		m_HandSprite->SetDrawActive(false);

		for (int i = 0; i < 3; i++)
		{
			m_RankUI.push_back(GetStage()->AddGameObject<RankUI>(Vec2(-50.0f + 50.0f * i, 10.0f), Vec2(100.0f, 100.0f)));
			m_RankUI[i]->SetDrawActive(false);
		}
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Sentaku");
		pMultiSoundEffect->AddAudioResource(L"Kettei");

		SetAlphaActive(true);
	}

	void ResultUI::OnUpdate()
	{
		Choose();
	}

	void ResultUI::Choose()
	{
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		bool flag = PtrCamera->GetMoveEnd();
		if (flag)
		{
			//RankDisplay(flag);

			auto nextPos = m_NextSprite->GetComponent<Transform>()->GetPosition();
			auto selectPos = m_SelectSprite->GetComponent<Transform>()->GetPosition();
			m_NextSprite->SetDrawActive(true);
			m_SelectSprite->SetDrawActive(true);
			m_HandSprite->SetDrawActive(true);

			auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();

			int num = m_InputHandler.Choose(m_num, 1, GetThis<ResultUI>());

			if (m_num != num)
			{
				pMultiSoundEffect->Start(L"Sentaku", 0, 0.5f);
			}
			switch (num)
			{
			case 0:
				m_HandSprite->GetThis<Sprite>()->SetPosition(Vec2(nextPos.x + 100.0f, -300.0f));
				break;
			case 1:
				m_HandSprite->GetThis<Sprite>()->SetPosition(Vec2(selectPos.x + 100.0f, -300.0f));
				break;
			}
			m_num = num;
		}
	}

	void ResultUI::SceneChange()
	{
		auto scene = App::GetApp()->GetScene<Scene>();
		auto stageNum = scene->GetStageNum();
		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		switch (m_num)
		{
		case 0:
			scene->SetStageNum(stageNum + 1);
			PostEvent(0.0f, GetThis<ObjectInterface>(), scene, L"ToGameStage");
			pMultiSoundEffect->Start(L"Kettei");
			break;
		case 1:
			PostEvent(0.0f, GetThis<ObjectInterface>(), scene, L"ToStageSelect");
			pMultiSoundEffect->Start(L"Kettei");
			break;
		}
	}

	void ResultUI::RankDisplay(bool flag)
	{
		auto chara = GetStage()->GetSharedGameObject<AutoCharacter>(L"Chara");
		auto deadFlag = chara->GetDeadCount();
		if (deadFlag)
		{
			m_RankUI[0]->SetDrawActive(true);
		}
	}

	//-------------------------------------------------------------------------
	//	class AnyButton : public GameObject
	//	用途 : TitleのAnyButton
	//-------------------------------------------------------------------------
	AnyButton::AnyButton(const shared_ptr<Stage>& StagePtr, const Vec2 & StartPos, const Vec2 & StartScale, const wstring TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Texture(TexturePtr), m_DrawActive(true)
	{}

	void AnyButton::OnCreate()
	{
		float HalfSize = 0.5f;
		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }
		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(true);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//描画のコンポーネント
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_Texture);

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Dog");
	}
	void AnyButton::OnUpdate()
	{
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_TotalTime += ElapsedTime;
		if (m_TotalTime > 1.0f)
		{
			m_TotalTime = 0.0f;
			if (m_DrawActive)
			{
				m_DrawActive = false;
			}
			else
			{
				m_DrawActive = true;
			}
		}

		SetDrawActive(m_DrawActive);

		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].wButtons)
		{
			auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"Dog", 0, 0.3f);

			SceneChenger();
		}
	}

	void AnyButton::SceneChenger()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToStageSelect");
	}

	//-------------------------------------------------------------------------
	//	class TypeUI : public GameObject
	//	用途 : Typeの表示
	//-------------------------------------------------------------------------
	TypeUI::TypeUI(const shared_ptr<Stage>& StagePtr, const Vec2 & StartPos, const Vec2 & StartScale) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale)
	{}

	void TypeUI::OnCreate()
	{
		float HalfSize = 0.5f;
		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }
		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(true);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//描画のコンポーネント
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
	}

	void TypeUI::OnUpdate()
	{
		auto objCount = GetStage()->GetSharedGameObject<Player>(L"Player")->GetObjCount();
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		switch (objCount)
		{
		case 0:
			//点
			PtrDraw->SetTextureResource(L"TYPEPOINT_TX");
			break;
		case 1:
			//スタート、ゴール用の足場
			PtrDraw->SetTextureResource(L"TYPESTARTLINE_TX");
			break;
		case 2:
			//ゴール
			PtrDraw->SetTextureResource(L"TYPEGOALLINE_TX");
			break;
		case 3:
			//近づく敵
			PtrDraw->SetTextureResource(L"CLOSE_TX");
			break;
		case 4:
			//往復する敵
			PtrDraw->SetTextureResource(L"TURN_TX");
			break;
		case 5:
			//逃げていく敵
			PtrDraw->SetTextureResource(L"AWAY_TX");
			break;
		case 6:
			//アイテム
			PtrDraw->SetTextureResource(L"TYPEITEM_TX");
			break;
		}
	}

	//-------------------------------------------------------------------------
	//	class CountUI : public GameObject
	//	用途 : Countを保存するよう
	//-------------------------------------------------------------------------
	CountUI::CountUI(const shared_ptr<Stage>& StagePtr, const Vec2 & StartPos, const Vec2 & StartScale) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale)
	{}

	void CountUI::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos.x, m_Pos.y, 0.0f);

		NumberSpritePtr.push_back(GetStage()->AddGameObject<NumberSprite>(Vec3(m_Pos.x, m_Pos.y, 0.0f), Vec3(m_Scale.x, m_Scale.y, 1.0f)));
		SetDrawActive(false);
	}

	void CountUI::OnUpdate()
	{
		auto CountPtr = GetStage()->GetSharedGameObject<Player>(L"Player")->GetSaveCount();
		NumberSpritePtr[0]->SetNum(CountPtr);
	}

	//-------------------------------------------------------------------------
	//	class RotUI : public GameObject
	//	用途 : RotUIの表示
	//-------------------------------------------------------------------------
	RotUI::RotUI(const shared_ptr<Stage>& StagePtr, const Vec2 & StartPos, const Vec2 & StartScale) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Rot(0.0f, 0.0f, 90.0f * XM_PI / 180.0f)
	{}

	void RotUI::OnCreate()
	{
		float HalfSize = 0.5f;
		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }
		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(true);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(m_Rot);

		//描画のコンポーネント
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(L"HAND_TX");
	}

	void RotUI::OnUpdate()
	{
		auto objRot = GetStage()->GetSharedGameObject<Player>(L"Player")->GetObjRot();
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetRotation(0.0f, 0.0f, m_Rot.z - objRot * XM_PI / 180.0f);
	}

	//-------------------------------------------------------------------------
	//	class RankUI : public GameObject
	//	用途 : Rankの表示
	//-------------------------------------------------------------------------
	RankUI::RankUI(const shared_ptr<Stage>& StagePtr, const Vec2 & StartPos, const Vec2 & StartScale) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale)
	{}

	void RankUI::OnCreate()
	{
		float HalfSize = 0.5f;
		//頂点の位置と色を指定して表示
		m_BackupVertices =
		{
			{ VertexPositionColorTexture(Vec3(-HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(HalfSize,-HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(1.0f,1.0f)) }
		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(true);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(m_Rot);

		//描画のコンポーネント
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		//PtrDraw->SetTextureResource(L"SAN_TX");
	}
	void RankUI::OnUpdate()
	{
		
	}
}
