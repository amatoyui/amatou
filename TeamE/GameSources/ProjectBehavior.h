#pragma once
#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	//型の抽象化
	template<typename T>
	//コントローラーのハンドラ
	struct InputHandler
	{
		//コントローラーの動きを選択
		void HandlerChoose(int num , const shared_ptr<T>& Obj)
		{
			switch (num)
			{
			case 0:
				//main
				PushHandler(Obj);
				break;
			case 1:
				//edit
				EditModeHandler(Obj);
				break;
			case 2:
				//select
				SelectHandler(Obj);
				break;
			}
		}
		//ボタン
		void PushHandler(const shared_ptr<T>& Obj)
		{
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected)
			{

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B)
				{
					Obj->SetButtonB(true);
				}
				if(CntlVec[0].wReleasedButtons & XINPUT_GAMEPAD_B)
				{
					Obj->SetButtonB(false);
				}

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START)
				{
					Obj->TumiReset(true, 1);
				}

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_BACK)
				{
					Obj->TumiReset(true, 0);
				}
			}
		}

		void EditModeHandler(const shared_ptr<T>& Obj)
		{
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();	
			if (CntlVec[0].bConnected)
			{
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_Y)
				{
					Obj->SetButtonY(true);
				}
				else
				{
					Obj->SetButtonY(false);
				}

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B)
				{
					Obj->SetButtonB(true);
				}
				if (CntlVec[0].wReleasedButtons & XINPUT_GAMEPAD_B)
				{
					Obj->SetButtonB(false);
				}

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A)
				{
					Obj->SetButtonA(true);
				}
				else
				{
					Obj->SetButtonA(false);
				}

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_X)
				{
					Obj->SetChange(Obj->GetChange());
				}

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START)
				{
					Obj->SetStartButton(true);
				}
				else
				{
					Obj->SetStartButton(false);
				}

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_LEFT_SHOULDER)
				{
					Obj->SetObjCount(Obj->GetObjCount() - 1);
				}
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER)
				{
					Obj->SetObjCount(Obj->GetObjCount() + 1);
				}

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_DPAD_UP)
				{
					if (!Obj->GetChange())
					{
						Obj->RotationObj(0);
					}
					else
					{
						Obj->SetSaveCount(Obj->GetSaveCount() + 1);
					}
				}
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_DPAD_RIGHT)
				{
					if (!Obj->GetChange())
					{
						Obj->RotationObj(1);
					}
				}
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_DPAD_DOWN)
				{
					if (!Obj->GetChange())
					{
						Obj->RotationObj(2);
					}
					else
					{
						Obj->SetSaveCount(Obj->GetSaveCount() - 1);
					}
				}
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_DPAD_LEFT)
				{
					if (!Obj->GetChange())
					{
						Obj->RotationObj(3);
					}
				}
			}
		}

		void SelectHandler(const shared_ptr<T>& Obj)
		{
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected)
			{
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B)
				{
					Obj->SetButtonB(true);
				}
				else
				{
					Obj->SetButtonB(false);
				}
			}
		}

		//スティック
		void Move(const shared_ptr<T>& Obj)
		{
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			auto PlayerTrans = Obj->GetComponent<Transform>();
			auto PlayerPos = Obj->GetPosition();
			auto PointRange = Obj->GetOneDis();

			if (PointRange < 10.0f)
			{
				Obj->SetBeforePos(PlayerPos);
				if (CntlVec[0].bConnected)
				{
					if (CntlVec[0].fThumbLX != 0.0f)
					{
						PlayerPos.x += (CntlVec[0].fThumbLX * ElapsedTime * Obj->GetSpeed() * Obj->GetDeceleration());
					}

					if (CntlVec[0].fThumbLY != 0.0f)
					{
						PlayerPos.z += (CntlVec[0].fThumbLY * ElapsedTime * Obj->GetSpeed() * Obj->GetDeceleration());
					}
				}
				Obj->SetPosition(PlayerPos);
			}
		}

		int Choose(int value, int buttonNum, const shared_ptr<T>& Obj)
		{
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			int num = value;
			
			if (CntlVec[0].bConnected)
			{
				if (CntlVec[0].fThumbLX == 1.0f)
				{
					num += 1;
				}
				if (CntlVec[0].fThumbLX == -1.0f)
				{
					num -= 1;
				}
			}

			if (num > buttonNum)
			{
				num = buttonNum;
			}
			if (num < 0)
			{
				num = 0;
			}

			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B)
			{
				Obj->SceneChange();
			}

			return num;
		}
	};
}