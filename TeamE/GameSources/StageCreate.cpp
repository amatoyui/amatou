#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	StageCreate::StageCreate(const shared_ptr<GameStage> StagePtr, const wstring & StageName):
		m_Stage(StagePtr),
		m_StageName(StageName),
		m_RowNum(0),
		m_ColNum(0)
	{}
	void StageCreate::OnCreate()
	{
		wstring DateDir;
		App::GetApp()->GetDataDirectory(DateDir);
		DateDir += L"Csv\\";
		CsvFile File;
		File.SetFileName(DateDir + m_StageName);
		File.ReadCsv();

		//行数の取得
		m_RowNum = File.GetRowCount();

		m_Stage->CreateSharedObjectGroup(L"pointGroup");
		m_Stage->CreateSharedObjectGroup(L"lineGroup");
		for (size_t i = 0; i < m_RowNum; i++)
		{
			//1行にある文字を列に代入
			vector<wstring> RowStr;
			File.GetRowVec(i, RowStr);

			//列の数取得
			m_ColNum = RowStr.size();
			for (size_t j = 0; j < m_ColNum; j++)
			{
				m_Pos = Vec3(static_cast<float>(j), 0.0f, static_cast<float>(i));
				CreateObject(RowStr[j]);
			}
		}
	}

	//オブジェクトの生成用
	void StageCreate::CreateObject(wstring CsvNum)
	{
		//数字だけを受け取る
		auto StrNum = Split(CsvNum, L'_');

		//数字によって生成するオブジェクトを変える
		switch (stoi(StrNum[0]))
		{
		
		case 0:
		{
			//何もなし
			break;
		}
		case 1:
		{
			//プレイヤー
			auto player = m_Stage->AddGameObject<Player>
				(
					Vec3(m_Pos.x, 0.0f,-( m_Pos.z - m_RowNum)),
					Vec3(1.5f, 1.5f, 1.5f),
					Vec3(0, 22.0f, 0), 
					stof(StrNum[1]), 
					L"NIGAKI_TX", 
					PlayerLineState::Instance(),
					stoi(StrNum[2])
				);
			//m_Stage->SetSharedGameObject(L"Player", player);
			player->SetVer(m_ColNum);
			player->SetHori(m_RowNum);
			break;
		}
		case 2:
		{
			// オートキャラクター
			auto Chara = m_Stage->AddGameObject<AutoCharacter>
				(
					Vec3(m_Pos.x, 0.0f,-( m_Pos.z - m_RowNum)), 
					Vec3(0, stof(StrNum[4])* XM_PI / 180.0f, 0),
					Vec3(1.5f, 1.5f, 1.5f), 
					L"DOG_TX", -5.0f, 
					CharaLineState::Instance(), 
					stof(StrNum[1]), 
					stof(StrNum[2]),
					stof(StrNum[3])
				);
			Chara->SetMoveFlag(true);
			//m_Stage->SetSharedGameObject(L"Chara", Chara);
			break;
		}
		case 3:
		{
			//点
			m_Stage->AddGameObject<Point>
				(
					Vec3(m_Pos.x, 0.0f, -(m_Pos.z - m_RowNum)),
					Vec3(1.0f, 1.0f, 1.0f), 
					L"BLUE_TX",
					L"RED_TX"
				);
			break;
		}
		case 4:
		{
			//足場
			auto line = m_Stage->AddGameObject<Line>
				(
					Vec3(m_Pos.x, 0.0f, -(m_Pos.z - m_RowNum)),
					Vec3(DegUp,stof(StrNum[1]) * XM_PI / 180.0f, 0.0f),
					Vec3(1.0f, stof(StrNum[2]), 1.0f),
					L"BLUE_TX"
				);
			line->AddTag(L"line");
			break;
		}
		case 5:
		{
			//ゴール
			auto goal = m_Stage->AddGameObject<Goal>
				(
					Vec3(m_Pos.x, 0.0f, -(m_Pos.z - m_RowNum)),
					Vec3(2.0f, 2.0f, 2.0f), 
					Vec3(0.0f, 0.0f, 0.0f),
					L"YELLOW_TX"
				);
			//m_Stage->SetSharedGameObject(L"Goal", goal);
			break;
		}
		case 6:
		{
			//スタートの足場
			auto Sline = m_Stage->AddGameObject<Line>
				(
					Vec3(m_Pos.x, 0.0f, -(m_Pos.z - m_RowNum)), 
					Vec3(DegUp, stof(StrNum[1]) * XM_PI / 180.0f, 0.0f), 
					Vec3(stof(StrNum[2]), stof(StrNum[3]), 1.0f), 
					L"WHITE_TX"					
				);
			Sline->AddTag(L"line");
			break;
		}	
		case 7:
		{
			//ゴールの足場
			auto Gline = m_Stage->AddGameObject<GoalLine>
				(
					Vec3(m_Pos.x, 0.0f, -(m_Pos.z - m_RowNum)),
					Vec3(DegUp, stof(StrNum[1]) * XM_PI / 180.0f, 0.0f),
					Vec3(stof(StrNum[2]), stof(StrNum[3]), 1.0f),
					L"WHITE_TX"

					);
			Gline->AddTag(L"GLine");
			break;
		}

		}
	}

	vector<wstring> StageCreate::Split(const wstring& ObjNum, wchar_t SplitStr)
	{
		//後で見直す ※
		vector<wstring> res;
		size_t current = 0;
		size_t found;
		while ((found = ObjNum.find_first_of(SplitStr, current)) != wstring::npos)
		{
			res.push_back(wstring(ObjNum, current, found - current));
			current = found + 1;
		}
		res.push_back(wstring(ObjNum, current, ObjNum.size() - current));
		return res;
	}

	XmlAccessor::XmlAccessor(const wstring XmlFileName) :
		m_FileName(XmlFileName),
		m_XmlDoc(XmlFileName),
		m_count(0)
	{} 

	void XmlAccessor::SetObjectData(const shared_ptr<GameObject>& Obj,int StageNum)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto Node = L"GameStage/map" + Util::IntToWStr(1) + L"/GameObject" + Util::IntToWStr(m_count);
			auto ObjectNode = m_XmlDoc.GetSelectSingleNode(Node.c_str());
			m_XmlDoc.SetAttribute(ObjectNode, L"Type", L"Point");
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			//tagのところを入れてほしいtag名にすると無いtagは作られる
			m_XmlDoc.SetAttribute(ObjectNode, L"Pos", Pos.c_str());

			//ノードの追加
			m_XmlDoc.AddChildNode(RootNode, L"GameObject");
			
			auto ObjectNode2 = m_XmlDoc.GetSelectSingleNode(RootNode, L"GameObject");
			m_XmlDoc.SetAttribute(ObjectNode2, L"Pos", Pos.c_str());
			m_XmlDoc.Save(m_FileName, false);
		}
		
	}
	void XmlAccessor::SetPoint(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto scale = Obj->GetComponent<Transform>()->GetScale();
			wstring Scale(L"");
			Scale = Util::FloatToWStr(scale.x) + L',' + Util::FloatToWStr(scale.y) + L',' + Util::FloatToWStr(scale.z);

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Point");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Scale", Scale.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetLine(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto scale = Obj->GetComponent<Transform>()->GetScale();
			wstring Scale(L"");
			Scale = Util::FloatToWStr(scale.x) + L',' + Util::FloatToWStr(scale.y) + L',' + Util::FloatToWStr(scale.z);
			auto rot = Obj->GetComponent<Transform>()->GetRotation();
			wstring Rot(L"");
			Rot = Util::FloatToWStr(rot.x) + L',' + Util::FloatToWStr(rot.y) + L',' + Util::FloatToWStr(rot.z);
			wstring Texture(Obj->GetThis<Line>()->GetTexture());

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Line");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Scale", Scale.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Rot", Rot.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Texture", Texture.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetChara(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto scale = Obj->GetComponent<Transform>()->GetScale();
			wstring Scale(L"");
			Scale = Util::FloatToWStr(scale.x) + L',' + Util::FloatToWStr(scale.y) + L',' + Util::FloatToWStr(scale.z);
			auto speed = Obj->GetThis<AutoCharacter>()->GetSpeed();
			auto moveX = Obj->GetThis<AutoCharacter>()->GetMoveX();
			auto moveZ = Obj->GetThis<AutoCharacter>()->GetMoveZ();

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Chara");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Scale", Scale.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Speed", Util::FloatToWStr(speed).c_str());
			m_XmlDoc.SetAttribute(GetObj, L"MoveX", Util::FloatToWStr(moveX).c_str());
			m_XmlDoc.SetAttribute(GetObj, L"MoveZ", Util::FloatToWStr(moveZ).c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetGoalLine(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto scale = Obj->GetComponent<Transform>()->GetScale();
			wstring Scale(L"");
			Scale = Util::FloatToWStr(scale.x) + L',' + Util::FloatToWStr(scale.y) + L',' + Util::FloatToWStr(scale.z);
			auto rot = Obj->GetComponent<Transform>()->GetRotation();
			wstring Rot(L"");
			Rot = Util::FloatToWStr(rot.x) + L',' + Util::FloatToWStr(rot.y) + L',' + Util::FloatToWStr(rot.z);
			wstring Texture(Obj->GetThis<GoalLine>()->GetTexture());

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Goalline");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Scale", Scale.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Rot", Rot.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Texture", Texture.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetGoal(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto scale = Obj->GetComponent<Transform>()->GetScale();
			wstring Scale(L"");
			Scale = Util::FloatToWStr(scale.x) + L',' + Util::FloatToWStr(scale.y) + L',' + Util::FloatToWStr(scale.z);
			auto rot = Obj->GetComponent<Transform>()->GetRotation();
			wstring Rot(L"");
			Rot = Util::FloatToWStr(rot.x) + L',' + Util::FloatToWStr(rot.y) + L',' + Util::FloatToWStr(rot.z);

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Goal");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Scale", Scale.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Rot", Rot.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetPlayer(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto rot = Obj->GetComponent<Transform>()->GetRotation();
			wstring Rot(L"");
			Rot = Util::FloatToWStr(rot.x) + L',' + Util::FloatToWStr(rot.y) + L',' + Util::FloatToWStr(rot.z);
			auto count = Obj->GetThis<Player>()->GetSaveCount();
			wstring Count(L"");
			Count = Util::IntToWStr(count);

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Player");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Rot", Rot.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"LineCount", Count.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetTurn(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto scale = Obj->GetComponent<Transform>()->GetScale();
			wstring Scale = Util::FloatToWStr(scale.x) + L',' + Util::FloatToWStr(scale.y) + L',' + Util::FloatToWStr(scale.z);

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Turn");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Scale", Scale.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetAway(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto scale = Obj->GetComponent<Transform>()->GetScale();
			wstring Scale = Util::FloatToWStr(scale.x) + L',' + Util::FloatToWStr(scale.y) + L',' + Util::FloatToWStr(scale.z);

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Away");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Scale", Scale.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetClose(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto scale = Obj->GetComponent<Transform>()->GetScale();
			wstring Scale = Util::FloatToWStr(scale.x) + L',' + Util::FloatToWStr(scale.y) + L',' + Util::FloatToWStr(scale.z);

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Close");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Scale", Scale.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetItem(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"Item");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetGoalPanel(const shared_ptr<GameObject>& Obj, int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapNode = L"GameStage/map" + Util::IntToWStr(num);
			auto GetMap = m_XmlDoc.GetSelectSingleNode(MapNode.c_str());
			auto ObjNode = L"GameStage/map" + Util::IntToWStr(num) + L"/GameObject" + Util::IntToWStr(m_count);
			auto GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			//ノードが無かったら
			if (!GetObj)
			{
				//新しいGameObjectNodeを生成
				auto newNode = L"GameObject" + Util::IntToWStr(m_count);
				m_XmlDoc.AddChildNode(GetMap, newNode.c_str());
				GetObj = m_XmlDoc.GetSelectSingleNode(ObjNode.c_str());
			}
			auto pos = Obj->GetComponent<Transform>()->GetPosition();
			wstring Pos(L"");
			Pos = Util::FloatToWStr(pos.x) + L',' + Util::FloatToWStr(pos.y) + L',' + Util::FloatToWStr(pos.z);
			auto scale = Obj->GetComponent<Transform>()->GetScale();
			wstring Scale(L"");
			Scale = Util::FloatToWStr(scale.x) + L',' + Util::FloatToWStr(scale.y) + L',' + Util::FloatToWStr(scale.z);
			auto rot = Obj->GetComponent<Transform>()->GetRotation();
			wstring Rot(L"");
			Rot = Util::FloatToWStr(rot.x) + L',' + Util::FloatToWStr(rot.y) + L',' + Util::FloatToWStr(rot.z);
			wstring Texture(Obj->GetThis<GoalPanel>()->GetTexture());

			m_XmlDoc.SetAttribute(GetObj, L"Type", L"GoalPanel");
			m_XmlDoc.SetAttribute(GetObj, L"Pos", Pos.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Scale", Scale.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Rot", Rot.c_str());
			m_XmlDoc.SetAttribute(GetObj, L"Texture", Texture.c_str());

			m_count++;
			//保存する
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::SetStageElement(int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if(RootNode)
		{
			auto MapPath = L"GameStage/map" + Util::IntToWStr(num);
			auto MapNode = m_XmlDoc.GetSelectSingleNode(MapPath.c_str());
			auto count = Util::IntToWStr(m_count);
			auto scene = App::GetApp()->GetScene<Scene>();
			auto ver = scene->GetVer();
			wstring Ver = Util::IntToWStr(ver);
			auto hori = scene->GetHori();
			wstring Hori = Util::IntToWStr(hori);
			auto time = scene->GetTime();
			wstring Time = Util::IntToWStr(time);

			m_XmlDoc.SetAttribute(MapNode, L"Count", count.c_str());
			m_XmlDoc.SetAttribute(MapNode, L"Ver", Ver.c_str());
			m_XmlDoc.SetAttribute(MapNode, L"Hori", Hori.c_str());
			m_XmlDoc.SetAttribute(MapNode, L"ComeCount", Time.c_str());
			m_XmlDoc.Save(m_FileName, false);
			
		}
	}

	void XmlAccessor::MapCreate()
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			for (int i = 1; i <= 100; i++)
			{
				auto MapPath = L"GameStage/map" + Util::IntToWStr(i);
				auto MapNode = m_XmlDoc.GetSelectSingleNode(MapPath.c_str());
				if (!MapNode)
				{
					//新しいGameObjectNodeを生成
					auto newNode = L"map" + Util::IntToWStr(i);
					m_XmlDoc.AddChildNode(RootNode, newNode.c_str());
					MapNode = m_XmlDoc.GetSelectSingleNode(MapPath.c_str());
				}
			}
		}
	}

	int XmlAccessor::GetCount(int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapPath = L"GameStage/map" + Util::IntToWStr(num) + L"/@Count";
			auto MapNode = m_XmlDoc.GetSelectSingleNode(MapPath.c_str());
			int count = stoi(m_XmlDoc.GetText(MapNode));
			return count;
		}
		return 0;
	}

	int XmlAccessor::GetVer(int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapPath = L"GameStage/map" + Util::IntToWStr(num) + L"/@Ver";
			auto MapNode = m_XmlDoc.GetSelectSingleNode(MapPath.c_str());
			int ver = stoi(m_XmlDoc.GetText(MapNode));
			return ver;
		}
		return 0;
	}

	int XmlAccessor::GetHori(int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapPath = L"GameStage/map" + Util::IntToWStr(num) + L"/@Hori";
			auto MapNode = m_XmlDoc.GetSelectSingleNode(MapPath.c_str());
			int hori = stoi(m_XmlDoc.GetText(MapNode));
			return hori;
		}
		return 0;
	}

	int XmlAccessor::GetTime(int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapPath = L"GameStage/map" + Util::IntToWStr(num) + L"/@ComeCount";
			auto MapNode = m_XmlDoc.GetSelectSingleNode(MapPath.c_str());
			int time = stoi(m_XmlDoc.GetText(MapNode));
			return time;
		}
		return 0;
	}

	void XmlAccessor::Remove(int num)
	{
		auto RootNode = m_XmlDoc.GetSelectSingleNode(L"GameStage");
		if (RootNode)
		{
			auto MapPath = L"GameStage/map" + Util::IntToWStr(num);
			auto MapNode = m_XmlDoc.GetSelectSingleNode(MapPath.c_str());
			m_XmlDoc.Save(m_FileName, false);
		}
	}

	void XmlAccessor::CreateStageXml(wstring XmlName)
	{
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		m_XmlDocReader.reset(new XmlDocReader(DataDir + XmlName));
		//Stageのノードを取得
		auto stage_node = m_XmlDocReader->GetSelectSingleNode(L"GameStage");

		//文字列を取得
		wstring MapStr = XmlDocReader::GetText(stage_node);
		vector<wstring> LineVec;
		//改行を区切りとした文字列を配列にする
		Util::WStrToTokenVector(LineVec, MapStr, L'\n');
		for (size_t i = 0; i < LineVec.size(); i++)
		{
			vector<wstring> Tokens;
			Util::WStrToTokenVector(Tokens, LineVec[i], L',');
			for (size_t j = 0; j < Tokens.size(); j++)
			{
				float PosX = static_cast<float>(static_cast<int>(j));
				float PosZ = static_cast<float>(static_cast<int>(j));
				switch (stoi(Tokens[j]))
				{
				case 0:
					//何もしない
					break;
				case 1:
					//プレイヤー
					break;
				case 2:
					//こっけん
					break;
				case 3:
					//点
					break;
				case 4:
					//足場
					break;
				case 5:
					//ゴール
					break;
				case 6:
					//スタート・ゴール用の足場
					break;
				}
			}
		}
	}
}
