/*!
@file Character.cpp
@brief キャラクターなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Area : public GameObject
	//	用途: 引ける範囲を表示
	//--------------------------------------------------------------------------------------
	Area::Area(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const Vec3 & StartScale, const Vec3 & StartRot, const wstring & TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Rot(StartRot), m_Scale(StartScale), m_Texture(TexturePtr)
	{}

	void Area::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(m_Rot);
		PtrTrans->SetScale(m_Scale);

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		PtrDraw->SetTextureResource(m_Texture);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class Point : public GameObject
	//	用途: 点
	//--------------------------------------------------------------------------------------
	Point::Point(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const Vec3 & StartScale, const wstring & TexturePtr, const wstring& ChooseTexture) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Texture(TexturePtr), m_ChooseTexture(ChooseTexture), m_GoalFlag(false)
	{}

	Point::Point(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node) :
		GameObject(StagePtr)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
		auto ScaleStr = XmlDocReader::GetAttribute(Node, L"Scale");
		vector<wstring> ScaleT;
		Util::WStrToTokenVector(ScaleT, ScaleStr, L',');
		m_Scale = Vec3
		(
			static_cast<float>(_wtof(ScaleT[0].c_str())),
			static_cast<float>(_wtof(ScaleT[1].c_str())),
			static_cast<float>(_wtof(ScaleT[2].c_str()))
		);

		m_Texture = L"BLUE_TX";
		m_ChooseTexture = L"RED_TX";
		
	}

	void Point::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetScale(m_Scale);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);

		//衝突判定
		auto PtrCol = AddComponent<CollisionSphere>();
		PtrCol->SetIsHitAction(IsHitAction::None);
		//PtrCol->SetDrawActive(true);//※

		//描画
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(m_Texture);

		auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
		Group->IntoGroup(GetThis<Point>());

		SetAlphaActive(true);

		//ステートマシンの構築
		m_StateMachine.reset(new LayeredStateMachine<Point>(GetThis<Point>()));
		//ステート初期化
		m_StateMachine->Reset(PointDefaultState::Instance());

		AddTag(L"Point");
	}

	void Point::OnUpdate()
	{
		//ステートマシン更新
		m_StateMachine->Update();
	}

	void Point::OnCollision(vector<shared_ptr<GameObject>>& OtherVec)
	{
		for (auto v : OtherVec)
		{
			//if (v->FindTag(L"Line") || v->FindTag(L"RowLine"))
			if (v->FindTag(L"Line") || v->FindTag(L"line"))
			{
				m_LinePos.push_back(v->GetComponent<Transform>()->GetPosition());
				m_LineDirection.push_back(Direction(v->GetComponent<Transform>()->GetPosition()));
				//m_LineLength.push_back(v->GetComponent<Transform>()->GetScale().y);
				m_LineLength.push_back(v->GetComponent<Transform>()->GetScale().x);
			}

			if (v->FindTag(L"Goalline"))
			{
				m_GoalLinePos = v->GetComponent<Transform>()->GetPosition();
				m_GoalLineDirection = Direction(v->GetComponent<Transform>()->GetPosition());
				m_GoalLineLength = v->GetComponent<Transform>()->GetScale().y;
				m_GoalFlag = true;
			}
		}
	}

	Vec3 Point::Direction(Vec3 LineVec)
	{
		auto pos = GetComponent<Transform>()->GetPosition();
		auto direction = (LineVec - pos) * 10000;
		direction.normalize();
		//m_LineDirection.push_back(direction);
		return direction;
	}

	//----------------------------------------------------------------------
	///点の通常ステート
	//----------------------------------------------------------------------
	IMPLEMENT_SINGLETON_INSTANCE(PointDefaultState)
		void PointDefaultState::Enter(const shared_ptr<Point>& Obj)
	{
		auto PtrDraw = Obj->GetComponent<PNTStaticDraw>();
		PtrDraw->SetTextureResource(Obj->GetDefaultTexture());
	}

	void PointDefaultState::Execute(const shared_ptr<Point>& Obj)
	{
	}

	void PointDefaultState::Exit(const shared_ptr<Point>& Obj)
	{
	}


	//----------------------------------------------------------------------
	///選択されたときのステート
	//----------------------------------------------------------------------
	IMPLEMENT_SINGLETON_INSTANCE(PointChooseState)
		void PointChooseState::Enter(const shared_ptr<Point>& Obj)
	{
		auto PtrDraw = Obj->GetComponent<PNTStaticDraw>();
		PtrDraw->SetTextureResource(Obj->GetChooseTexture());
	}

	void PointChooseState::Execute(const shared_ptr<Point>& Obj)
	{
	}

	void PointChooseState::Exit(const shared_ptr<Point>& Obj)
	{
	}

	//---------------------------------------------------------------------
	//	class TurnGousun : public GameObject
	//	用途 : X方向に往復する敵
	//---------------------------------------------------------------------
	TurnGousun::TurnGousun(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartRot, const Vec3& StartScale, const wstring& TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Rot(StartRot), m_Scale(StartScale), m_Texture(TexturePtr)
	{}

	TurnGousun::TurnGousun(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node) :
		GameObject(StagePtr)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
		auto ScaleStr = XmlDocReader::GetAttribute(Node, L"Scale");
		vector<wstring> ScaleT;
		Util::WStrToTokenVector(ScaleT, ScaleStr, L',');
		m_Scale = Vec3
		(
			static_cast<float>(_wtof(ScaleT[0].c_str())),
			static_cast<float>(_wtof(ScaleT[1].c_str())),
			static_cast<float>(_wtof(ScaleT[2].c_str()))
		);

		m_Rot = Vec3(DegUp, 0.0f, 0.0f);

		m_Texture = L"GOUSUN_TX";

		m_Move = true;
	}

	void TurnGousun::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(m_Rot);
		PtrTrans->SetScale(m_Scale);

		//描画
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(m_Texture);
		PtrDraw->SetEmissive(Col4(0.0f, 1.0f, 0.0f, 1.0f));

		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::None);

		auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
		Group->IntoGroup(GetThis<TurnGousun>());

		SetAlphaActive(true);
	}

	void TurnGousun::OnUpdate()
	{
		if (m_Move)
		{
			auto elepasdtime = App::GetApp()->GetElapsedTime();

			if (!on) {
				velo += 2.0f * elepasdtime;
			}
			if (on) {
				velo -= 2.0f * elepasdtime;
			}

			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetPosition(m_Pos.x + velo, 0.0f, PtrTrans->GetPosition().z);
			if (velo >= 5.0f)
			{
				on = true;
			}
			else if (velo <= -5.0f)
			{
				on = false;
			}
		}
	}

	void TurnGousun::OnCollision(vector<shared_ptr<GameObject>>& OtherVec)
	{
		for (auto& v : OtherVec)
		{
			if (v->FindTag(L"Player") && v->GetThis<Player>()->GetStateMachine()->GetTopState() != EditModeState::Instance())
			{
				GetStage()->RemoveGameObject<TurnGousun>(GetThis<TurnGousun>());
				v->GetThis<Player>()->SetDeceleration(0.5f);
			}

			if (v->FindTag(L"Chara"))
			{
				GetStage()->RemoveGameObject<TurnGousun>(GetThis<TurnGousun>());
				v->GetThis<AutoCharacter>()->SetDead(true);
			}
		}
	}
	//----------------------------------------------------------------------
	//	class CloseGousun : public GameObject
	//	用途 : 近づく敵
	//----------------------------------------------------------------------
	CloseGousun::CloseGousun(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartRot, const Vec3& StartScale, const wstring& TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Rot(StartRot), m_Scale(StartScale), m_Texture(TexturePtr)
	{}

	CloseGousun::CloseGousun(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node):
		GameObject(StagePtr)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
		auto ScaleStr = XmlDocReader::GetAttribute(Node, L"Scale");
		vector<wstring> ScaleT;
		Util::WStrToTokenVector(ScaleT, ScaleStr, L',');
		m_Scale = Vec3
		(
			static_cast<float>(_wtof(ScaleT[0].c_str())),
			static_cast<float>(_wtof(ScaleT[1].c_str())),
			static_cast<float>(_wtof(ScaleT[2].c_str()))
		);

		m_Rot = Vec3(DegUp, 0.0f, 0.0f);

		m_Texture = L"GOUSUN_TX";

		m_Move = true;
	}

	void CloseGousun::OnCreate()
	{
		//トランスフォーム
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(m_Rot);
		PtrTrans->SetScale(m_Scale);

		//描画
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(m_Texture);

		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::None);

		auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
		Group->IntoGroup(GetThis<CloseGousun>());

		SetAlphaActive(true);
	}

	void CloseGousun::OnUpdate()
	{
		if (m_Move)
		{
			auto PtrTrans = AddComponent<Transform>();
			auto ela = App::GetApp()->GetElapsedTime();

			//プレイヤーのポジション取得
			auto pPos = GetStage()->GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetPosition();
			m_PlayerPos = pPos;

			//X軸
			if (m_Pos.x < m_PlayerPos.x)
			{
				PtrTrans->SetPosition(m_Pos.x += 1.0f * ela, 0, m_Pos.z);
			}
			else if (m_Pos.x > m_PlayerPos.x)
			{
				PtrTrans->SetPosition(m_Pos.x -= 1.0f * ela, 0, m_Pos.z);
			}

			//Z軸
			if (m_Pos.z < m_PlayerPos.z)
			{
				PtrTrans->SetPosition(m_Pos.x, 0, m_Pos.z += 0.5f * ela);
			}
			else if (m_Pos.z > m_PlayerPos.z)
			{
				PtrTrans->SetPosition(m_Pos.x, 0, m_Pos.z -= 0.5f * ela);
			}
		}
	}

	void CloseGousun::OnCollision(vector<shared_ptr<GameObject>>& OtherVec)
	{
		for (auto& v : OtherVec)
		{
			if (v->FindTag(L"Player") && v->GetThis<Player>()->GetStateMachine()->GetTopState() != EditModeState::Instance())
			{
				GetStage()->RemoveGameObject<CloseGousun>(GetThis<CloseGousun>());
				v->GetThis<Player>()->SetDeceleration(0.5f);
			}

			if (v->FindTag(L"Chara"))
			{
				GetStage()->RemoveGameObject<CloseGousun>(GetThis<CloseGousun>());
				v->GetThis<AutoCharacter>()->SetDead(true);
			}
		}
	}

	//----------------------------------------------------------------------
	//	class AwayGousun : public GameObject
	//	用途 : 逃げる敵
	//----------------------------------------------------------------------
	AwayGousun::AwayGousun(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartRot, const Vec3& StartScale, const wstring& TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Rot(StartRot), m_Scale(StartScale), m_Texture(TexturePtr)
	{}

	AwayGousun::AwayGousun(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node):
		GameObject(StagePtr)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
		auto ScaleStr = XmlDocReader::GetAttribute(Node, L"Scale");
		vector<wstring> ScaleT;
		Util::WStrToTokenVector(ScaleT, ScaleStr, L',');
		m_Scale = Vec3
		(
			static_cast<float>(_wtof(ScaleT[0].c_str())),
			static_cast<float>(_wtof(ScaleT[1].c_str())),
			static_cast<float>(_wtof(ScaleT[2].c_str()))
		);

		m_Rot = Vec3(DegUp, 0.0f, 0.0f);

		m_Texture = L"GOUSUN_TX";

		m_Move = true;
	}

	float DiffeX = 0;
	float DiffeZ = 0;

	void AwayGousun::OnCreate()
	{
		//トランスフォーム
		auto TransPtr = AddComponent<Transform>();
		TransPtr->SetPosition(m_Pos);
		TransPtr->SetRotation(m_Rot);
		TransPtr->SetScale(m_Scale);

		//描画
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(m_Texture);
		//PtrDraw->SetDiffuse(Col4(0.0f, 0.0f, 1.0f, 1.0f));
		PtrDraw->SetEmissive(Col4(0.0f, 0.0f, 1.0f, 1.0f));

		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::None);

		auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
		Group->IntoGroup(GetThis<AwayGousun>());

		SetAlphaActive(true);
	}

	void AwayGousun::OnUpdate()
	{
		if (m_Move)
		{
			auto PtrTrans = AddComponent<Transform>();
			auto ela = App::GetApp()->GetElapsedTime();

			auto pPos = GetStage()->GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetPosition();
			m_PlayerPos = pPos;

			DiffeX = m_Pos.x - m_PlayerPos.x;
			DiffeZ = m_Pos.z - m_PlayerPos.z;

			if (DiffeX > 5 || DiffeX < -5)
			{
				//PtrTrans->SetPosition(m_Pos.x += -1.0f * ela, 0, 0);
			}

			//X軸プラスのとき
			if (DiffeX > 0 && DiffeX > 4.9)
			{
				PtrTrans->SetPosition(m_Pos.x += -1.0f * ela, 0, m_Pos.z);
			}
			else if (DiffeX > 0 && DiffeX < 4.9)
			{
				PtrTrans->SetPosition(m_Pos.x += 1.0f * ela, 0, m_Pos.z);
			}

			//X軸マイナスのとき
			if (DiffeX < 0 && DiffeX > -4.9)
			{
				PtrTrans->SetPosition(m_Pos.x += -1.0f * ela, 0, m_Pos.z);
			}
			else if (DiffeX < 0 && DiffeX < -4.9)
			{
				PtrTrans->SetPosition(m_Pos.x += 1.0f * ela, 0, m_Pos.z);
			}

			//Z軸プラスのとき
			if (DiffeZ > 0 && DiffeZ > 4.9)
			{
				PtrTrans->SetPosition(m_Pos.x, 0, m_Pos.z += -1.0f * ela);
			}
			else if (DiffeZ > 0 && DiffeZ < 4.9)
			{
				PtrTrans->SetPosition(m_Pos.x, 0, m_Pos.z += 1.0f * ela);
			}
			//Z軸マイナスのとき
			if (DiffeZ < 0 && DiffeZ > -4.9)
			{
				PtrTrans->SetPosition(m_Pos.x, 0, m_Pos.z += -1.0f * ela);
			}
			else if (DiffeZ < 0 && DiffeZ < -4.9)
			{
				PtrTrans->SetPosition(m_Pos.x, 0, m_Pos.z += 1.0f * ela);

			}
		}
	}

	void AwayGousun::OnCollision(vector<shared_ptr<GameObject>>& OtherVec)
	{
		for (auto& v : OtherVec)
		{
			if (v->FindTag(L"Player") && v->GetThis<Player>()->GetStateMachine()->GetTopState() != EditModeState::Instance())
			{
				GetStage()->RemoveGameObject<AwayGousun>(GetThis<AwayGousun>());
				v->GetThis<Player>()->SetDeceleration(0.5f);
			}

			if (v->FindTag(L"Chara"))
			{
				GetStage()->RemoveGameObject<AwayGousun>(GetThis<AwayGousun>());
				v->GetThis<AutoCharacter>()->SetDead(true);
			}
		}
	}

	//----------------------------------------------------------------------
	//	class CameraBox : public GameObject
	//	用途 : ゲームスタート時のカメラ演出
	//----------------------------------------------------------------------
	CameraBox::CameraBox(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_Pos(0.0f, 0.0f, 0.0f),
		m_Pos2(0.0f, 0.0f, 0.0f),
		m_PlayerPos(),
		m_KokkenPos(),
		m_GoalPos(),
		k_Count(0),
		k_rim(200),
		g_Count(0),
		g_rim(100),
		m_CamSpX(10),
		m_CamSpZ(10),
		StartOn(false),
		Xok(false),
		Zok(false)
	{}

	void CameraBox::OnCreate()
	{
		//こっけんの位置を取得
		KokkenPtr = GetStage()->GetSharedGameObject<AutoCharacter>(L"Chara");
		m_KokkenPos = KokkenPtr->GetComponent<Transform>()->GetPosition();
		//m_KokkenPos.x += 10.0f;	//こっけんを見たいためずらす
	   // m_KokkenPos.z += 5.0f;
		m_KokkenPos.y += 5.0f;	//上から見下ろす用

		//ずらした位置を記憶
		m_Pos = m_KokkenPos;
		//元の場所をもう一度取得
		m_KokkenPos = KokkenPtr->GetComponent<Transform>()->GetPosition();
		m_KokkenPos.z += 5.0f;

		//プレイヤーの位置を取得
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		m_PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();

		//ゴール位置を取得
		auto GoalPtr = GetStage()->GetSharedGameObject<Goal>(L"Goal");
		m_GoalPos = GoalPtr->GetComponent<Transform>()->GetPosition();

		//Transformの取得
		auto TransPtr = AddComponent<Transform>();
		//TransPtr->SetPosition(Vec3(0,50.0f,-1.0f));

		auto DrawPtr = AddComponent<PNTStaticDraw>();
		DrawPtr->SetMeshResource(L"DEFAULT_CUBE");
		DrawPtr->SetDrawActive(false);
		TransPtr->SetPosition(m_Pos);
		m_Pos = TransPtr->GetPosition();

		GetStage()->SetSharedGameObject(L"CameraBox", GetThis<CameraBox>());

		//メインカメラの取得
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		if (PtrCamera) {
			//CameraBoxさんをカメラターゲットに設定し、こっけんの位置を見る
			PtrCamera->SetTargetObject(GetThis<GameObject>());
			PtrCamera->SetEye(m_Pos.x, m_Pos.y + 1.0f, m_Pos.z);
			PtrCamera->SetAt(m_KokkenPos);
		}
	}

	void CameraBox::OnUpdate()
	{

		//メインカメラの取得
		auto CameraPtr = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		CharaStop(CameraPtr->GetStart());

		//トランスフォームの取得
		auto TransPtr = GetComponent<Transform>();

		//elapasedtimeの取得
		auto ela = App::GetApp()->GetElapsedTime();

		//こっけんへの注視時間を加算
		k_Count++;

		//こっけんカウントが限界値を越え、フラグが立っていないとき、カメラのStartフラグがfalseのときゴールへカメラをシフト
		if (k_Count > k_rim && !StartOn && !CameraPtr->GetStart() && !CameraPtr->GetGoalFlag())
		{
			//ゴールの位置にCameraBoxの位置を設定
			m_Pos = m_GoalPos;
			m_Pos.y += 20.0f;
			m_Pos.z += -5.0f;
			TransPtr->SetPosition(m_Pos);
			CameraPtr->SetEye(m_Pos);
			m_Pos2 = m_GoalPos;
			CameraPtr->SetAt(m_Pos2);
			StartOn = true;
		}
		if (StartOn)
		{
			//こっけんの動きを止める
			//
			KokkenPtr->GetThis<AutoCharacter>()->SetMoveFlag(CameraPtr->GetStart());
			g_Count++;
			if (g_Count > g_rim)
			{
				//---------------------------------------------------------------------------------
				//�@ゴールのzがプレイヤーよりも大きい、ゴールのxがプレイヤーよりも大きい
				if (m_GoalPos.z > m_PlayerPos.z && m_GoalPos.x > m_PlayerPos.x)
				{
					m_Pos.z += -m_CamSpZ * ela;
					m_Pos.x += -m_CamSpX * ela;
					m_Pos2.z += -m_CamSpZ * ela;
					m_Pos2.x += -m_CamSpX * ela;

					CameraPtr->SetEye(m_Pos);
					CameraPtr->SetAt(m_Pos2);
					//二つの軸の移動を確認する
					if (m_Pos2.z < m_PlayerPos.z)
					{
						Zok = true;
						m_CamSpZ = 0;
					}
					if (m_Pos2.x < m_PlayerPos.x)
					{
						Xok = true;
						m_CamSpX = 0;
					}
					if (Xok && Zok)
					{
						auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
						CameraPtr->SetTargetObject(PlayerPtr);
						CameraPtr->SetStart(true);
						CameraPtr->SetObjMove(true);
						auto TimeSystem = GetStage()->GetSharedGameObject<TimeSprite>(L"TimeSprite");
						TimeSystem->SetStart(true);

						StartOn = false;
					}
				}
				//---------------------------------------------------------------------------------
				//�Aゴールのzがプレイヤーよりも大きい、ゴールのxがプレイヤーよりも小さい
				if (m_GoalPos.z > m_PlayerPos.z && m_GoalPos.x < m_PlayerPos.x)
				{
					m_Pos.z += -m_CamSpZ * ela;
					m_Pos.x += m_CamSpX * ela;
					m_Pos2.z += -m_CamSpZ * ela;
					m_Pos2.x += m_CamSpX * ela;

					CameraPtr->SetEye(m_Pos);
					CameraPtr->SetAt(m_Pos2);
					//二つの軸の移動を確認する
					if (m_Pos2.z < m_PlayerPos.z)
					{
						Zok = true;
						m_CamSpZ = 0;
					}
					if (m_Pos2.x > m_PlayerPos.x)
					{
						Xok = true;
						m_CamSpX = 0;
					}
					if (Xok && Zok)
					{
						auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
						CameraPtr->SetTargetObject(PlayerPtr);
						CameraPtr->SetStart(true);
						auto TimeSystem = GetStage()->GetSharedGameObject<TimeSprite>(L"TimeSprite");
						TimeSystem->SetStart(true);

						StartOn = false;
					}
				}
				//---------------------------------------------------------------------------------
				//�Bゴールのzがプレイヤーよりも小さい、ゴールのxがプレイヤーよりも大きい
				if (m_GoalPos.z < m_PlayerPos.z && m_GoalPos.x > m_PlayerPos.x)
				{
					m_Pos.z += m_CamSpZ * ela;
					m_Pos.x += -m_CamSpX * ela;
					m_Pos2.z += m_CamSpZ * ela;
					m_Pos2.x += -m_CamSpX * ela;

					CameraPtr->SetEye(m_Pos);
					CameraPtr->SetAt(m_Pos2);
					//二つの軸の移動を確認する
					if (m_Pos2.z > m_PlayerPos.z)
					{
						Zok = true;
						m_CamSpZ = 0;
					}
					if (m_Pos2.x < m_PlayerPos.x)
					{
						Xok = true;
						m_CamSpX = 0;
					}
					if (Xok && Zok)
					{
						auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
						CameraPtr->SetTargetObject(PlayerPtr);
						CameraPtr->SetStart(true);
						auto TimeSystem = GetStage()->GetSharedGameObject<TimeSprite>(L"TimeSprite");
						TimeSystem->SetStart(true);

						StartOn = false;
					}
				}
				//---------------------------------------------------------------------------------
				//�Cゴールのzがプレイヤーよりも小さい、ゴールのxがプレイヤーよりも小さい
				if (m_GoalPos.z < m_PlayerPos.z && m_GoalPos.x < m_PlayerPos.x)
				{
					m_Pos.z += m_CamSpZ * ela;
					m_Pos.x += m_CamSpX * ela;
					m_Pos2.z += m_CamSpZ * ela;
					m_Pos2.x += m_CamSpX * ela;

					CameraPtr->SetEye(m_Pos);
					CameraPtr->SetAt(m_Pos2);
					//二つの軸の移動を確認する
					if (m_Pos2.z > m_PlayerPos.z)
					{
						Zok = true;
						m_CamSpZ = 0;
					}
					if (m_Pos2.x > m_PlayerPos.x)
					{
						Xok = true;
						m_CamSpX = 0;
					}
					if (Xok && Zok)
					{
						auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
						CameraPtr->SetTargetObject(PlayerPtr);
						CameraPtr->SetStart(true);
						auto TimeSystem = GetStage()->GetSharedGameObject<TimeSprite>(L"TimeSprite");
						TimeSystem->SetStart(true);

						StartOn = false;
					}
				}
				
			}
		}
	}

	void CameraBox::CharaStop(bool flag)
	{
		auto edit = GetStage()->GetSharedObjectGroup(L"EditGroup");
		for (auto& v : edit->GetGroupVector())
		{
			auto shPtr = v.lock();
			if (shPtr)
			{
				auto turn = dynamic_pointer_cast<TurnGousun>(shPtr);
				if (turn)
				{
					turn->SetMove(flag);
				}

				auto close = dynamic_pointer_cast<CloseGousun>(shPtr);
				if (close)
				{
					close->SetMove(flag);
				}

				auto away = dynamic_pointer_cast<AwayGousun>(shPtr);
				if (away)
				{
					away->SetMove(flag);
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class Line : public GameObject
	//	用途: 線
	//--------------------------------------------------------------------------------------
	Line::Line(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const Vec3 & StartRot, const Vec3 & StartScale, const wstring & TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Rot(StartRot), m_Scale(StartScale), m_Texture(TexturePtr)
	{}

	Line::Line(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node) :
		GameObject(StagePtr)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),
			//-0.1f,
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
		auto ScaleStr = XmlDocReader::GetAttribute(Node, L"Scale");
		vector<wstring> ScaleT;
		Util::WStrToTokenVector(ScaleT, ScaleStr, L',');
		m_Scale = Vec3
		(
			static_cast<float>(_wtof(ScaleT[0].c_str())),
			static_cast<float>(_wtof(ScaleT[1].c_str())),
			static_cast<float>(_wtof(ScaleT[2].c_str()))
		);
		auto RotStr = XmlDocReader::GetAttribute(Node, L"Rot");
		vector<wstring> RotT;
		Util::WStrToTokenVector(RotT, RotStr, L',');
		m_Rot = Vec3
		{
			0.0f,
			static_cast<float>(_wtof(RotT[1].c_str())),
			0.0f
		};
		auto TexStr = XmlDocReader::GetAttribute(Node, L"Texture");
		m_Texture = TexStr;
	}

	void Line::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(m_Rot);
		PtrTrans->SetScale(m_Scale);

		//衝突判定
		auto PtrCol = AddComponent<CollisionRect>();

		//描画
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(m_Texture);

		AddTag(L"line");

		SetGroup();

		SetAlphaActive(true);
	}

	void Line::OnCollision(vector<shared_ptr<GameObject>>& OtherVec)
	{
		for (auto v : OtherVec)
		{
			if (v->FindTag(L"line"))
			{
				//auto player = GetStage()->GetSharedGameObject<Player>(L"Player");
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class GoalLine : public GameObject
	//	用途: 線
	//--------------------------------------------------------------------------------------
	GoalLine::GoalLine(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const Vec3 & StartRot, const Vec3 & StartScale, const wstring & TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Rot(StartRot), m_Scale(StartScale), m_Texture(TexturePtr)
	{}

	GoalLine::GoalLine(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node) :
		GameObject(StagePtr)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),

			//-0.1f,
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
		auto ScaleStr = XmlDocReader::GetAttribute(Node, L"Scale");
		vector<wstring> ScaleT;
		Util::WStrToTokenVector(ScaleT, ScaleStr, L',');
		m_Scale = Vec3
		(
			static_cast<float>(_wtof(ScaleT[0].c_str())),
			static_cast<float>(_wtof(ScaleT[1].c_str())),
			static_cast<float>(_wtof(ScaleT[2].c_str()))
		);
		auto RotStr = XmlDocReader::GetAttribute(Node, L"Rot");
		vector<wstring> RotT;
		Util::WStrToTokenVector(RotT, RotStr, L',');
		m_Rot = Vec3
		{
			static_cast<float>(_wtof(RotT[0].c_str())),
			static_cast<float>(_wtof(RotT[1].c_str())),
			static_cast<float>(_wtof(RotT[2].c_str()))
		};
		auto TexStr = XmlDocReader::GetAttribute(Node, L"Texture");
		m_Texture = TexStr;
	}

	void GoalLine::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(0.0f, m_Rot.y, 0.0f);
		PtrTrans->SetScale(m_Scale);

		//衝突判定
		auto PtrCol = AddComponent<CollisionRect>();

		//描画
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(m_Texture);

		AddTag(L"Goalline");

		SetGroup();
		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class ThroughLine : public GameObject
	//	用途: 線
	//--------------------------------------------------------------------------------------
	ThroughLine::ThroughLine(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const Vec3 & StartRot, const Vec3 & StartScale, const wstring & TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Rot(StartRot), m_Scale(StartScale), m_Texture(TexturePtr)
	{}

	void ThroughLine::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(m_Rot);
		PtrTrans->SetScale(m_Scale);

		//衝突判定
		auto PtrCol = AddComponent<CollisionRect>();

		//描画
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		PtrDraw->SetTextureResource(m_Texture);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class Goal : public GameObject
	//	用途: ゴール
	//--------------------------------------------------------------------------------------
	Goal::Goal(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const Vec3 & StartScale, const Vec3& StartRot, const wstring TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Texture(TexturePtr), m_Rot(StartRot)
	{}

	Goal::Goal(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node) :
		GameObject(StagePtr), m_Texture(L"YELLOW_TX")
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
		auto ScaleStr = XmlDocReader::GetAttribute(Node, L"Scale");
		vector<wstring> ScaleT;
		Util::WStrToTokenVector(ScaleT, ScaleStr, L',');
		m_Scale = Vec3
		(
			static_cast<float>(_wtof(ScaleT[0].c_str())),
			static_cast<float>(_wtof(ScaleT[1].c_str())),
			static_cast<float>(_wtof(ScaleT[2].c_str()))
		);
		auto RotStr = XmlDocReader::GetAttribute(Node, L"Rot");
		vector<wstring> RotT;
		Util::WStrToTokenVector(RotT, RotStr, L',');
		m_Rot = Vec3
		(
			static_cast<float>(_wtof(RotT[0].c_str())),
			static_cast<float>(_wtof(RotT[1].c_str())),
			static_cast<float>(_wtof(RotT[2].c_str()))
		);
	}

	void Goal::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetScale(m_Scale);
		PtrTrans->SetRotation(m_Rot);

		//モデル用トランスフォーム
		Mat4x4 mat;
		mat.affineTransformation
		(
			Vec3(1.0f, 1.0f, 1.0f),//scale
			Vec3(0.0f, 0.0f, 0.0f),//rotationの原点
			Vec3(0.0f, 90.0f * XM_PI / 180.0f, 0.0f),//rotation
			Vec3(0.0f, 0.0f, 0.0f)//position
		);

		auto PtrCol = AddComponent<CollisionObb>();

		auto PtrDraw = AddComponent<BcPNTStaticModelDraw>();
		PtrDraw->SetMeshResource(L"Goal");
		PtrDraw->SetMeshToTransformMatrix(mat);

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Fanfare");

		auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
		Group->IntoGroup(GetThis<Goal>());

		GetStage()->SetSharedGameObject(L"Goal", GetThis<Goal>());
		AddTag(L"Gorl");
	}

	void Goal::OnCollision(vector<shared_ptr<GameObject>>& OtherVec)
	{
		for (auto& v : OtherVec)
		{
			if (v->FindTag(L"Chara"))
			{
				v->GetThis<AutoCharacter>()->SetMoveFlag(false);
				auto pos = GetComponent<Transform>()->GetPosition();
				auto scene = App::GetApp()->GetScene<Scene>();
				auto player = GetStage()->GetSharedGameObject<Player>(L"Player");
				player->SetUpdateActive(false);
				auto PtrCamera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
				PtrCamera->SetGoalFlag(true);
				PtrCamera->SetGoalPos(pos);
				PtrCamera->SetTargetObject(GetThis<Goal>());
				v->GetThis<AutoCharacter>()->GoalMove();
				auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Fanfare");
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class SelectDoor : public GameObject
	//	用途: ステージセレクトに置くドア
	//--------------------------------------------------------------------------------------
	SelectDoor::SelectDoor(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const wstring TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Texture(TexturePtr), FadeOn(false)
	{}

	void SelectDoor::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetScale(2.6f, 3.27f, 1.0f);
		PtrTrans->SetRotation(DegUp, 0.0f, 0.0f);

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		PtrDraw->SetTextureResource(m_Texture);

		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::None);

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Knock");

		AddTag(L"Door");
		SetAlphaActive(true);
	}

	void SelectDoor::OnUpdate()
	{
		auto scene = App::GetApp()->GetScene<Scene>();
		auto FadeOutPtr = GetStage()->GetSharedGameObject<FadeOut>(L"Fadeout");
		FadeOn = FadeOutPtr->getOn();		
	}

	void SelectDoor::OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec)
	{

		auto FadeOutPtr = GetStage()->GetSharedGameObject<FadeOut>(L"Fadeout");

		auto scene = App::GetApp()->GetScene<Scene>();
		for (auto& v : OtherVec)
		{
			if (v->FindTag(L"Player") && v->GetThis<Player>()->GetButtonB())
			{

				FadeOutPtr->SetStart(true);
				App::GetApp()->GetScene<Scene>()->SetStageNum(m_StageNum);
				PostEvent(1.0f, GetThis<ObjectInterface>(), scene, L"ToGameStage");
				auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Knock");
			}
		}
	}

	CraeteDoor::CraeteDoor(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos, const wstring TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Texture(TexturePtr), FadeOn(false)
	{}

	void CraeteDoor::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetScale(2.6f, 3.27f, 1.0f);
		PtrTrans->SetRotation(DegUp, 0.0f, 0.0f);

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		PtrDraw->SetTextureResource(m_Texture);

		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::None);

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Knock");

		AddTag(L"Door");
		SetAlphaActive(true);
	}

	void CraeteDoor::OnUpdate()
	{
		//auto scene = App::GetApp()->GetScene<Scene>();
		//auto FadeOutPtr = GetStage()->GetSharedGameObject<FadeOut>(L"Fadeout");
		//FadeOn = FadeOutPtr->getOn();
	}

	void CraeteDoor::OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec)
	{

		auto FadeOutPtr = GetStage()->GetSharedGameObject<FadeOut>(L"Fadeout");

		auto scene = App::GetApp()->GetScene<Scene>();
		for (auto& v : OtherVec)
		{
			if (v->FindTag(L"Player") && v->GetThis<Player>()->GetButtonB())
			{

				//FadeOutPtr->SetStart(true);
				//App::GetApp()->GetScene<Scene>()->SetStageNum(m_StageNum);
				PostEvent(1.0f, GetThis<ObjectInterface>(), scene, L"ToObjCreate");
				auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Knock");
			}
		}
	}


	//--------------------------------------------------------------------------------------
	//	class PlusCount : public GameObject
	//	用途: 本数を増やすアイテム
	//--------------------------------------------------------------------------------------
	PlusCount::PlusCount(const shared_ptr<Stage>& StagePtr, Vec3 & StartPos) :
		GameObject(StagePtr), m_Pos(StartPos)
	{}

	PlusCount::PlusCount(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node):
		GameObject(StagePtr)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
	}

	void PlusCount::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(Vec3(DegUp, 0.0f, 0.0f));
		PtrTrans->SetScale(Vec3(2.0f, 2.0f, 1.0f));

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		PtrDraw->SetTextureResource(L"BONE_TX");

		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::None);

		auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
		Group->IntoGroup(GetThis<PlusCount>());

		SetAlphaActive(true);
	}

	void PlusCount::OnUpdate()
	{

	}

	void PlusCount::OnCollision(vector<shared_ptr<GameObject>>& OtherVec)
	{
		for (auto& v : OtherVec)
		{
			if (v->FindTag(L"Chara"))
			{
				auto player = GetStage()->GetSharedGameObject<Player>(L"Player");
				player->SetCount(player->GetCount() + 1);
				GetStage()->RemoveGameObject<PlusCount>(GetThis<PlusCount>());
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class GoalPanel : public GameObject
	//	用途: Goal板
	//--------------------------------------------------------------------------------------
	GoalPanel::GoalPanel(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartScale, const Vec3& StartRot, const wstring TexturePtr) :
		GameObject(StagePtr), m_Pos(StartPos), m_Scale(StartScale), m_Rot(StartRot), m_Texture(TexturePtr)
	{}
	GoalPanel::GoalPanel(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node) :
		GameObject(StagePtr)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
		auto ScaleStr = XmlDocReader::GetAttribute(Node, L"Scale");
		vector<wstring> ScaleT;
		Util::WStrToTokenVector(ScaleT, ScaleStr, L',');
		m_Scale = Vec3
		(
			static_cast<float>(_wtof(ScaleT[0].c_str())),
			static_cast<float>(_wtof(ScaleT[1].c_str())),
			static_cast<float>(_wtof(ScaleT[2].c_str()))
		);
		auto RotStr = XmlDocReader::GetAttribute(Node, L"Rot");
		vector<wstring> RotT;
		Util::WStrToTokenVector(RotT, RotStr, L',');
		m_Rot = Vec3
		(
			static_cast<float>(_wtof(RotT[0].c_str())),
			static_cast<float>(_wtof(RotT[1].c_str())),
			static_cast<float>(_wtof(RotT[2].c_str()))
		);
		auto TexStr = XmlDocReader::GetAttribute(Node, L"Texture");
		m_Texture = TexStr;
	}
	void GoalPanel::OnCreate()
	{
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(DegUp, 0.0f, 0.0f);
		PtrTrans->SetScale(m_Scale);

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		PtrDraw->SetTextureResource(m_Texture);

		auto PtrCol = AddComponent<CollisionObb>();

		auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
		Group->IntoGroup(GetThis<GoalPanel>());

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class SelectPanel : public GameObject
	//	用途: セレクトの背景
	//--------------------------------------------------------------------------------------
	SelectPanel::SelectPanel(const shared_ptr<Stage>& StagePtr, const Vec3 & StartPos) :
		GameObject(StagePtr), m_Pos(StartPos)
	{}

	void SelectPanel::OnCreate()
	{
		float m_HalfSize = 0.5f;
		m_BackupVertices = 
		{
			{ VertexPositionColorTexture(Vec3(-m_HalfSize,m_HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(m_HalfSize,m_HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(10.0f,0.0f)) },
			{ VertexPositionColorTexture(Vec3(-m_HalfSize,-m_HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(0.0f,1.0f)) },
			{ VertexPositionColorTexture(Vec3(m_HalfSize,-m_HalfSize,0),Col4(1.0f,1.0f,1.0f,0.0f),Vec2(10.0f,1.0f)) }
		};
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(true);

		auto PtrTrans = GetComponent<Transform>();
		Quat Qt;
		Qt.rotationRollPitchYawFromVector(Vec3(XM_PIDIV2, 0, 0));
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetScale(100, 100 * 0.263f, 1.0f);
		PtrTrans->SetQuaternion(Qt);

		//auto PtrDraw = AddComponent<PNTStaticDraw>();
		//PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		//PtrDraw->SetTextureResource(L"APARTMENT_TX");

		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(L"APARTMENT_TX");
		PtrDraw->OnDraw();
	}
	void SelectPanel::OnUpdate()
	{

	}
}
//end basecross
