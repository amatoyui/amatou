/*!
@file Character.h
@brief キャラクターなど
*/

#pragma once
#include "Project.h"
namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Area : public GameObject
	//	用途: 引ける範囲を表示
	//--------------------------------------------------------------------------------------

	class Area : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Rot;
		Vec3 m_Scale;
		wstring m_Texture;

	public:
		Area(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartScale, const Vec3& StartRot, const wstring& TexturePtr);
		~Area() {};

		virtual void OnCreate() override;
		virtual void OnUpdate() {};

	};

	//--------------------------------------------------------------------------------------
	//	class Point : public GameObject
	//	用途: 点
	//--------------------------------------------------------------------------------------
	
	class Point : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Scale;
		wstring m_Texture;
		wstring m_ChooseTexture;
		float m_EndPoint;
		bool m_GoalFlag;

		Vec3 m_GoalLineDirection;
		Vec3 m_GoalLinePos;
		float m_GoalLineLength;
		vector<Vec3> m_LineDirection;
		vector<Vec3> m_LinePos;
		vector<float> m_LineLength;

		//ステートマシン
		unique_ptr<LayeredStateMachine<Point>> m_StateMachine;

	public:
		Point(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartScale, const wstring& TexturePtr, const wstring& ChooseTexture);
		Point(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		~Point() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		//ステートマシンを得る
		unique_ptr<LayeredStateMachine<Point>>& GetStateMachine() { return m_StateMachine; }

		//コリジョン
		void OnCollision(vector<shared_ptr<GameObject>>& OtherVec);
		//向きの決定用
		Vec3 Direction(Vec3 LineVec);

		//アクセサ
		wstring GetChooseTexture() { return m_ChooseTexture; }
		wstring GetDefaultTexture() { return m_Texture; }

		float GetEndPoint() { return m_EndPoint; }
		void SetEndPoint(float value) { m_EndPoint = value; }

		vector<Vec3> GetLineDire() { return m_LineDirection; }
		void SetLineDire(Vec3 value) { m_LineDirection.push_back(value); }

		vector<Vec3> GetLinePos() { return m_LinePos; }

		Vec3 GetGoalLineDire() { return m_GoalLineDirection; }

		Vec3 GetGoalLinePos() { return m_GoalLinePos; }

		bool GetGoalFlag() { return m_GoalFlag; }

		vector<float> GetLineLength() { return m_LineLength; }

		float GetGoalLineLength() { return m_GoalLineLength; }

		void SetPosition(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetPosition(value);
		}
	};
	//----------------------------------------------------------------------
	///点の通常ステート
	//----------------------------------------------------------------------
	class PointDefaultState : public ObjState<Point>
	{
		PointDefaultState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(PointDefaultState)
		virtual void Enter(const shared_ptr<Point>& Obj)override;
		virtual void Execute(const shared_ptr<Point>& Obj)override;
		virtual void Exit(const shared_ptr<Point>& Obj)override;
	};

	//----------------------------------------------------------------------
	///選択されたときのステート
	//----------------------------------------------------------------------
	class PointChooseState : public ObjState<Point>
	{
		PointChooseState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(PointChooseState)
		virtual void Enter(const shared_ptr<Point>& Obj)override;
		virtual void Execute(const shared_ptr<Point>& Obj)override;
		virtual void Exit(const shared_ptr<Point>& Obj)override;
	};

	//----------------------------------------------------------------------
	//	class TrunGousun : public GameObject
	//	用途 : 往復する敵
	//----------------------------------------------------------------------
	class TurnGousun : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Rot;
		Vec3 m_Scale;
		wstring m_Texture;

		float velo = 0;
		bool  on = false;

		bool m_Move;
	public :
		TurnGousun(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartRot,const Vec3& StartScale, const wstring& TexturePtr);
		TurnGousun(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		~TurnGousun() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		//コリジョン
		void OnCollision(vector<shared_ptr<GameObject>>& OtherVec);

		//アクセサ
		void SetMove(bool flag) { m_Move = flag; }
	};

	//----------------------------------------------------------------------
	//	class CloseGousun : public GameObject
	//	用途 : 近づく敵
	//----------------------------------------------------------------------
	class CloseGousun : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Rot;
		Vec3 m_Scale;

		wstring m_Texture;

		Vec3 m_PlayerPos;
		bool m_Move;
	public :
		CloseGousun(const shared_ptr<Stage>& stagePtr, const Vec3& StartPos, const Vec3& StartRot, const Vec3& StartScale, const wstring& TexturePtr);
		CloseGousun(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		~CloseGousun() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		//コリジョン
		void OnCollision(vector<shared_ptr<GameObject>>& OtherVec);

		//アクセサ
		void SetMove(bool flag) { m_Move = flag; }
	};

	//----------------------------------------------------------------------
	//	class AwayGousun : public GameObject
	//	用途 : 逃げる敵
	//----------------------------------------------------------------------
	class AwayGousun : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Rot;
		Vec3 m_Scale;
		wstring m_Texture;

		Vec3 m_PlayerPos;
		bool m_Move;

	public :
		AwayGousun(const shared_ptr<Stage>& stagePtr, const Vec3& StartPos, const Vec3& StartRot, const Vec3& StartScale, const wstring& TexturePtr);
		AwayGousun(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		~AwayGousun() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		//コリジョン
		void OnCollision(vector<shared_ptr<GameObject>>& OtherVec);

		//アクセサ
		void SetMove(bool flag) { m_Move = flag; }
	};

	//----------------------------------------------------------------------
	//	class CameraBox : public GameObject
	//	用途 : ゲームスタート時のカメラ演出
	//----------------------------------------------------------------------
	class CameraBox : public GameObject
	{
		Vec3 m_Pos;			//CameraBoxさんの位置
		Vec3 m_Pos2;		//CameraBoxさんの位置サブ（ごり押し用）
		Vec3 m_PlayerPos;	//プレイヤーの位置取得用
		Vec3 m_KokkenPos;	//こっけんの位置取得用
		Vec3 m_GoalPos;		//ゴールの位置取得用
		int k_Count;		//こっけんを何秒間見つめるか
		int k_rim;			//見つめる限界
		int g_Count;		//ゴールを何秒見つめるか
		int g_rim;			//見つめる限界
		float m_CamSpX;		//Xカメラの移動速度
		float m_CamSpZ;		//Zカメラの移動速度
		bool StartOn;		//起動の有無を表す
		bool Xok;			//x軸の移動完了の有無を表す
		bool Zok;			//y軸の移動完了の有無を表す

		shared_ptr<GameObject> KokkenPtr;
		//shared_ptr<GameObject> PlayerPtr;

	public :
		CameraBox(const shared_ptr<Stage>& StagePtr);
	    ~CameraBox() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		void CharaStop(bool flag);

		bool GetStart() { return StartOn; }
	};


	//--------------------------------------------------------------------------------------
	//	class Line : public GameObject
	//	用途: 線
	//--------------------------------------------------------------------------------------
	class Line : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Rot;
		Vec3 m_Scale;
		wstring m_Texture;

		Vec3 m_OnePoint;
		Vec3 m_TwoPoint;

		Vec3 m_distance;

	public:
		Line(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos,  const Vec3& StartRot, const Vec3& StartScale, const wstring& TexturePtr);
		Line(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		~Line() {};


		virtual void OnCreate() override;
		virtual void OnUpdate() {};

		void OnCollision(vector<shared_ptr<GameObject>>& OtherVec);

		void SetGroup()
		{
			auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
			Group->IntoGroup(GetThis<Line>());
		}
		
		//アクセサ
		void SetDistance(Vec3 value) { m_distance = value; }
		Vec3 GetDistance() { return m_distance; }

		void SetOnePoint(Vec3 value) { m_OnePoint = value; }
		Vec3 GetOnePoint() { return m_OnePoint; }

		void SetTwoPoint(Vec3 value) { m_TwoPoint = value; }
		Vec3 GetTwoPoint() { return m_TwoPoint; }

		void SetPosition(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetPosition(value);
		}
		void SetScale(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetScale(value);
		}
		void SetRot(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetRotation(value);
		}
		wstring GetTexture() { return m_Texture; }
	};

	//--------------------------------------------------------------------------------------
	//	class GoalLine : public GameObject
	//	用途: ゴールの足場
	//--------------------------------------------------------------------------------------
	class GoalLine : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Rot;
		Vec3 m_Scale;
		wstring m_Texture;

		Vec3 m_OnePoint;
		Vec3 m_TwoPoint;

		Vec3 m_distance;

	public:
		GoalLine(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartRot, const Vec3& StartScale, const wstring& TexturePtr);
		GoalLine(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		~GoalLine() {};

		virtual void OnCreate() override;
		virtual void OnUpdate() {};

		void SetGroup()
		{
			auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
			Group->IntoGroup(GetThis<GoalLine>());
		}

		//アクセサ
		void SetDistance(Vec3 value) { m_distance = value; }
		Vec3 GetDistance() { return m_distance; }

		void SetOnePoint(Vec3 value) { m_OnePoint = value; }
		Vec3 GetOnePoint() { return m_OnePoint; }

		void SetTwoPoint(Vec3 value) { m_TwoPoint = value; }
		Vec3 GetTwoPoint() { return m_TwoPoint; }

		void SetPosition(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetPosition(value);
		}
		void SetScale(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetScale(value);
		}
		void SetRot(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetRotation(value);
		}
		wstring GetTexture() { return m_Texture; }
	};

	//--------------------------------------------------------------------------------------
	//	class ThroughLine : public GameObject
	//	用途: 補助線
	//--------------------------------------------------------------------------------------
	class ThroughLine : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Rot;
		Vec3 m_Scale;
		wstring m_Texture;

		Vec3 m_OnePoint;
		Vec3 m_TwoPoint;

		Vec3 m_distance;

	public:
		ThroughLine(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartRot, const Vec3& StartScale, const wstring& TexturePtr);
		~ThroughLine() {};

		virtual void OnCreate() override;
		virtual void OnUpdate() {};

		//アクセサ
		void SetDistance(Vec3 value) { m_distance = value; }
		Vec3 GetDistance() { return m_distance; }

		void SetOnePoint(Vec3 value) { m_OnePoint = value; }
		Vec3 GetOnePoint() { return m_OnePoint; }

		void SetTwoPoint(Vec3 value) { m_TwoPoint = value; }
		Vec3 GetTwoPoint() { return m_TwoPoint; }

		void SetPosition(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetPosition(value);
		}
		void SetScale(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetScale(value);
		}
		void SetRot(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetRotation(value);
		}
		wstring GetTexture() { return m_Texture; }
	};

	//--------------------------------------------------------------------------------------
	//	class Goal : public GameObject
	//	用途: ゴール
	//--------------------------------------------------------------------------------------
	class Goal : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Scale;
		Vec3 m_Rot;
		wstring m_Texture;
		float m_distance;

		shared_ptr<GameObject> m_GoalPanel;

	public:
		Goal(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartScale, const Vec3& StartRot, const wstring TexturePtr);
		Goal(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		~Goal() {}


		virtual void OnCreate()override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec);

		void SetGroup()
		{
			auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
			Group->IntoGroup(GetThis<Goal>());
		}
	};

	//--------------------------------------------------------------------------------------
	//	class SelectDoor : public GameObject
	//	用途: ステージセレクトに置くドア
	//--------------------------------------------------------------------------------------
	class SelectDoor : public GameObject
	{
		Vec3 m_Pos;
		wstring m_Texture;
		wstring m_CsvName;
		int m_StageNum;

		//テスト
		bool FadeOn;


	public:
		SelectDoor(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const wstring TexturePtr);
		~SelectDoor() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		void OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec);

		//アクセサ
		wstring GetCsvName() { return m_CsvName; }
		void SetCsvName(wstring name) { m_CsvName = name; }

		void SetTexture(wstring texture) { m_Texture = texture; }

		int GetStageNum() { return m_StageNum; }
		void SetStageNum(int value) { m_StageNum = value; }
	};

	//--------------------------------------------------------------------------------------
	//	class CraeteDoor : public GameObject
	//	用途: ステージセレクトに置くドア
	//--------------------------------------------------------------------------------------
	class CraeteDoor : public GameObject
	{
		Vec3 m_Pos;
		wstring m_Texture;
		wstring m_CsvName;
		int m_StageNum;

		//テスト
		bool FadeOn;


	public:
		CraeteDoor(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const wstring TexturePtr);
		~CraeteDoor() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		void OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec);

		//アクセサ
		wstring GetCsvName() { return m_CsvName; }
		void SetCsvName(wstring name) { m_CsvName = name; }

		void SetTexture(wstring texture) { m_Texture = texture; }

		int GetStageNum() { return m_StageNum; }
		void SetStageNum(int value) { m_StageNum = value; }
	};

	//--------------------------------------------------------------------------------------
	//	class PlusCount : public GameObject
	//	用途: 本数を増やすアイテム
	//--------------------------------------------------------------------------------------
	class PlusCount : public GameObject
	{
		Vec3 m_Pos; 
		
	public:
		PlusCount(const shared_ptr<Stage>& StagePtr, Vec3& StartPos);
		PlusCount(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		~PlusCount() {}

		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec);
	};

	//--------------------------------------------------------------------------------------
	//	class GoalPanel : public GameObject
	//	用途: Goal板
	//--------------------------------------------------------------------------------------
	class GoalPanel : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Scale;
		Vec3 m_Rot;
		wstring m_Texture;

	public:
		GoalPanel(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos, const Vec3& StartScale, const Vec3& StartRot, const wstring TexturePtr);
		GoalPanel(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		~GoalPanel() {}

		virtual void OnCreate()override;

		wstring GetTexture() { return m_Texture; }
	};

	//--------------------------------------------------------------------------------------
	//	class SelectPanel : public GameObject
	//	用途: セレクトの背景
	//--------------------------------------------------------------------------------------
	class SelectPanel : public GameObject
	{
		Vec3 m_Pos;

		vector<VertexPositionColorTexture> m_BackupVertices;
	public:
		SelectPanel(const shared_ptr<Stage>& StagePtr, const Vec3& StartPos);
		virtual ~SelectPanel() {};

		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};
}