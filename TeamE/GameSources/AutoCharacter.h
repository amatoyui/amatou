#pragma once
#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	//--------------------------------------------------------------------------------------
	//	class AutoCharacter : public GameObject
	//	用途: 線の上を歩くなど
	//--------------------------------------------------------------------------------------
	class AutoCharacter : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Rot;
		Vec3 m_Scale;

		float m_Gravity;
		//向き
		Vec3 m_Velocity;
		//加速
		float m_TouchAcceleration;

		bool m_Lineflag;
		bool m_Colflag;
		bool m_DeadFlag;
		int m_VeloNum;
		bool m_gameflag;
		Vec3 m_BeforePos;
		Vec3 m_ColPos;
		float m_Speed;
		float m_Acceleration;
		float m_NextRot;
		float m_Length;
		int m_DeadCount;

		float moveX;
		float moveZ;

		wstring m_Texture;

		bool m_MoveFlag;

		shared_ptr<GameObject> m_Area;
		unique_ptr<LayeredStateMachine<AutoCharacter>> m_StateMachine;
		shared_ptr<ObjState<AutoCharacter>> m_State;
		shared_ptr<GameObject> m_Obj;
		shared_ptr<GameObject> m_point;

		Vec3 m_vec;

	public:
		AutoCharacter
		(
			const shared_ptr<Stage>& StagePtr, 
			const Vec3& StartPos, 
			const Vec3& StartRot,
			const Vec3& StartScale,
			const wstring TexturePtr,
			const float Gravity,
			const shared_ptr<ObjState<AutoCharacter>> StartState,
			const float MoveX, 
			const float MoveZ,
			const float Speed
		);
		AutoCharacter(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		virtual ~AutoCharacter() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		unique_ptr<LayeredStateMachine<AutoCharacter>>& GetStateMachine() { return m_StateMachine; }

		void OnCollision(vector<shared_ptr<GameObject>>& OtherVec);
		void OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec); // ※
		void OnCollisionExit(vector<shared_ptr<GameObject>>& OtherVec);

		//キャラの移動
		void Move();
		//次の線にいくとき
		void NextMove();
		//方向を決める
		void NextVelo();
		//死んだときの処理
		void DeadChara();
		//スタートの動き
		void StartMove();
		//ゴール後の動き
		void GoalMove();
		
		//アクセサ
		void SetVec(Vec3 value) { m_vec = value; }
		Vec3 GetVec() { return m_vec; }

		void SetBeforePos(Vec3 value) { m_BeforePos = value; }

		shared_ptr<GameObject> GetObj() { return m_Obj; }
		shared_ptr<GameObject> GetPoint() { return m_point; }

		Vec3 GetVelo() { return m_Velocity; }
		void SetVelo(Vec3 value) { m_Velocity = value; }

		void SetTouchAcceleration(float value) { m_TouchAcceleration = value; }
		void SetPosition(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetPosition(value);
		}
		void SetPosition(float value)
		{
			SetPosition(Vec3(value, value, value));
		}

		void SetScale(Vec3 value) 
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetScale(value);
		}
		void SetScale(float value)
		{
			SetScale(Vec3(value, value, value));
		}

		void SetRot(Vec3 value)
		{
			auto PtrTrans = GetComponent<Transform>();
			PtrTrans->SetRotation(value);
		}
		void SetRot(float value)
		{
			SetRot(Vec3(value, value, value));
		}

		void SetMoveFlag(bool flag) { m_MoveFlag = flag; }
		bool GetMoveFlag() { return m_MoveFlag; }

		float GetSpeed() { return m_Speed; }
		float GetMoveX() { return moveX; }
		float GetMoveZ() { return moveZ; }

		bool GetDead() { return m_DeadFlag; }
		void SetDead(bool flag) { m_DeadFlag = flag; }

		int GetDeadCount() { return m_DeadCount; }
		void SetDeadCount(int value) { m_DeadCount = value; }

		void SetGroup()
		{
			auto Group = GetStage()->GetSharedObjectGroup(L"EditGroup");
			Group->IntoGroup(GetThis<AutoCharacter>());
		}
	};

	//ラインゲーム用
	class CharaLineState : public ObjState<AutoCharacter>
	{
		CharaLineState(){}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(CharaLineState)
		virtual void Enter(const shared_ptr<AutoCharacter>& Obj)override;
		virtual void Execute(const shared_ptr<AutoCharacter>& Obj)override;
		virtual void Exit(const shared_ptr<AutoCharacter>& Obj)override;
	};

	//次の線に行くとき
	class NextLineState : public ObjState<AutoCharacter>
	{
		NextLineState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(NextLineState)
		virtual void Enter(const shared_ptr<AutoCharacter>& Obj)override;
		virtual void Execute(const shared_ptr<AutoCharacter>& Obj)override;
		virtual void Exit(const shared_ptr<AutoCharacter>& Obj)override;
	};
}
