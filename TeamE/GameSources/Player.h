/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"
namespace basecross{

	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject
	//	用途: 手のやつの動き
	//--------------------------------------------------------------------------------------

	class Player : public GameObject
	{
		Vec3 m_Pos;
		Vec3 m_Scale;
		Vec3 m_Rot;
		wstring m_Texture;
		//プレイヤーのスピード
		float m_Speed;
		//線が引ける数を制限
		int m_LineCount;

		//ボタンのフラグ
		bool m_buttonY;
		bool m_buttonB;
		bool m_buttonA;
		bool m_startbutton;

		//当たっているかのフラグ
		bool m_hit;
		//1つ目の点のPosition保存
		Vec3 m_oneVec;
		//2つ目の点のPosition保存
		Vec3 m_twoVec;
		//点を何個選択したか
		int m_hitcount;
		//線のPosition保存用
		Vec3 m_LinePos;
		//始点からの距離
		float m_OneDis;
		//1つ前のPosition
		Vec3 m_BeforePos;
		//縦取得
		size_t m_Horizontal;
		//横取得
		size_t m_Vertical;
		//減速
		float m_Deceleration;

		float m_NowDis;
		bool m_Pointflag;

		int m_ObjCount;
		float m_time;
		float m_ObjRot;
		int m_SaveCount;
		bool m_Change;
		bool m_flag;
		bool m_MoveFlag;

		weak_ptr<GameObject> m_ObjStay;
		weak_ptr<GameObject> m_PointArea;
		weak_ptr<GameObject> m_ThroughLine;

		shared_ptr<ObjState<Player>> m_State;
		unique_ptr<LayeredStateMachine<Player>> m_StateMachine;
				
		float None;
		int TestNum;
		
	public:
		Player
		(
			const shared_ptr<Stage>& StagePtr, 
			const Vec3& StartPos,
			const Vec3& StartScale,
			const Vec3& StartRotation,
			const float& Speed, 
			const wstring& TexturePtr,
			const shared_ptr<ObjState<Player>>& StartState,
			const int& LineCount
		);
		Player(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node);
		virtual ~Player() {};
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//入力のハンドラー
		InputHandler<Player> m_inputhandler;

		unique_ptr<LayeredStateMachine<Player>>& GetStateMachine() { return m_StateMachine; }

		//衝突判定
		void OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec); //当たっているとき
		void OnCollisionExit(vector<shared_ptr<GameObject>>& OtherVec);	  //抜けた時
		//2点間の距離
		float Distance(Vec3 one, Vec3 two); 
		Vec3 dis();
		//2点間の角度
		float Rad();  
		//線の生成
		void LineCreate(); 
		//点の選択
		void PointChoose(shared_ptr<GameObject>& Obj);		
		//点の取得
		void PointAcquisition();
		//線の上を選択
		void LineChoose(shared_ptr<GameObject>& Obj);
		//線が引ける範囲
		void LineRange();
		//移動制限
		void MoveRestriction();
		void SelectMoveRestriction();
		//予測線
		void PredictionLine();
		//速度を戻す
		void ReDece();
		//選択のリセット
		void CooseReset();

		void TumiReset(bool flag, int num)
		{
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			if (flag)
			{
				switch (num)
				{
				case 0:
					PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage");
					break;
				case 1:
					PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToStageSelect");
					break;
				}
			}
		};

		//オブジェクト生成
		void CreateObject();
		//向きの変更
		void RotationObj(int value);

		void SceneChange(int value);
		void CharaStop();

		//アクセサ
		void SetButtonY(bool flag) { m_buttonY = flag; }
		bool GetButtonY() { return m_buttonY; }

		void SetButtonB(bool flag) { m_buttonB = flag; }
		bool GetButtonB() { return m_buttonB; }

		void SetButtonA(bool flag) { m_buttonA = flag; }
		bool GetButtonA() { return m_buttonA; }

		void SetStartButton(bool flag) { m_startbutton = flag; }
		bool GetStartButton() { return m_startbutton; }

		void SetPosition(Vec3 value) 
		{ 
			m_Pos = value;
			auto PlayerTrans = GetComponent<Transform>();
			PlayerTrans->SetPosition(value);
		}
		Vec3 GetPosition() { return m_Pos; }

		float GetSpeed() { return m_Speed; }

		void SetCount(int value) { m_LineCount = value; }
		int GetCount() { return m_LineCount; }

		float GetOneDis() { return m_OneDis; }

		Vec3 GetBeforePos() { return m_BeforePos; }
		void SetBeforePos(Vec3 value) { m_BeforePos = value; }

		bool GetHit() { return m_hit; };

		size_t GetHori() { return m_Horizontal; }
		void SetHori(size_t hori) 
		{ 
			auto scene = App::GetApp()->GetScene<Scene>();
			scene->SetHori(hori);
			m_Horizontal = hori;
		}

		size_t GetVer() { return m_Vertical; }
		void SetVer(size_t ver) 
		{ 
			auto scene = App::GetApp()->GetScene<Scene>();
			scene->SetVer(ver);
			m_Vertical = ver; 
		}

		bool GetMoveFlag() { return m_MoveFlag; }
		void SetMoveFlag(bool flag) { m_MoveFlag = flag; }

		weak_ptr<GameObject> GetObj() { return m_ObjStay; }

		int GetObjCount() { return m_ObjCount; }
		void SetObjCount(int value)
		{
			int num = value;
			if (num > 6)
			{
				num = 0;
			}
			if (num < 0)
			{
				num = 6;
			}
			m_ObjCount = num;
		}
		float GetDeceleration() { return m_Deceleration; }
		void SetDeceleration(float value) { m_Deceleration = value; }
		int GetSaveCount() { return m_SaveCount; }
		void SetSaveCount(int value) 
		{ 
			int num = value;
			if (num < 0)
			{
				num = 9;
			}

			if (num > 9)
			{
				num = 0;
			}
			m_SaveCount = num; 
		}
		bool GetChange() { return m_Change; }
		void SetChange(bool flag) 
		{ 
			if (m_Change)
			{
				m_Change = false;
			}
			else
			{
				m_Change = true;
			}
		}
		float GetObjRot() { return m_ObjRot; }
	};

	//ラインゲーム用のステート
	class PlayerLineState : public ObjState<Player>
	{
		PlayerLineState() {}
	public:
		DECLARE_SINGLETON_INSTANCE(PlayerLineState)
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

	//点を選択したとき
	class ChoosePointState : public ObjState<Player>
	{
		ChoosePointState() {}
	public:
		DECLARE_SINGLETON_INSTANCE(ChoosePointState)
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

	//セレクトステージ用のステート
	class SelectStageState : public ObjState<Player>
	{
		SelectStageState() {}
	public:
		DECLARE_SINGLETON_INSTANCE(SelectStageState)
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

	//エディットモードのステート
	class EditModeState : public ObjState<Player>
	{
		EditModeState() {}
	public:
		DECLARE_SINGLETON_INSTANCE(EditModeState)
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};
}
//end basecross

