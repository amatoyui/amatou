#pragma once
#include "Project.h"
namespace basecross
{
	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class ObjCreateStage : public Stage
	{
		void CreateViewLight();
		void CreatePlate();
		void CreatePlayer();

	public:
		ObjCreateStage() : Stage(){}
		virtual ~ObjCreateStage() {}
		virtual void OnCreate() override;
	};
}
