/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------
	//ビューとライトの作成
	void GameStage::CreateViewLight() 
	{
		auto PtrView = CreateView<SingleView>();
		////ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<MyCamera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 20.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(10.0f, 10.0f, 0.0f);
	}

	//プレートの作成
	void GameStage::CreatePlate() 
	{
		auto scene = App::GetApp()->GetScene<Scene>();
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quat Qt;
		Qt.rotationRollPitchYawFromVector(Vec3(XM_PIDIV2, 0, 0));
		//PtrTrans->SetScale(100.0f, 100.0f, 1.0f);
		//PtrTrans->SetQuaternion(Qt);
		//PtrTrans->SetPosition(11.0f, -0.5f, 25.0f);
		PtrTrans->SetScale(static_cast<float>(scene->GetVer()), static_cast<float>(scene->GetHori()), 1.0f);
		PtrTrans->SetQuaternion(Qt);
		PtrTrans->SetPosition(static_cast<float>(scene->GetVer()) / 2, -0.5f, static_cast<float>(scene->GetHori()) / 2);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"BACKGROUND_TX");
	}

	void GameStage::CreateTime()
	{
		auto scene = App::GetApp()->GetScene<Scene>();
		auto time = AddGameObject<TimeSprite>(1, static_cast<float>(scene->GetTime()), L"TIMER_TX", true, Vec2(130.0f, 130.0f), Vec3(0.0f, 230.0f, 0.0f));
		SetSharedGameObject(L"TimeSprite", time);
	}

	void GameStage::CreateTimeText()
	{
		AddGameObject<TimeText>(Vec2(-310.0f, 230.0f), Vec2(400.0f, 400.0f), L"NOKORIJIKAN_TX");
	}

	void GameStage::CreateCount()
	{
		auto count = AddGameObject<CountSprite>(Vec3(0.0f, 330.0f, 0.0f), Vec3(130.0f, 130.0f,1.0f));
	}

	void GameStage::CreateCountText()
	{
		
		AddGameObject<CountText>(Vec2(-330.0f, 330.0f), Vec2(400.0f, 400.0f), L"SENNOKORI_TX");
	}

	void GameStage::CreateResultUI()
	{
		AddGameObject<ResultUI>(Vec2(0.0f, 0.0f), Vec2(50.0f, 50.0f));
	}

	void GameStage::CreateGroup()
	{
		CreateSharedObjectGroup(L"LineGroup");
	}

	void GameStage::xml()
	{
		wstring DataDir;

		GameObjecttXMLBuilder Builder;
		//GameObjectの登録
		Builder.Register<AutoCharacter>(L"Chara");
		Builder.Register<Point>(L"Point");
		Builder.Register<Line>(L"Line");
		Builder.Register<Goal>(L"Goal");
		Builder.Register<TurnGousun>(L"Turn");
		Builder.Register<AwayGousun>(L"Away");
		Builder.Register<CloseGousun>(L"Close");
		Builder.Register<GoalLine>(L"Goalline");
		Builder.Register<Player>(L"Player");
		Builder.Register<PlusCount>(L"Item");
		Builder.Register<GoalPanel>(L"GoalPanel");

		auto scene = App::GetApp()->GetScene<Scene>();

		App::GetApp()->GetDataDirectory(DataDir);
		wstring XMLStr = DataDir + L"StageCreate\\Data";
		XMLStr += L".xml";
		auto xml = new XmlAccessor(XMLStr);

		for (int i = 0; i < xml->GetCount(scene->GetStageNum()); i++)
		{
			Builder.Build(GetThis<Stage>(), XMLStr, L"GameStage/map" + Util::IntToWStr(scene->GetStageNum()) + L"/GameObject" + Util::IntToWStr(i));
		}
		scene->SetVer(xml->GetVer(scene->GetStageNum()));
		scene->SetHori(xml->GetHori(scene->GetStageNum()));
		scene->SetMinVer(0);
		scene->SetMinHori(0);
		scene->SetTime(xml->GetTime(scene->GetStageNum()));

		delete xml;
	}

	void GameStage::OnCreate() 
	{
		try {
			CreateSharedObjectGroup(L"EditGroup");

			//ビューとライトの作成
			CreateViewLight();
			//StageCreate用
			xml();
			//プレートの作成
			CreatePlate();
			//タイム
			CreateTime();
			//本数表示用
			CreateCount();
			//グループの作成
			CreateGroup();
			CreateTimeText();
			CreateCountText();
			//ResultUI表示
			CreateResultUI();

			AddGameObject<FadeIn>();
			AddGameObject<CameraBox>();
		}
		catch (...) 
		{
			throw;
		}
	}
}
//end basecross
