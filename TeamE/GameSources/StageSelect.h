#pragma once
#include "Project.h"

namespace basecross
{
	//------------------------------------------------------------------
	//	ステージセレクトクラス
	//------------------------------------------------------------------
	class StageSelect : public Stage
	{
		//ビューとライトの作成
		void CreateCiewLight();
		//プレートの作成
		void CreatePlate();

		//プレイヤーの作成
		void CreatePlayer();

		//セレクト用のドア
		void CreateDoor();

		//スプライトの作成
		void CreateSprite();

		//テスト
		void FadeOutManager();


	public :
		//構築と破棄
		StageSelect() :Stage() {}
		virtual ~StageSelect() {}

		//初期化
		virtual void OnCreate()override;
	};
}