/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "Project.h"
namespace basecross {
	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage 
	{
		//Csvファイル
		CsvFile m_GameStageCsv;
		//ビューとライトの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//タイム
		void CreateTime();
		//タイムテキスト
		void CreateTimeText();
		//本数表示
		void CreateCount();
		//本数テキスト
		void CreateCountText();
		//ResultUI表示
		void CreateResultUI();
		//グループの作成
		void CreateGroup();
		//Stage読み込み用
		void xml();

	public:
		//構築と破棄
		GameStage() :Stage() {}
		virtual ~GameStage() {}
		//初期化
		virtual void OnCreate()override;
	};


}
//end basecross

