/*!
@file Project.h
@brief コンテンツ用のヘッダをまとめる
*/

#pragma once


#include "stdafx.h"
#include "ProjectBehavior.h"
#include "ProjectShader.h"
#include "Scene.h"
#include "GameStage.h"
#include "Character.h"
#include "Player.h"
#include "Title.h"
#include "StageSelect.h"
#include "Result.h"
#include "AutoCharacter.h"
#include "2DUI.h"
#include "MyCamera.h"
#include "ObjCreateStage.h"
#include "StageCreate.h"

#define DegUp 90.0f * XM_PI / 180.0f



