#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	//--------------------------------------------------------------------------------------
	//	struct MyCamera::Impl;
	//	用途: Implクラス
	//--------------------------------------------------------------------------------------
	struct MyCamera::Impl
	{
		//目標のオブジェクト
		weak_ptr<GameObject> mTargetObject;
		//目標から始点を調整する位置ベクトル
		Vec3 m_TargetToAt;
		//腕の長さの設定
		float m_ArmLen;
		float m_MaxArm;
		float m_MinArm;
		//前のターンのEyePosition
		Vec3 m_BeforeEye; 
		//前のターンのAtPosition
		Vec3 m_BeforeAt;
		//ゴールした時のフラグ
		bool m_GoalFlag;
		//ゴールのPosition保存用
		Vec3 m_GoalPos;
		//カメラのスピード
		float m_Speed;
		//ゴールに向かう動きが終わったフラグ
		bool m_MoveEnd;
		//セレクトに入っているフラグ
		bool m_Select;  
		//Start
		bool m_Start;
		//オブジェクト動き用のフラグ
		bool m_ObjMove;

		Impl() :
			m_TargetToAt(0, 0, 0),
			m_ArmLen(5.0f),
			m_MaxArm(20.0f),
			m_MinArm(2.0f),
			m_GoalFlag(false),
			m_GoalPos(0, 0, 0),
			m_Speed(6.0f),
			m_MoveEnd(false),
			m_Select(false),
			m_Start(false),
			m_ObjMove(false)
		{}
		~Impl(){}
	};

	//GameStage用のカメラ
	MyCamera::MyCamera():
		Camera(),
		pImpl(new Impl())
	{}

	MyCamera::~MyCamera(){}

	//アクセサ
	shared_ptr<GameObject> MyCamera::GetTargetObject() const
	{
		if (!pImpl->mTargetObject.expired())
		{
			return pImpl->mTargetObject.lock();
		}

		return nullptr;
	}

	void MyCamera::SetTargetObject(const shared_ptr<GameObject>& Obj)
	{
		pImpl->mTargetObject = Obj;
	}

	Vec3 MyCamera::GetTargetToAt() const
	{
		return pImpl->m_TargetToAt;
	}

	void MyCamera::SetTargetToAt(const Vec3 & v)
	{
		pImpl->m_TargetToAt = v;
	}

	Vec3 MyCamera::GetBeforeEye() const
	{
		return pImpl->m_BeforeEye;
	}

	void MyCamera::SetBeforeEye(const Vec3 & v)
	{
		pImpl->m_BeforeEye = v;
	}

	Vec3 MyCamera::GetBeforeAt() const
	{
		return pImpl->m_BeforeAt;
	}

	void MyCamera::SetBeforeAt(const Vec3 & v)
	{
		pImpl->m_BeforeAt = v;
	}

	bool MyCamera::GetGoalFlag()const
	{
		return pImpl->m_GoalFlag;
	}
	void MyCamera::SetGoalFlag(const bool flag)
	{
		pImpl->m_GoalFlag = flag;
	}

	Vec3 MyCamera::GetGoalPos() const
	{
		return pImpl->m_GoalPos;
	}
	void MyCamera::SetGoalPos(const Vec3 & v)
	{
		pImpl->m_GoalPos = v;
	}

	bool MyCamera::GetMoveEnd() const
	{
		return pImpl->m_MoveEnd;
	}

	void MyCamera::SetMoveEnd(const bool flag) const
	{
		pImpl->m_MoveEnd = flag;
	}

	bool MyCamera::GetSelect() const
	{
		return pImpl->m_Select;
	}

	void MyCamera::SetSelect(const bool flag) const
	{
		pImpl->m_Select = flag;
	}

	bool MyCamera::GetStart() const
	{
		return pImpl->m_Start;
	}

	void MyCamera::SetStart(const bool flag) const
	{
		pImpl->m_Start = flag;
	}

	bool MyCamera::GetObjMove() const
	{
		return pImpl->m_ObjMove;
	}

	void MyCamera::SetObjMove(const bool flag) const
	{
		pImpl->m_ObjMove = flag;
	}


	void MyCamera::OnUpdate()
	{
		//更新する前のやつを保存
		SetBeforeEye(GetEye());
		SetBeforeAt(GetAt());

		if (GetStart()) {
			if (GetSelect())
			{
				auto TargetPos = GetTargetObject()->GetComponent<Transform>()->GetPosition();
				SetAt(TargetPos);
				SetEye(Vec3(TargetPos.x, GetEye().y, TargetPos.z));
				SetUp(Vec3(0.0f, 0.0f, 1.0f));
			}
			else
			{
				auto TargetPos = GetTargetObject()->GetComponent<Transform>()->GetPosition();
				SetAt(TargetPos);
				SetEye(Vec3(TargetPos.x, GetEye().y, TargetPos.z - 5.0f));
			}
		}

		if (GetTargetObject()->FindTag(L"Player"))
		{
			//プレイヤーをターゲットしているときだけ有効
			Restriction();
		}

		if (GetGoalFlag())
		{
			//ゴール時の動き
			GoalMove();
		}


		Camera::OnUpdate();
	}

	//カメラの写す範囲
	void MyCamera::Restriction()
	{
		auto scene = App::GetApp()->GetScene<Scene>();
		auto ver = scene->GetVer();
		auto hori = scene->GetHori();
		auto minver = scene->GetMinVer();
		auto minhori = scene->GetMinHori();
		auto eye = GetEye();
		auto at = GetAt();

		if (eye.x < 20 + minver || ver - 20 < eye.x)
		{
			eye.x = GetBeforeEye().x;
			at.x = GetBeforeAt().x;
		}
		if (eye.z < 20 + minhori || hori - 20 < eye.z)
		{
			eye.z = GetBeforeEye().z;
			at.z = GetBeforeAt().z;
		}
		SetEye(eye);
		SetAt(at);
	}

	void MyCamera::GoalMove()
	{
		SetStart(false);
		auto goal = GetGoalPos();
		auto eye = GetEye();
		auto time = App::GetApp()->GetElapsedTime();

		if (eye.y > goal.y)
		{
			eye.y -= pImpl->m_Speed * time;
		}
		else
		{
			SetMoveEnd(true);
		}
		SetEye(eye);
	}
}