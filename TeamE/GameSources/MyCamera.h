#pragma once
#include "stdafx.h"

namespace basecross
{
	class MyCamera : public Camera
	{
	public :
		explicit MyCamera();
		virtual ~MyCamera();

		//カメラの目標オブジェクトを得る
		shared_ptr<GameObject> GetTargetObject() const;
		//カメラの目標オブジェクトを設定する
		void SetTargetObject(const shared_ptr<GameObject>& Obj);

		//ターゲットからAtへの調整ベクトルを得る
		Vec3 GetTargetToAt() const;
		//ターゲットからAtへの調整ベクトルを設定する
		void SetTargetToAt(const Vec3& v);

		//アクセサ
		Vec3 GetBeforeEye()const;
		void SetBeforeEye(const Vec3& v);

		Vec3 GetBeforeAt()const;
		void SetBeforeAt(const Vec3& v);

		bool GetGoalFlag()const;
		void SetGoalFlag(const bool flag);

		Vec3 GetGoalPos()const;
		void SetGoalPos(const Vec3& v);

		bool GetMoveEnd()const;
		void SetMoveEnd(const bool flag)const;

		bool GetSelect()const;
		void SetSelect(const bool flag)const;

		bool GetStart()const;
		void SetStart(const bool flag)const;

		bool GetObjMove()const;
		void SetObjMove(const bool flag)const;

		virtual void OnUpdate()override;

		//カメラの写す範囲
		void Restriction();
		//ゴール時の動き
		void GoalMove();
	private:
		struct Impl;
		unique_ptr<Impl> pImpl;
	};
}
