#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	AutoCharacter::AutoCharacter
	(
		const shared_ptr<Stage>& StagePtr, 
		const Vec3& StartPos,
		const Vec3& StartRot,
		const Vec3& StartScale,
		const wstring TexturePtr,
		const float Gravity,
		const shared_ptr<ObjState<AutoCharacter>> StartState,
		const float MoveX, 
		const float MoveZ,
		const float Speed
	):
		GameObject(StagePtr), 
		m_Pos(StartPos),
		m_Rot(StartRot), 
		m_Scale(StartScale),
		m_Texture(TexturePtr),
		m_Gravity(Gravity),
		m_DeadFlag(false),
		m_VeloNum(0),
		m_State(StartState),
		moveX(MoveX),
		moveZ(MoveZ),
		m_TouchAcceleration(1.0f),
		m_Speed(Speed),
		m_Acceleration(1.0f),
		m_NextRot(0.0f),
		m_Length(0.0f)
	{}

	AutoCharacter::AutoCharacter(const shared_ptr<Stage>& StagePtr, IXMLDOMNodePtr Node):
		GameObject(StagePtr),
		m_Rot(Vec3(0.0f,90.0f * XM_PI / 180.0f,0.0f)),
		m_Texture(L"DOG_TX"),
		m_Gravity(-5.0f),
		m_DeadFlag(false),
		m_VeloNum(0),
		m_State(CharaLineState::Instance()),
		m_TouchAcceleration(1.0f),
		m_MoveFlag(true),
		m_Acceleration(1.0f),
		m_NextRot(0.0f),
		m_Length(0.0f),
		m_DeadCount(0)
	{
		auto PosStr = XmlDocReader::GetAttribute(Node, L"Pos");
		vector<wstring> PosT;
		Util::WStrToTokenVector(PosT, PosStr, L',');
		m_Pos = Vec3
		(
			static_cast<float>(_wtof(PosT[0].c_str())),
			static_cast<float>(_wtof(PosT[1].c_str())),
			static_cast<float>(_wtof(PosT[2].c_str()))
		);
		auto ScaleStr = XmlDocReader::GetAttribute(Node, L"Scale");
		vector<wstring> ScaleT;
		Util::WStrToTokenVector(ScaleT, ScaleStr, L',');
		m_Scale = Vec3
		(
			static_cast<float>(_wtof(ScaleT[0].c_str())),
			static_cast<float>(_wtof(ScaleT[1].c_str())),
			static_cast<float>(_wtof(ScaleT[2].c_str()))
		);
		auto SpeedStr = XmlDocReader::GetAttribute(Node, L"Speed");
		m_Speed = static_cast<float>(_wtof(SpeedStr.c_str()));
		auto MoveX = XmlDocReader::GetAttribute(Node, L"MoveX");
		moveX = static_cast<float>(_wtof(MoveX.c_str()));
		auto MoveZ = XmlDocReader::GetAttribute(Node, L"MoveZ");
		moveZ = static_cast<float>(_wtof(MoveZ.c_str()));
	}

	void AutoCharacter::OnCreate()
	{
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos);
		PtrTransform->SetRotation(m_Rot);
		PtrTransform->SetScale(m_Scale);

		//モデル用トランスフォーム
		Mat4x4 mat;
		mat.affineTransformation
		(
			Vec3(0.3f, 0.3f, 0.3f),//scale
			Vec3(0.0f, 0.0f, 0.0f),//rotationの原点
			Vec3(0.0f, 90.0f * XM_PI / 180.0f, 0.0f),//rotation
			Vec3(0.0f, 0.0f, 0.0f)//position
		);

		//コリジョン
		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::None);

		auto PtrRigit = AddComponent<Rigidbody>();
		//PtrRigit->SetGravityVelocity(Vec3(0.0f, m_Gravity, 0.0f));

		//描画のコンポーネント
		auto PtrDraw = AddComponent<BcPNTBoneModelDraw>();
		PtrDraw->SetMeshResource(L"Kokken2");
		PtrDraw->SetMeshToTransformMatrix(mat);
		PtrDraw->AddAnimation(L"RUN", 0, 120, true, 60.0f);
		PtrDraw->ChangeCurrentAnimation(L"RUN");

		//m_Area = GetStage()->AddGameObject<Area>(m_Pos, m_Rot, m_Scale * 3);

		//ステートマシンの構築
		m_StateMachine.reset(new LayeredStateMachine<AutoCharacter>(GetThis<AutoCharacter>()));
		//ステートマシンの初期化
		m_StateMachine->Reset(m_State);

		//DebugLog表示 ※
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
		PtrString->SetFontColor(Col4(1.0f, 1.0f, 1.0f, 1.0f));

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Fall");

		GetStage()->SetSharedGameObject(L"Chara", GetThis<AutoCharacter>());
		AddTag(L"Chara");
		SetGroup();

		m_NextRot = m_Rot.y;

		SetAlphaActive(true);
	}
	void AutoCharacter::OnUpdate()
	{
		m_StateMachine->Update();

		if (m_DeadFlag)
		{
			DeadChara();
		}
	}

	void AutoCharacter::OnCollision(vector<shared_ptr<GameObject>>& OtherVec)
	{
		for (auto& v : OtherVec)
		{		
			m_Obj = v;
			auto scale = v->GetComponent<Transform>()->GetScale();
			auto camera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
			if (GetStateMachine()->GetTopState() == CharaLineState::Instance() && v->FindTag(L"Point") & camera->GetStart())
			{
				m_point = m_Obj;
				m_ColPos = GetComponent<Transform>()->GetPosition();
				GetStateMachine()->Push(NextLineState::Instance());
			}
			else if(GetStateMachine()->GetTopState() == CharaLineState::Instance() && v->FindTag(L"Point") & !camera->GetStart())
			{
				m_point = m_Obj;
				StartMove();
			}			
		}
	}

	void AutoCharacter::OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec)
	{
	}

	void AutoCharacter::OnCollisionExit(vector<shared_ptr<GameObject>>& OtherVec)
	{
	}

	void AutoCharacter::Move()
	{
		auto count = GetStage()->GetSharedGameObject<Player>(L"Player")->GetCount();
		auto pos = GetComponent<Transform>()->GetPosition();
		auto time = App::GetApp()->GetElapsedTime();
		auto draw = GetComponent<BcPNTBoneModelDraw>();
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		m_Velocity = pos - m_BeforePos;
		m_BeforePos = GetComponent<Transform>()->GetPosition();

		GetComponent<Transform>()->SetRotation(Vec3(0.0f, m_NextRot, 0.0f));

		if (GetMoveFlag())
		{
			pos.x += moveX * m_TouchAcceleration * m_Speed  * m_Acceleration * time;
			pos.z += moveZ * m_TouchAcceleration * m_Speed * m_Acceleration * time;
			draw->UpdateAnimation(m_TouchAcceleration * m_Speed  * m_Acceleration * time);
		}

		GetComponent<Transform>()->SetPosition(pos.x, pos.y, pos.z);


		m_Length -= m_TouchAcceleration * m_Speed  * m_Acceleration * time;
		if (m_Length <= 0.0f)
		{
			m_Length = 0.0f;
		}

		if (m_TouchAcceleration > 1)
		{
			m_TouchAcceleration -= time;
		}

		if (count == 0)
		{
			m_Acceleration = 3.0f;
		}
	}

	void AutoCharacter::NextMove()
	{
		auto pos = GetComponent<Transform>()->GetPosition();
		auto radius = GetPoint()->GetComponent<Transform>()->GetScale().x;
		float dis = sqrtf(pow(pos.x - m_ColPos.x, 2) + pow(pos.z - m_ColPos.z, 2));

		//点の中心に行くまで移動
		if (dis < radius)
		{
			Move();
		}
		else
		{
			NextVelo();
			GetStateMachine()->Pop();
		}
	}

	void AutoCharacter::NextVelo()
	{
		auto pos = GetComponent<Transform>()->GetPosition();
		auto LineDire = GetPoint()->GetThis<Point>()->GetLineDire();
		auto Targetpos = GetPoint()->GetThis<Point>()->GetLinePos();
		auto GoalLineDire = GetPoint()->GetThis<Point>()->GetGoalLineDire();
		auto GoalLinePos = GetPoint()->GetThis<Point>()->GetGoalLinePos();
		auto GoalFlag = GetPoint()->GetThis<Point>()->GetGoalFlag();
		auto CharaDire = m_Velocity;
		auto length = LineDire.size();
		auto LineLengh = GetPoint()->GetThis<Point>()->GetLineLength();
		auto goallineLength = GetPoint()->GetThis<Point>()->GetGoalLineLength();
		auto pointScale = GetPoint()->GetComponent<Transform>()->GetScale();

		vector<Vec3> StorageDire;
		vector<Vec3> StoragePos;
		vector<float> StorageLength;

		CharaDire = CharaDire.normalize() * 10;

		SetVec(CharaDire);

		if (!GoalFlag)
		{
			for (int i = 0; i < (int)length; i++)
			{
				if ((int)CharaDire.x != (int)(-LineDire[i].x * 10) || (int)CharaDire.z != (int)(-LineDire[i].z * 10))
				{
					StorageDire.push_back(LineDire[i]);
					StoragePos.push_back(Targetpos[i]);
					if (m_Length <= pointScale.x)
					{
						StorageLength.push_back(LineLengh[i]);
					}
					else
					{
						return;
					}
				}			
			}
		}
		else
		{
			StorageDire.push_back(GoalLineDire);
			StoragePos.push_back(GoalLinePos);
			StorageLength.push_back(goallineLength);
		}


		if (StorageDire.size() != 0 && StorageLength.size() != 0)
		{
			auto pos = GetComponent<Transform>()->GetPosition();
			auto target = StoragePos[(int)StoragePos.size() - 1];
			m_NextRot = -atan2f(pos.z - target.z, pos.x - target.x);
			moveX = StorageDire[(int)StorageDire.size() - 1].x;
			moveZ = StorageDire[(int)StorageDire.size() - 1].z;
			m_Length = StorageLength[(int)StorageLength.size() - 1];
		}
		else
		{
			m_DeadFlag = true;
			auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"Fall");
			m_DeadCount = 1;
		}
	}

	void AutoCharacter::DeadChara()
	{
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		PtrCamera->SetTargetObject(GetThis<GameObject>());

		auto scene = App::GetApp()->GetScene<Scene>();
		auto trans = GetComponent<Transform>();
		auto pos = trans->GetPosition();
		auto scale = trans->GetScale();
		auto rot = trans->GetRotation();
		auto time = App::GetApp()->GetElapsedTime();
		auto pointScale = GetPoint()->GetComponent<Transform>()->GetScale();
		float num = scale.x;
		if (num > 0)
		{
			//回転気になる
			SetRot(Vec3(rot.x * time, rot.y, rot.z));
			num -= time / 1.5f;
			SetScale(num);
		}
		else
		{
			PostEvent(0.0f, GetThis<ObjectInterface>(), scene, L"ToGameStage");
		}		
	}

	void AutoCharacter::StartMove()
	{
		m_MoveFlag = false;
		auto pos = GetComponent<Transform>()->GetPosition();
		auto pointScale = GetPoint()->GetComponent<Transform>()->GetScale();
		SetPosition(Vec3(pos.x - (moveX * pointScale.x * 2), pos.y, pos.z - (moveZ * pointScale.z * 2)));
	}

	void AutoCharacter::GoalMove()
	{
		auto pos = GetComponent<Transform>()->GetPosition();
		auto camera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		auto target = camera->GetEye();
		m_NextRot = -atan2f(pos.z - target.z, pos.x - target.x);	
		SetRot(Vec3(0.0f, m_NextRot, 0.0f));
	}

	//ラインゲーム用
	IMPLEMENT_SINGLETON_INSTANCE(CharaLineState)
	void CharaLineState::Enter(const shared_ptr<AutoCharacter>& Obj)
	{
		
	}
	void CharaLineState::Execute(const shared_ptr<AutoCharacter>& Obj)
	{
		Obj->Move();
	}
	void CharaLineState::Exit(const shared_ptr<AutoCharacter>& Obj)
	{

	}

	//次の線に行くとき
	IMPLEMENT_SINGLETON_INSTANCE(NextLineState)
	void NextLineState::Enter(const shared_ptr<AutoCharacter>& Obj)
	{
	}
	void NextLineState::Execute(const shared_ptr<AutoCharacter>& Obj)
	{
		Obj->NextMove();
	}
	void NextLineState::Exit(const shared_ptr<AutoCharacter>& Obj)
	{
	}
}
