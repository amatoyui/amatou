#pragma once
#include "Project.h"

namespace basecross
{
	//---------------------------------------------------------------
	//	タイトルクラス
	//---------------------------------------------------------------
	class Title : public Stage
	{
		//ビューとライトの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//スプライトの作成
		void CreateSprite();
		void CreateFloor();
		//シーン変更
		void SceneChenger();

	public :
		//構築と破棄
		Title() :Stage() {}
		virtual ~Title() {}

		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;

	};



}