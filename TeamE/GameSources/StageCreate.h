#pragma once
#include "stdafx.h"

namespace basecross
{
	class StageCreate
	{
		shared_ptr<GameStage> m_Stage;
		wstring m_StageName;
		//行の数
		size_t m_RowNum;
		//列の数
		size_t m_ColNum;
		//Position
		Vec3 m_Pos;

	public:
		StageCreate(const shared_ptr<GameStage> StagePtr,const wstring& FileName);
		~StageCreate(){}

		void OnCreate();

		//CsvでStageの生成
		//オブジェクトの生成用
		void CreateObject(wstring CsvNum);

		//文字の分割
		vector<wstring> Split(const wstring& ObjNum, wchar_t SplitStr);
	};

	class XmlAccessor
	{
		//データの保存用
		wstring m_FileName;
		//ルートノードの取得用
		XmlDoc m_XmlDoc;
		int m_count;

		//XMLリーダー
		unique_ptr<XmlDocReader> m_XmlDocReader;
	public:
		XmlAccessor(const wstring XmlFileName);
		~XmlAccessor() {};

		void SetObjectData(const shared_ptr<GameObject>& Obj, int StageNum);//※
		void SetPoint(const shared_ptr<GameObject>& Obj, int num);
		void SetLine(const shared_ptr<GameObject>& Obj, int num);
		void SetChara(const shared_ptr<GameObject>& Obj, int num);
		void SetGoalLine(const shared_ptr<GameObject>& Obj, int num);
		void SetGoal(const shared_ptr<GameObject>& Obj, int num);
		void SetPlayer(const shared_ptr<GameObject>& Obj, int num);
		void SetTurn(const shared_ptr<GameObject>& Obj, int num);
		void SetAway(const shared_ptr<GameObject>& Obj, int num);
		void SetClose(const shared_ptr<GameObject>& Obj, int num);
		void SetItem(const shared_ptr<GameObject>& Obj, int num);
		void SetGoalPanel(const shared_ptr<GameObject>& Obj, int num);
		void SetStageElement(int num);
		void MapCreate();

		int GetCount(int num);
		int GetVer(int num);
		int GetHori(int num);
		int GetTime(int num);
		void Remove(int num);

		//xmlでStageの生成
		void CreateStageXml(wstring XmlName);
	};
}
