#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	void ObjCreateStage::CreateViewLight()
	{
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<MyCamera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 20.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));

		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 11.0f, -0.25f);
	}
	void ObjCreateStage::CreatePlate()
	{
		auto scene = App::GetApp()->GetScene<Scene>();
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quat Qt;
		Qt.rotationRollPitchYawFromVector(Vec3(XM_PIDIV2, 0, 0));
		PtrTrans->SetScale(static_cast<float>(scene->GetVer()), static_cast<float>(scene->GetHori()), 1.0f);
		PtrTrans->SetQuaternion(Qt);
		PtrTrans->SetPosition(static_cast<float>(scene->GetVer()) / 2, -0.5f, static_cast<float>(scene->GetHori()) / 2);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"Checker_TX");

	}
	void ObjCreateStage::CreatePlayer()
	{
		auto player = AddGameObject<Player>(Vec3(30.0f, 0.0f, 30.0f), Vec3(1.0f, 1.0f, 1.0f), Vec3(0, 22.0f, 0), float(8.0f), L"NIGAKI_TX",EditModeState::Instance(),99);
		player->SetVer(100);
		player->SetHori(100);
		auto scene = App::GetApp()->GetScene<Scene>();
		scene->SetMinVer(0);
		scene->SetMinHori(0);
	}

	void ObjCreateStage::OnCreate()
	{
		try
		{
			CreateSharedObjectGroup(L"EditGroup");

			//ステージの番号選択
			auto scene = App::GetApp()->GetScene<Scene>();
			scene->SetStageNum(42);
			scene->SetTime(6);

			CreateViewLight();

			AddGameObject<TypeUI>(Vec2(400.0f, 330.0f), Vec2(250.0f, 250.0f));
			AddGameObject<CountUI>(Vec2(400.0f, 200.0f), Vec2(150.0f, 150.0f));
			AddGameObject<RotUI>(Vec2(400.0f, 100.0f), Vec2(80.0f, 100.0f));

			GameObjecttXMLBuilder Builder;
			//GameObjectの登録
			Builder.Register<Point>(L"Point");
			Builder.Register<Line>(L"Line");
			Builder.Register<Goal>(L"Goal");
			Builder.Register<AutoCharacter>(L"Chara");
			Builder.Register<TurnGousun>(L"Turn");
			Builder.Register<AwayGousun>(L"Away");
			Builder.Register<CloseGousun>(L"Close");
			Builder.Register<GoalLine>(L"Goalline");
			Builder.Register<PlusCount>(L"Item");
			Builder.Register<GoalPanel>(L"GoalPanel");

			wstring DataDir;
			App::GetApp()->GetDataDirectory(DataDir);
			wstring XMLStr = DataDir + L"StageCreate\\Data";
			XMLStr += L".xml";
			auto xml = new XmlAccessor(XMLStr);
			
			for (int i = 0; i < xml->GetCount(scene->GetStageNum()); i++)
			{
				Builder.Build(GetThis<Stage>(), XMLStr, L"GameStage/map" + Util::IntToWStr(scene->GetStageNum()) + L"/GameObject" + Util::IntToWStr(i));
			}

			CreatePlayer();
			CreatePlate();
		}
		catch (...)
		{
			throw;
		}
	}
}
