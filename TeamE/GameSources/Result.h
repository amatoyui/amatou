#pragma once
#include "Project.h"

namespace basecross
{
    //----------------------------------------------------------------
	//	リザルトクラス
	//----------------------------------------------------------------

	class Result : public Stage
	{
		//ビューとライトの作成
		void CreateViewLight();

		//スプライトの作成
		void CreateSprite();

		//シーンチェンジャー
		void SceneChenger();

	public :
		//構築と破棄
		Result() :Stage() {}
		virtual ~Result() {}

		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

}
