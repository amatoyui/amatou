/*!
@file Scene.h
@brief シーン
*/
#pragma once

#include "Project.h"
namespace basecross{

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	class Scene : public SceneBase{
		//--------------------------------------------------------------------------------------
		/*!
		@brief リソースの作成
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		void CreateResourses();

		wstring m_CsvName = L"GameStage.csv";
		int m_vertical;
		int m_horizontal;
		int m_MinVer = 0;
		int m_MinHori = 0;
		int m_StageNum = 1;

		int m_MapNum;
		int m_Time = 1;
	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		*/
		//--------------------------------------------------------------------------------------
		Scene() :SceneBase(){}
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Scene(){}
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief イベント取得
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnEvent(const shared_ptr<Event>& event) override;

		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		void StopBGM();
		
		//アクセサ
		wstring GetCsv() { return m_CsvName; }
		void SetCsv(wstring CsvFile) { m_CsvName = CsvFile; };

		int GetVer() { return m_vertical; }
		void SetVer(int value) { m_vertical = value; }

		int GetHori() { return m_horizontal; }
		void SetHori(int value) { m_horizontal = value; }

		int GetMinVer() { return m_MinVer; }
		void SetMinVer(int value) { m_MinVer = value; }

		int GetMinHori() { return m_MinHori; }
		void SetMinHori(int value) { m_MinHori = value; }

		int GetMapNum() { return m_MapNum; }
		void SetMapNum(int value) { m_MapNum = value; }

		int GetTime() { return m_Time; }
		void SetTime(int value) { m_Time = value; }

		int GetStageNum() { return m_StageNum; }
		void SetStageNum(int value) 
		{ 
			m_StageNum = value; 
			switch (m_StageNum)
			{
			case 1:
				m_CsvName = L"GameStage.csv";
				break;
			case 2:
				m_CsvName = L"GameStage2.csv";
				break;
			case 3:
				m_CsvName = L"GameStage3.csv";
				break;
			}
		}
	};

}

//end basecross
