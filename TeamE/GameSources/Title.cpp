#include "stdafx.h"
#include "Project.h"      
#include "Title.h"
namespace basecross 
{
	//---------------------------------------------------------------
	//タイトルクラス実体
	//---------------------------------------------------------------
	void Title::CreateViewLight()
	{
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラ設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vec3(-10.0f, 5.0f, 0.0f));//目の位置
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));	 //見たい位置
		PtrCamera->SetFovY(90.0f);					 //視野角
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	void Title::CreatePlate()
	{
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quat Qt;
		Qt.rotationRollPitchYawFromVector(Vec3(0, 0, 0));
		PtrTrans->SetScale(50.0f, 50.0f, 1.0f);
		PtrTrans->SetQuaternion(Qt);
		PtrTrans->SetPosition(0.0f, 0.0f, 0.0f);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"SAN_TX");
	}
	void Title::CreateFloor()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quat Qt;
		Qt.rotationRollPitchYawFromVector(Vec3(0, 0, 0));
		PtrTrans->SetScale(5.0f, 5.0f, 5.0f);
		PtrTrans->SetQuaternion(Qt);
		PtrTrans->SetPosition(0.0f, 0.0f,0.0f);

		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		DrawComp->SetMeshResource(L"DEFAULT_CUBE");
		DrawComp->SetOwnShadowActive(true);
		DrawComp->SetTextureResource(L"KOKKEN_TX");
	}

	void Title::CreateSprite()
	{
		auto sprite = AddGameObject<Sprite>(Vec2(0.0f, 0.0f), Vec2(1280.0f, 800.0f), L"TITLE_TX", false);
		auto anybutton = AddGameObject<AnyButton>(Vec2(0.0f, -300.0f), Vec2(600.0f, 180.0f), L"ANYBUTTON_TX");
	}

	void Title::OnCreate()
	{
		try {
			//ビューとライトの作成
			CreateViewLight();

			//スプライトの作成
			CreateSprite();	
		}
		catch (...)
		{
			throw;
		}
	}
	void Title::OnUpdate()
	{
			
	}

}